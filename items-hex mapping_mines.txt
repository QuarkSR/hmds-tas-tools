Note all numbers are in hexadecimal. 

------------------------------------------------------------
Mine 1
------------------------------------------------------------
	High bits
------------------------------------------------------------
		10 -> rock containing nothing
		20 -> rock containing junk ore
		30 -> rock containing bronze
		40 -> rock containing silver
		50 -> rock containing gold
		60 -> rock containing mystrile
------------------------------------------------------------
	Low bits
------------------------------------------------------------
		01 -> possible location for staircase down
		02 -> contains hole
		03 -> contains coin bag
		04 -> contains black grass
		0C -> dug square
		0D -> dug hole
		0E -> staircase up
		0F -> staircase down, unearthed
------------------------------------------------------------
Mine 2
------------------------------------------------------------
	High bits
------------------------------------------------------------
		10 -> rock containing nothing
		20 -> rock containing junk ore
		30 -> rock containing pink diamond
		40 -> rock containing alexandrite
		50 -> rock containing moon stone
		60 -> rock containing sand rose
		70 -> rock containing diamond
		80 -> rock containing emerald
		90 -> rock containing ruby
		A0 -> rock containing topaz
		B0 -> rock containing peridot
		C0 -> rock containing fluorite
		D0 -> rock containing agate
		E0 -> rock containing amethyst
-------------------------------------------------------------
	Low bits
-------------------------------------------------------------
		01 -> possible location for staircase down
		02 -> contains hole
		03 -> contains coin bag
		04 -> contains black grass
		05 -> contains lithograph
		0C -> dug square
		0D -> hole, already dug
		0E -> staircase up
		0F -> staircase down, unearthed
------------------------------------------------------------
Mine 3
------------------------------------------------------------
	High bits
------------------------------------------------------------
		10 -> rock containing nothing
		20 -> rock containing junk ore
		30 -> rock containing copper
		40 -> rock containing silver
		50 -> rock containing gold
		60 -> rock containing mystrile
		70 -> rock containing spring sun
		80 -> rock containing summer sun
		90 -> rock containing autumn sun
		A0 -> rock containing winter sun
		B0 -> Mythic stone
		C0 -> rock containing adamantite
		D0 -> rock containing orichalc
------------------------------------------------------------
	Low bits
------------------------------------------------------------
		01 -> possible location for staircase down
		02 -> contains hole
		03 -> contains coin bag
		04 -> contains black grass
		05 -> may contain cursed tool
		06 -> contains cursed accessory
		07 -> contains lithograph
		0C -> dug square
		0D -> hole, already dug
		0E -> staircase up
		0F -> staircase down, unearthed
------------------------------------------------------------
Mine 4
------------------------------------------------------------
	High bits
------------------------------------------------------------
		10 -> rock containing nothing
		20 -> rock containing junk ore
		30 -> rock containing blue wonderful
		40 -> rock containing green wonderful
		50 -> rock containing red wonderful
		60 -> rock containing yellow wonderfulz
		70 -> rock containing orange wonderful
		80 -> rock containing purple wonderful
		90 -> rock containing indigo wonderful
		A0 -> rock containing black wonderful
		B0 -> rock containing white wonderful
-------------------------------------------------------------
	Low bits
-------------------------------------------------------------
		01 -> possible location for staircase down
		02 -> contains hole
		03 -> contains coin bag
		04 -> contains bronze coin
		05 -> contains silver coin
		06 -> contains gold coin
		07 -> contains black grass
		08 -> contains lithograph
		0C -> dug square
		0D -> hole, already dug
		0E -> staircase up
		0F -> staircase down, unearthed
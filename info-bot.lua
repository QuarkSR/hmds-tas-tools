-- Memory locations
require("lua-tas-wip.memoryLocation")

global_timer = 0

-- Setup base level for tool when starting the script
tool_timer = 0
tools = {"axe", "rod", "hammer", "hammer", "sickle", "watering can", "hoe", "milker"}
tool_memory = {
    ["axe"] = {mem_axe},
    ["rod"] = {mem_rod},
    ["hammer"] = {mem_hammer},
    ["sickle"] = {mem_sickle},
    ["watering can"] = {mem_can},
    ["hoe"] = {mem_hoe},
    ["milker"] = {mem_milker}
}
tool_info = {}
for t = 1, #tools do
    tool = tools[t]
    val = memory.readword(tool_memory[tool][1][hmdsv])
    tool_info[tool] = {val, val}
end

-- Setupb base level for friendship when starting the script
timer_friend = 0
friends = {"Celia", "Muffy", "Nami", "Romana", "Sebastian", "Lumina", "Wally", "Chris", "Grant", "Kate",
"Hugh", "Carter", "Flora", "Vesta", "Marlin", "Ruby", "Rock", "Dr. Hardy", "Galen", "Nina",
"Daryl", "Cody", "Gustafa", "Griffin", "Vans", "Kassey", "Patrick", "Murrey", "Takakura",
"Uknown 2", "Uknown 3", "Uknown 4", "Uknown 5", "Uknown 6", "Uknown 7", "Uknown 8", "Uknown 9", 
"Uknown 10", "Kai", "Uknown 11", "Uknown 12", "Uknown 13", "Uknown 14", "Harvest G.",
"Thomas", "Gotz", "Uknown 16", "Leia", "Keira", "Witch P."} 
-- 23DC460
friendInfo = {}
loveInfo = {}
for f = 1, #friends do
    friend = friends[f]
    val = memory.readbyte(mem_NPCArray[hmdsv] + 4 + (f*40))
    friendInfo[friend] = {val, val, 0}
    val = memory.readword(mem_NPCArray[hmdsv] + 8 + (f*40))
    -- memory.writeword(mem_NPCArray[hmdsv] + 8 + (f*40), 510*120)
    loveInfo[friend] = {val, val, 0}
end

items = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}


-- Tomorrow's weather in home
function weather()
    local w = memory.readbyte(mem_tomorrows_weather[hmdsv])
    if w == 0 then
        gui.text(238, -10, "Su")
    elseif w == 1 then
        gui.text(238, -10, "Ra")
    elseif w == 2 then
        gui.text(238, -10, "Sn")
    elseif w == 3 then
        gui.text(238, -10, "St")
    elseif w == 4 then
        gui.text(238, -10, "Sh")
    end
end

function count_fish()
    local total = 0
    for f = 0, 50 do
        total = total + memory.readdword(mem_fishlist[hmdsv] + (f*4))
    end
    return total
end

-- Highlight holes (red) and potential down stairs (yellow)
function mines()
    local floorX = memory.readbyte(mem_floorX[hmdsv])
    local floorY = memory.readbyte(mem_floorY[hmdsv])
    local floorSize = floorX * floorY
    local floorArr = memory.readbyterange(mem_floorArr[hmdsv], floorSize)

    local highlight = {}

    for y = 1, floorY do
        for x = 1, floorX do

            -- Convert data to LSB
            local tile_hoe = AND(floorArr[x+((y-1)*floorX)], 0x0F)

            -- 0x01 = stairs, 0x02 = holes, 0x05 or 0x06 = cursed
            if tile_hoe == tonumber(0x01) then
                table.insert(highlight, {x, y, "yellow"})
            elseif tile_hoe == tonumber(0x02) then
                table.insert(highlight, {x, y, "red"})
            -- elseif tile_hoe == tonumber(0x04) then
            --     table.insert(highlight, {x, y, "blue"})
            elseif tile_hoe == tonumber(0x05) or tile_hoe == tonumber(0x06) then
                table.insert(highlight, {x, y, "green"})
            end
        end
    end

    -- Screen scroll (to move around squares when you move the screen)
    local screen_x = memory.readbyte(mem_screenX[hmdsv]) + (memory.readbyte(mem_screenX[hmdsv]+1))*255
    local screen_y = memory.readbyte(mem_screenY[hmdsv]) + (memory.readbyte(mem_screenY[hmdsv]+1))*255

    -- Floor size is 16x16 pixel
    local tiles_size = 16

    -- Red box around holes
    local h_count = table.getn(highlight)
    for i = 1, h_count do
        gui.box(tiles_size*(highlight[i][1])-screen_x+1,
                -(8*tiles_size)+(tiles_size*(highlight[i][2]-1)-screen_y+1),
                tiles_size*(highlight[i][1]+1)-screen_x-1,
                -(8*tiles_size)+tiles_size*(highlight[i][2])-screen_y-1,
                0,
                highlight[i][3])
    end
end

function section(xstart, ystart, pos, type, info)
    for y = 0, ystart-1 do
        for x = 0, xstart-1 do
            if type == "write" then
                memory.writedword(0x023D83DC+pos+(4*x)+(4*(xstart)*y), info)
            end
        end
    end
    pos = pos + 4*(xstart)*(ystart)
    return pos
end

function count_items()
    items = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}

    for fl = 0, 3101 do
        tile = memory.readdword(mem_farmland[hmdsv]+(fl*4))

        tile_array = {}
        str= tostring(bit.tohex(tile))
        str:gsub(".",function(c) table.insert(tile_array,c) end)

        if tile_array[8] == "3" then
            if (("0x"..tile_array[5]) % 2 == 0) then
                for tl = 0, 15 do
                    if (("0x"..tile_array[6])*1 == tl) then
                        items[tl+1] = items[tl+1] + 1
                    end
                end
            else
                for tl = 0, 15 do
                    if (("0x"..tile_array[6])*1 == tl) then
                        items[tl+1+16] = items[tl+1+16] + 1
                    end
                end
            end
        end
    end

    return items
end

function tiles(update)

    if global_timer % update == 0 then
        items = count_items()
    end

    gui.text(0, -10, "R:" .. items[2])
    gui.text(35, -10, "B:" .. items[5]/4)
    gui.text(70, -10, "B:" .. items[6]/4)
    gui.text(0, -20, "S:" .. items[3])
    gui.text(35, -20, "S:" .. items[4]/4)

end

-- Tool use information
function tool_use()
    local screen_x = memory.readbyte(mem_screenX[hmdsv]) + (memory.readbyte(mem_screenX[hmdsv]+1))*255
    local screen_y = memory.readbyte(mem_screenY[hmdsv]) + (memory.readbyte(mem_screenY[hmdsv]+1))*255
    local char_x = memory.readbyte(mem_charX[hmdsv]) + (memory.readbyte(mem_charX[hmdsv]+1))*255
    local char_y = memory.readbyte(mem_charY[hmdsv]) + (memory.readbyte(mem_charY[hmdsv]+1))*255

    for t = 1, #tools do
        tool = tools[t]
        tool_info[tool][2] = memory.readword(tool_memory[tool][1][hmdsv])

        diff = tool_info[tool][2] - tool_info[tool][1]

        if diff > 0 then
            tool_timer = 30
            tool_val = tool_info[tool][2]
            if diff == 100 then 
                colour = "yellow"
            elseif diff == 200 then
                colour = "green"
            else
                colour = "red"
            end
        end
        tool_info[tool][1] = tool_info[tool][2]
    end

    if tool_timer > 0 then
        tool_timer = tool_timer - 1
        gui.text(char_x-screen_x-5, (-220+(tool_timer/3))+(char_y-screen_y), tool_val/100, colour)
    end
end

-- Friendship points up/down
function friendship()

    for f = 1, #friends do
        friend = friends[f]
        if friendInfo[friend][3] == 0 then

            friendInfo[friend][2] = memory.readbyte(mem_NPCArray[hmdsv] + 4 + (f*40))
            if friendInfo[friend][2] ~= friendInfo[friend][1] then
                friendInfo[friend][3] = 60
            end
        end
        if loveInfo[friend][3] == 0 then

            loveInfo[friend][2] = memory.readword(mem_NPCArray[hmdsv] + 8 + (f*40))
            if loveInfo[friend][2] ~= loveInfo[friend][1] then
                loveInfo[friend][3] = 200
            end
        end
    end

    fc = -10
    for f = 1, #friends do
        friend = friends[f]
        if loveInfo[friend][3] > 0 then
            fc = fc + 10

            diff = loveInfo[friend][2] - loveInfo[friend][1]

            if diff > 0 then
                colour = "green"
                sign = "+"
            elseif diff < 0 then 
                colour = "red"
                sign = ""
            end
            fText = friend .. " (LP)" ..  loveInfo[friend][2] .. " (" .. sign .. diff .. ")"

            gui.text(130, fc-187, fText, colour)

            loveInfo[friend][3] = loveInfo[friend][3] - 1
            if loveInfo[friend][3] == 0 then
                loveInfo[friend][1] = loveInfo[friend][2]
            end
        end
        -- if friendInfo[friend][3] > 0 then
        --     fc = fc + 10

        --     diff = friendInfo[friend][2] - friendInfo[friend][1]

        --     if diff > 0 then
        --         colour = "green"
        --         sign = "+"
        --     elseif diff < 0 then 
        --         colour = "red"
        --         sign = ""
        --     end
        --     fText = friend .. " " ..  friendInfo[friend][2] .. " (" .. sign .. diff .. ")"

        --     gui.text(150, fc-187, fText, colour)

        --     friendInfo[friend][3] = friendInfo[friend][3] - 1
        --     if friendInfo[friend][3] == 0 then
        --         friendInfo[friend][1] = friendInfo[friend][2]
        --     end
        -- end
    end
end

function contains(table, val)
    for i=1,#table do
       if table[i] == val then 
          return true
       end
    end
    return false
 end

function animation_timer()
    local action = memory.readbyte(0x023D37E4)
    -- gui.text(0, 0, action)
    dont_display = {0, 1, 2, 35, 59, 61}
    if not contains(dont_display, action) then
        local curr_chunk = memory.readbyte(0x023D37DC) 
        if curr_chunk ~= nil then
            local curr_frame = memory.readbyte(0x023D37DF)
            local loc = memory.readdword(0x023D37D8)
            local count = memory.readbyte(loc-2)
            
            frames = 0
            frame_list = {}
            for f = 0, count-1 do
                table.insert(frame_list, memory.readbyte(loc+(f*2)))
                frames = frames + memory.readbyte(loc+(f*2))
            end
            frames_left = 0
            if count > 0 then
                for cf = 1, curr_chunk+1 do
                    if frame_list[cf] == nil then
                        frame_list[cf] = 0 
                    end
                    frames_left = frames_left + frame_list[cf]
                end
                frames_left = frames_left - curr_frame
            end

            gui.text(115, -10, frames .. " (" .. frames-frames_left+1 .. ")")
        end
    end
end

function wrap()

    -- Debug menu for writing information
    require("debug-menu.debug-menu")

    -- Stamina and Exhaustion
    gui.text(160, -33, "S = " .. memory.readbyte(mem_stamina[hmdsv]))
    gui.text(210, -33, "E = " .. memory.readbyte(mem_fatigue[hmdsv]))

    -- Shops
    shop = memory.readbyte(mem_shop[hmdsv])
    grass = memory.readbyte(mem_grass[hmdsv])
    watered = memory.readword(0x023DE2C6)
    fish = count_fish()
    gui.text(0, -190, "Shop = " .. shop .. " | Grass = " .. grass .. " | F = " .. fish .. " | W = " .. watered)

    -- Minutes (instead of 10s of minutes)
    gui.text(213, -11, memory.readbyte(mem_minute[hmdsv])%10)

    -- gui.text(0, 10, "Touch: " .. string.format("0x%x", memory.readdword(0X023DE18E)))

    -- Pop up information
    tool_use()
    friendship()

    -- Timer
    animation_timer()

    -- Farmland
    tiles(60)

    -- Temp

    -- where are you on the map?
    local loc = memory.readbyte(mem_char_loc[hmdsv])

    -- at home
    if loc == tonumber("0x30") then
        weather()

    -- in the mines
    elseif loc >= tonumber("0x3a") and loc <= tonumber("0x3f") then
        mines()
    end
    
    global_timer = global_timer + 1

    -- memory.writedword(mem_inv18[hmdsv], bit.bor(bit.lshift(100, 7*8), 0x016F))

end

gui.register(wrap)
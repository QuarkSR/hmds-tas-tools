-- version of HMDS (will work as a index+1 for memory lists)
local hmdsv = 0

-- floor memory locations
local mem_vansprice = {0x023D3554}

function main(mattemps, attemps, price)

    function file_exists(name)
        local f=io.open(name,"r")
        if f~=nil then io.close(f) return true else return false end
     end

    if not file_exists("vans.csv") then
        f = io.open(string.format("vans.csv"), "w")
        f:write("price,sell\n")
    else
        f = io.open(string.format("vans.csv"), "a")
    end

    function cleanup()
        f:flush()
        io.close(f)
    end

    emu.registerexit(cleanup)

    savestate.create(8)

    for tl = 1, mattemps do
        savestate.load(8)
        for xl = 0, 150 do
            emu.frameadvance()
        end
        savestate.save(8)
        for ml = 1, attemps do
            savestate.load(8)
            for fl = 0, ml do
                emu.frameadvance()
            end

            joypad.set({A=true})
            emu.frameadvance()
            for fl = 0, 1 do
                emu.frameadvance()
            end
            vp = memory.readdword(0x023D3554)

            f:write(string.format("%s,%s\n", price, vp))
        end
        print(tl)
    end

    f:flush()
	io.close(f)
end

main(1000, 60, 1000)

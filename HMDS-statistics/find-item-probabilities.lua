com = require(".\\lua-tas-wip\\common")

verbose = false

function main(mine, startingFloor, trialNo)
	-- defaults
	if not mine then mine = 1 end
	if not startingFloor then startingFloor = 0 end
	if not trialNo then trialNo = 3 end
	
	if startingFloor == 0 then
		f = io.open(string.format("probabilities-mine%d.csv", mine), "w")
		f:write("Floor,Trial no.,Item hex,Item,Amount\n")
	else
		f = io.open(string.format("probabilities-mine%d.csv", mine), "a")
	end

	function cleanup()
		f:flush()
		io.close(f)
	end

	emu.registerexit(cleanup)
	
	local floorCount = -1
	local entranceX = -1
	
	if mine == 0 then
		floorCount = 0
		entranceX = 640
	elseif mine == 1 then
		floorCount = 10
		entranceX = 32
	elseif mine == 2 then
		floorCount = 255
		entranceX = 96
	elseif mine == 3 then
		floorCount = 999
		entranceX = 160
	elseif mine == 4 then
		floorCount = 65535
		entranceX = 223
	else
		print("mine must be in range 1-4. Exiting.")
		return
	end
		
	for i = startingFloor, floorCount do
		-- freezes mine floor, declared here to minimise resource usage
		function freezeMineFloor()
			memory.writeword(0x023DBD2A, i)
		end
		
		for j = 1, trialNo do
			-- regenerate room
			com.moveX(128)
			com.moveY(216)
			emu.registerbefore(freezeMineFloor)
			com.moveX(entranceX)
			com.moveY(47)
			
			print(string.format("Floor %d / %d, Trial %d / %d", i, floorCount, j, trialNo))
			
			-- Guts text
			if i == floorCount and mine ~= 0 then
				com.advanceThroughTextBoxes(3)
			elseif (i % 100 == 0 and i > 0 and i <= 500) or (i % 1000 == 0 and i > 0 and i <= 5000) or (i % 10000 == 0 and i > 0) then
				com.advanceThroughTextBoxes(5)
			end
			
			local floorX = memory.readbyte(0x022C6C84)
			local floorY = memory.readbyte(0x022C6C86)
			local floorSize = floorX * floorY
			local floorArr = memory.readbyterange(0x023DB960, floorSize)
			
			local itemTable = { -- table of hex values to number of items
				[0x00] = 0,
				
				-- high nybbles
				[0x10] = 0,
				[0x20] = 0,
				[0x30] = 0,
				[0x40] = 0,
				[0x50] = 0,
				[0x60] = 0,
				[0x70] = 0,
				[0x80] = 0,
				[0x90] = 0,
				[0xA0] = 0,
				[0xB0] = 0,
				[0xC0] = 0,
				[0xD0] = 0,
				[0xE0] = 0,
				[0xF0] = 0,
				
				-- low nybbles
				[0x01] = 0,
				[0x02] = 0,
				[0x03] = 0,
				[0x04] = 0,
				[0x05] = 0,
				[0x06] = 0,
				[0x07] = 0,
				[0x08] = 0,
				[0x09] = 0,
				[0x0A] = 0,
				[0x0B] = 0,
				[0x0C] = 0,
				[0x0D] = 0,
				[0x0E] = 0,
				[0x0F] = 0
			}
			
			local hexToStr = { -- table of hex value to human-readable string		
				--mine0 = {
					
				--}
				mine1 = {
					[0x10] = "No item",
					[0x20] = "Junk ore",
					[0x30] = "Copper",
					[0x40] = "Silver",
					[0x50] = "Gold",
					[0x60] = "Mystrile",
					
					[0x00] = "No item",
					[0x01] = "Possible staircase",
					[0x02] = "Hole",
					[0x03] = "Coin bag",
					[0x04] = "Black grass",
					[0x0C] = "Mined square",
					[0x0D] = "Mined hole",
					[0x0E] = "Staircase up",
					[0x0F] = "Staircase down"
				},
				
				mine2 = {
					[0x10] = "No item",
					[0x20] = "Junk ore",
					[0x30] = "Pink diamond",
					[0x40] = "Alexandrite",
					[0x50] = "Moon stone",
					[0x60] = "Sand rose",
					[0x70] = "Diamond",
					[0x80] = "Emerald",
					[0x90] = "Ruby",
					[0xA0] = "Topaz",
					[0xB0] = "Peridot",
					[0xC0] = "Fluorite",
					[0xD0] = "Agate",
					[0xE0] = "Amethyst",
			
					[0x00] = "No item",
					[0x01] = "Possible staircase",
					[0x02] = "Hole",
					[0x03] = "Coin bag",
					[0x04] = "Black grass",
					[0x05] = "Lithograph",
					[0x0C] = "Mined square",
					[0x0D] = "Mined hole",
					[0x0E] = "Staircase up",
					[0x0F] = "Staircase down"
				},
				
				mine3 = {
					[0x10] = "No item",
					[0x20] = "Junk ore",
					[0x30] = "Copper",
					[0x40] = "Silver",
					[0x50] = "Gold",
					[0x60] = "Mystrile",
					[0x70] = "Spring sun",
					[0x80] = "Summer sun",
					[0x90] = "Fall sun",
					[0xA0] = "Winter sun",
					[0xB0] = "Mythic stone",
					[0xC0] = "Adamantite",
					[0xD0] = "Orichalc",
					
					[0x01] = "Possible staircase",
					[0x02] = "Hole",
					[0x03] = "Coin bag",
					[0x04] = "Black grass",
					[0x05] = "Cursed tool",
					[0x06] = "Cursed accessory",
					[0x07] = "Lithograph",
					[0x0C] = "Mined square",
					[0x0D] = "Mined hole",
					[0x0E] = "Staircase up",
					[0x0F] = "Staircase down"
				},
				
				mine4 = {
					[0x10] = "No item",
					[0x20] = "Junk ore",
					[0x30] = "Blue wonderful",
					[0x40] = "Green wonderful",
					[0x50] = "Red wonderful",
					[0x60] = "Yellow wonderful",
					[0x70] = "Orange wonderful",
					[0x80] = "Purple wonderful",
					[0x90] = "Indigo wonderful",
					[0xA0] = "Black wonderful",
					[0xB0] = "White wonderful",
					
					[0x01] = "Possible staircase",
					[0x02] = "Hole",
					[0x03] = "Coin bag",
					[0x04] = "Bronze coin",
					[0x05] = "Silver coin",
					[0x06] = "Gold coin",
					[0x07] = "Black grass",
					[0x08] = "Lithograph",
					[0x0C] = "Mined square",
					[0x0D] = "Mined hole",
					[0x0E] = "Staircase up",
					[0x0F] = "Staircase down"
				}
			}
			
			-- populate itemTable
			local currentRow = 3

			for k = 1, floorSize do
				local element = floorArr[k]
				local highNybble = AND(element, 0xF0)
				local lowNybble = AND(element, 0x0F)
				
				if (k - 1) % floorX == 0 then currentRow = currentRow + 1 end
				
				-- TODO: Probably doesn't work with rooms larger than the standard 14*7 (depends on the modulus I think)
				local currentSquareX = (((k - 1) * 16) % 224) + 16
				local currentSquareY = ((currentRow * 16) - 224)
				
				-- warning: enabling verbose mode slows down the trials substantially because of all the emu.frameadvance() calls
				if verbose then
					gui.box(currentSquareX, currentSquareY, currentSquareX + 16, currentSquareY + 16, "clear", "red")
					emu.frameadvance()
				end
				
				itemTable[highNybble] = itemTable[highNybble] + 1
				itemTable[lowNybble]  = itemTable[lowNybble]  + 1
			end
			
			-- which nested table in hexToStr
			local whichNested = string.format("mine%d", mine)
			
			for k = 0x01, 0xFF do
				if hexToStr[whichNested][k] then
					f:write(string.format("%d,%d,0x%.2X,%s,%d\n", i, j, k, hexToStr[whichNested][k], itemTable[k]))
				end
			end
		end
	end
	
	f:flush()
	io.close(f)
end

main(4, 990, 100)
-- version of HMDS (will work as a index+1 for memory lists)
local hmdsv = 0

-- Weather
local mem_todays_weather = {0x023D6B00}
local mem_tomorrows_weather = {0x023D6B02}

function main(day, number_of_trials, years)

    function file_exists(name)
        local f=io.open(name,"r")
        if f~=nil then io.close(f) return true else return false end
     end

    if not file_exists("weather.csv") then
        f = io.open(string.format("weather.csv"), "w")
        f:write("day,trial,today_weather,tomorrow_weather\n")
    else
        f = io.open(string.format("weather.csv"), "a")
    end

    function cleanup()
        f:flush()
        io.close(f)
    end

    emu.registerexit(cleanup)

    savestate.create(2)
    savestate.create(3)

    savestate.save(3)

    for y = 1, years+1 do

        savestate.load(3)
        if y > 1 then
            for frame = 1, 80000 do
                emu.frameadvance()
            end
            -- Wake up from floor snooze
            joypad.set({B=true, up=true})
            for frame = 1, 30 do
                emu.frameadvance()
            end
        end
        savestate.save(3)

        for d = day, 122 do

            for frame = 1, 30 do
                joypad.set({B=true, up=true})
                emu.frameadvance()
            end
            for frame = 1, 30 do
                joypad.set({B=true, left=true})
                emu.frameadvance()
            end

            savestate.save(2)

            for j = 1, number_of_trials do

                savestate.load(2)
                emu.frameadvance()
                savestate.save(2)

                joypad.set({A=true})
                emu.frameadvance()
                for frame = 1, 30 do
                    emu.frameadvance()
                end
                joypad.set({A=true})
                emu.frameadvance()

                if d == 115 then
                    -- God damnit Santa
                    for frame = 1, 400 do
                        emu.frameadvance()
                        joypad.set({A=true})
                        emu.frameadvance()
                    end
                else
                    for frame = 1, 180 do
                        emu.frameadvance()
                    end
                end

                local today = memory.readbyte(mem_todays_weather[hmdsv+1])
                local tomorrow = memory.readbyte(mem_tomorrows_weather[hmdsv+1])

                gui.text(0, 0, "Y = " .. y .. "   D = " .. d .. "   T = " .. j)
                f:write(string.format("%d,%d,%s,%s\n", d, j, today, tomorrow))

            end

            if d == 115 then
                -- God damnit Santa
                for frame = 1, 400 do
                    emu.frameadvance()
                    joypad.set({A=true})
                    emu.frameadvance()
                end
            else
                for frame = 1, 200 do
                    emu.frameadvance()
                end
            end
        end
        f:flush()
    end

    f:flush()
	io.close(f)
end

main(2, 50, 10)
-- version of HMDS (will work as a index+1 for memory lists)
local hmdsv = 0

-- floor memory locations
local mem_farmland = {0x023D83DC}

function main(attemps)

    function file_exists(name)
        local f=io.open(name,"r")
        if f~=nil then io.close(f) return true else return false end
     end

    if not file_exists("farmland.csv") then
        f = io.open(string.format("farmland.csv"), "w")
        f:write("Grass,Stone,Stick,Stump,Large_stone,Boulder,Moondrop_Flower,Pinkcat_Flower,Blue_Magic_Flower,Red_Magic_Flower,Toyflower,Blue_Grass,Green_Grass,Red_Grass,Yellow_Grass,Orange_Grass,Purple_Grass,Indigo_Grass,White_Grass,Toadstool_S,Matsutake_S,Bamboo_Shoot,Wild_Grape,Rotten_Wood,Lumber,Material_Stone,Golder_Lumber\n")
    else
        f = io.open(string.format("farmland.csv"), "a")
    end

    function cleanup()
        f:flush()
        io.close(f)
    end

    function empty_land()
        for fl = 0, 3101 do
            memory.writedword(mem_farmland[hmdsv]+(fl*4), "0x00000000")
        end
    end

    function count_items()
        items = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}

        for fl = 0, 3101 do
            tile = memory.readdword(mem_farmland[hmdsv]+(fl*4))

            tile_array = {}
            str= tostring(bit.tohex(tile))
            str:gsub(".",function(c) table.insert(tile_array,c) end)

            if tile_array[8] == "3" then
                if (("0x"..tile_array[5]) % 2 == 0) then
                    for tl = 0, 15 do
                        if (("0x"..tile_array[6])*1 == tl) then
                            items[tl+1] = items[tl+1] + 1
                        end
                    end
                else
                    for tl = 0, 15 do
                        if (("0x"..tile_array[6])*1 == tl) then
                            items[tl+1+16] = items[tl+1+16] + 1
                        end
                    end
                end
            end
        end
    end

    emu.registerexit(cleanup)

    savestate.create(3)

    for ml = 1, attemps do
        savestate.load(3)
        for fl = 0, ml do
            emu.frameadvance()
        end

        empty_land()

        joypad.set({A=true})
        for fl = 0, 200 do
            gui.text(0, 10, ml)
            emu.frameadvance()
        end
        count_items()
        emu.frameadvance()

        f:write(string.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", items[1], items[2], items[3], items[4], items[5], items[6], items[7], items[8], items[9], items[10], items[11], items[12], items[13], items[14], items[15], items[16], items[17], items[18], items[19], items[20], items[21], items[22], items[23], items[24], items[25], items[26], items[27]))
    end

    f:flush()
	io.close(f)
end

main(100)


-- pos = 0
-- -- Region 01, Main Farm
-- pos = section(6, 31, pos, "write", "0x00000008")
-- pos = section(5, 26, pos, "write", "0x00000008")
-- pos = section(7, 23, pos, "write", "0x00000008")
-- pos = section(13, 22, pos, "write", "0x00000008")
-- pos = section(5, 25, pos, "write", "0x00000008")
-- pos = section(4, 23, pos, "write", "0x00000008")


-- -- Region 02, left most field (some "grass" count as tiles)
-- pos = section(6, 4, pos, "write", "0x00000008")
-- pos = section(8, 4, pos, "write", "0x00000008")
-- pos = section(8, 3, pos, "write", "0x00000008")
-- pos = section(18, 4, pos, "write", "0x00000008")
-- pos = section(13, 3, pos, "write", "0x00000008")

-- -- Region 02, field nearest inn
-- pos = section(4, 9, pos, "write", "0x00000008")

-- -- Region 02, field nearest farm
-- pos = section(9, 6, pos, "write", "0x00000008")

-- -- Region 02, field nearest river (some "grass" count as tiles)
-- pos = section(7, 6, pos, "write", "0x00000008")


-- -- Region 03, field bottom left (some "grass" count as tiles)
-- pos = section(6, 18, pos, "write", "0x00000008")
-- pos = section(15, 9, pos, "write", "0x00000008")

-- -- Region 03, field close to exit towards bar
-- pos = section(6, 16, pos, "write", "0x00000008")

-- -- Region 03, field near beach
-- pos = section(13, 7, pos, "write", "0x00000008")

-- -- Region 03, field near exist towards inn
-- pos = section(7, 6, pos, "write", "0x00000008")


-- -- Region 06
-- pos = section(7, 17, pos, "write", "0x00000008")
-- pos = section(25, 6, pos, "write", "0x00000008")
-- pos = section(6, 25, pos, "write", "0x00000008")
-- pos = section(26, 8, pos, "write", "0x00000008")


-- -- Region 07, field near mine entrance
-- pos = section(9, 9, pos, "write", "0x00000008")
-- pos = section(8, 9, pos, "write", "0x00000008")
-- pos = section(8, 6, pos, "write", "0x00000008")
-- pos = section(11, 3, pos, "write", "0x00000008")

-- -- Region 07, field near river (some "grass" count as tiles)
-- pos = section(4, 12, pos, "write", "0x00000008")
-- pos = section(3, 11, pos, "write", "0x00000008")
-- pos = section(11, 16, pos, "write", "0x00000008")


-- -- Region 9
-- pos = section(10, 8, pos, "write", "0x00000008")
-- pos = section(4, 13, pos, "write", "0x00000008")
-- pos = section(7, 11, pos, "write", "0x00000008")
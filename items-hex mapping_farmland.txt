Little endian.
------------------------------------------------------
00 00 00 00 -> empty farmland
03 00 00 00 -> weed
03 03 03 00 -> stump, upper left
03 05 03 00 -> boulder, upper left
03 06 00 00 -> wild moondrop flower
03 07 00 00 -> wild pinkcat flower
03 08 00 00 -> wild blue magicred flower
03 09 00 00 -> wild red magicred flower
03 0A 00 00 -> wild toy flower
03 0B 00 00 -> blue grass
03 0C 00 00 -> green grass
03 0D 00 00 -> red grass
03 0E 00 00 -> yellow grass
03 0F 00 00 -> orange grass
03 10 00 00 -> purple grass
03 11 00 00 -> indigo grass
03 12 00 00 -> white grass
03 13 00 00 -> wild poisonous mushroom
03 14 00 00 -> wild matsutake mushroom
03 15 00 00 -> bamboo shoot
03 16 00 00 -> wild grape
03 23 00 00 -> stump, upper right
03 25 03 00 -> boulder, upper right
03 43 00 00 -> stump, bottom left
03 45 03 00 -> boulder, bottom left
03 63 00 00 -> stump, bottom right
03 65 03 00 -> boulder, bottom right
03 81 00 00 -> stone
03 82 00 00 -> stick
03 84 01 00 -> large stone, upper left
03 98 00 00 -> wood lumber
03 99 00 00 -> stone lumber
03 A4 01 00 -> large stone, upper right
03 C4 01 00 -> large stone, lower left
03 E4 01 00 -> large stone, lower right
03 1A 03 00 -> golden lumber
08 00 00 00 -> hoed square
10 00 00 00 -> watered square
13 97 00 00 -> wood lumber, decayed
------------------------------------------------------
Faster farmland works by varying the most significant
nybble in the third byte of the given element at each
stage of growth (seed, stage 1, stage 2 and stage 3).
Starting values that are lower than the starting 
value on the slowest piece of farmland cause the 
crop to grow quicker.
------------------------------------------------------ 
09 00 30 00 -> turnip seed, day 1
09 00 20 00 -> turnip seed, day 2
09 00 10 00 -> turnip seed, day 3
09 00 A0 00 -> turnip stage 1, day 1
09 00 90 00 -> turnip stage 1, day 2
29 00 00 01 -> mature turnip

09 01 40 00 -> potato seed, day 1
09 01 30 00 -> potato seed, day 2
09 01 20 00 -> potato seed, day 3
09 01 10 00 -> potato seed, day 4
09 01 C0 00 -> potato stage 1, day 1
09 01 B0 00 -> potato stage 1, day 2
09 01 A0 00 -> potato stage 1, day 3
09 01 90 00 -> potato stage 1, day 4
29 01 00 01 -> mature potato

09 02 50 00 -> cucumber seed, day 1
09 02 40 00 -> cucumber seed, day 2
09 02 30 00 -> cucumber seed, day 3
09 02 20 00 -> cucumber seed, day 4
02 02 10 00 -> cucumber seed, day 5
09 02 B0 00 -> cucumber stage 1, day 1
09 02 A0 00 -> cucumber stage 1, day 2
09 02 90 00 -> cucumber stage 1, day 3
09 02 20 01 -> cucumber stage 2, day 1
09 02 10 01 -> cucumber stage 2, day 2
29 02 80 01 -> mature cucumber

09 03 40 00 -> strawberry seed, day 1
09 03 30 00 -> strawberry seed, day 2
09 03 20 00 -> strawberry seed, day 3
09 03 10 00 -> strawberry seed, day 4
09 03 B0 00 -> stawberry stage 1, day 1
09 03 A0 00 -> strawberry stage 1, day 2
09 03 10 01 -> strawberry stage 2, day 1
29 03 80 01 -> mature strawberry

09 04 50 00 -> cabbage seed, day 1
09 04 40 00 -> cabbage seed, day 2
09 04 30 00 -> cabbage seed, day 3
09 04 20 00 -> cabbage seed, day 4
09 04 10 00 -> cabbage seed, day 5
09 04 D0 00 -> cabbage stage 1, day 1
09 04 C0 00 -> cabbage stage 1, day 2
09 04 B0 00 -> cabbage stage 1, day 3
09 04 A0 00 -> cabbage stage 1, day 4
09 04 90 00 -> cabbage stage 1, day 5
09 04 50 01 -> cabbage stage 2, day 1
09 04 40 01 -> cabbage stage 2, day 2
09 04 30 01 -> cabbage stage 2, day 3
09 04 20 01 -> cabbage stage 2, day 4
09 04 10 01 -> cabbage stage 2, day 5
29 04 80 01 -> mature cabbage

09 05 30 00 -> tomato seed, day 1
09 05 20 00 -> tomato seed, day 2
09 05 10 00 -> tomato seed, day 3
09 05 A0 00 -> tomato stage 1, day 1
09 05 90 00 -> tomato stage 1, day 2
09 05 20 01 -> tomato stage 2, day 1
09 05 10 01 -> tomato stage 2, day 2
09 05 B0 01 -> tomato stage 3, day 1
09 05 A0 01 -> tomato stage 3, day 2
09 05 90 01 -> tomato stage 3, day 3
29 05 00 02 -> mature tomato

09 06 40 00 -> corn seed, day 1
09 06 30 00 -> corn seed, day 2
09 06 20 00 -> corn seed, day 3
09 06 10 00 -> corn seed, day 4
09 06 C0 00 -> corn stage 1, day 1
09 06 B0 00 -> corn stage 1, day 2
09 06 A0 00 -> corn stage 1, day 3
09 06 90 00 -> corn stage 1, day 4
09 06 40 01 -> corn stage 2, day 1
09 06 30 01 -> corn stage 2, day 2
09 06 20 01 -> corn stage 2, day 3
09 06 10 01 -> corn stage 2, day 4
09 06 B0 01 -> corn stage 3, day 1
09 06 A0 01 -> corn stage 3, day 2
09 06 90 01 -> corn stage 3, day 3
29 06 00 02 -> mature corn

09 07 40 00 -> onion seed, day 1
09 07 30 00 -> onion seed, day 2
09 07 20 00 -> onion seed, day 3
09 07 10 00 -> onion seed, day 4
09 07 C0 00 -> onion stage 1, day 1
09 07 B0 00 -> onion stage 1, day 2
09 07 A0 00 -> onion stage 1, day 3
09 07 90 00 -> onion stage 1, day 4
29 07 00 01 -> mature onion

09 08 50 00 -> pumpkin seed, day 1
09 08 40 00 -> pumpkin seed, day 2
09 08 30 00 -> pumpkin seed, day 3
09 08 20 00 -> pumpkin seed, day 2
09 08 10 00 -> pumpkin seed, day 3
09 08 D0 00 -> pumpkin stage 1, day 1
09 08 C0 00 -> pumpkin stage 1, day 2
09 08 B0 00 -> pumpkin stage 1, day 3
09 08 A0 00 -> pumpkin stage 1, day 4
09 08 90 00 -> pumpkin stage 1, day 5
09 08 50 01 -> pumpkin stage 2, day 1
09 08 40 01 -> pumpkin stage 2, day 2
09 08 30 01 -> pumpkin stage 2, day 3
09 08 20 01 -> pumpkin stage 2, day 4
09 08 10 01 -> pumpkin stage 2, day 5
29 08 80 01 -> mature pumpkin

09 09 60 00 -> pineapple seed, day 1
09 09 50 00 -> pineapple seed, day 2
09 09 40 00 -> pineapple seed, day 3
09 09 30 00 -> pineapple seed, day 4
09 09 20 00 -> pineapple seed, day 5
09 09 10 00 -> pineapple seed, day 6
09 09 D0 00 -> pineapple stage 1, day 1
09 09 C0 00 -> pineapple stage 1, day 2
09 09 B0 00 -> pineapple stage 1, day 3
09 09 A0 00 -> pineapple stage 1, day 4
09 09 90 00 -> pineapple stage 1, day 5
09 09 50 01 -> pineapple stage 2, day 1
09 09 40 01 -> pineapple stage 2, day 2
09 09 30 01 -> pineapple stage 2, day 3
09 09 20 01 -> pineapple stage 2, day 4
09 09 10 01 -> pineapple stage 2, day 5
09 09 D0 01 -> pineapple stage 3, day 1
09 09 C0 01 -> pineapple stage 3, day 2
09 09 B0 01 -> pineapple stage 3, day 3
09 09 A0 01 -> pineapple stage 3, day 4
09 09 90 01 -> pineapple stage 3, day 5
29 09 00 02 -> mature pineapple

09 0A 40 00 -> eggplant seed, day 1
09 0A 30 00 -> eggplant seed, day 2
09 0A 20 00 -> eggplant seed, day 3
09 0A 10 00 -> eggplant seed, day 4
09 0A B0 00 -> eggplant stage 1, day 1
09 0A A0 00 -> eggplant stage 1, day 2
09 0A 90 00 -> eggplant stage 1, day 3
09 0A 30 01 -> eggplant stage 2, day 1
09 0A 20 01 -> eggplant stage 2, day 2
09 0A 10 01 -> eggplant stage 2, day 3
29 0A 80 01 -> mature eggplant

09 0B 40 00 -> carrot seed, day 1
09 0B 30 00 -> carrot seed, day 2
09 0B 20 00 -> carrot seed, day 3
09 0B 10 00 -> carrot seed, day 4
09 0B C0 00 -> carrot stage 1, day 1
09 0B B0 00 -> carrot stage 1, day 2
09 0B A0 00 -> carrot stage 1, day 3
09 0B 90 00 -> carrot stage 1, day 4
29 0B 00 01 -> mature carrot

09 0C 40 00 -> yam seed, day 1
09 0C 30 00 -> yam seed, day 2
09 0C 20 00 -> yam seed, day 3
09 0C 10 00 -> yam seed, day 4
09 0C A0 00 -> yam stage 1, day 1
09 0C 90 00 -> yam stage 1, day 2
29 0C 00 01 -> mature yam

09 0D 30 00 -> spinach seed, day 1
09 0D 20 00 -> spinach seed, day 2
09 0D 10 00 -> spinach seed, day 3
09 0D B0 00 -> spinach stage 1, day 1
09 0D A0 00 -> spinach stage 1, day 2
09 0D 90 00 -> spinach stage 1, day 3
29 0D 00 01 -> mature spinach

09 0E 30 00 -> bell pepper seed, day 1
09 0E 20 00 -> bell pepper seed, day 2
09 0E 10 00 -> bell pepper seed, day 3
09 0E 90 00 -> bell pepper stage 1, day 1
09 0E 20 01 -> bell pepper stage 2, day 1
09 0E 10 01 -> bell pepper stage 2, day 2
09 0E A0 01 -> bell pepper stage 3, day 1
09 0E 90 01 -> bell pepper stage 3, day 2
29 0E 00 02 -> mature bell pepper

09 0F 30 00 -> moondrop seed, day 1
09 0F 20 00 -> moondrop seed, day 2
09 0F 10 00 -> moondrop seed, day 3
09 0F A0 00 -> moondrop stage 1, day 1
09 0F 90 00 -> moondrop stage 1, day 2
09 0F 20 01 -> moondrop stage 2, day 1 
09 0F 10 01 -> moondrop stage 2, day 2
29 0F 80 01 -> mature moondrop

09 10 40 00 -> toy flower seed, day 1
09 10 30 00 -> toy flower seed, day 2
09 10 20 00 -> toy flower seed, day 3
09 10 10 00 -> toy flower seed, day 4
09 10 B0 00 -> toy flower stage 1, day 1
09 10 A0 00 -> toy flower stage 1, day 2
09 10 90 00 -> toy flower stage 1, day 3
09 10 30 01 -> toy flower stage 2, day 1
09 10 20 01 -> toy flower stage 2, day 2
09 10 10 01 -> toy flower stage 2, day 3
09 10 B0 01 -> toy flower stage 3, day 1
09 10 A0 01 -> toy flower stage 3, day 2
09 10 90 01 -> toy flower stage 3, day 3
29 10 00 02 -> mature toy flower

09 11 30 00 -> pinkcat seed, day 1
09 11 20 00 -> pinkcat seed, day 2
09 11 10 00 -> pinkcat seed, day 3
09 11 A0 00 -> pinkcat stage 1, day 1
09 11 90 00 -> pinkcat stage 1, day 2
09 11 20 01 -> pinkcat stage 2, day 1
09 11 10 01 -> pinkcat stage 2, day 2
29 11 80 01 -> mature pinkcat

09 12 40 00 -> blue magicred seeds, day 1
09 12 30 00 -> blue magicred seeds, day 2
09 12 20 00 -> blue magicred seeds, day 3
09 12 10 00 -> blue magicred seeds, day 4
09 12 B0 00 -> blue magicred stage 1, day 1
09 12 A0 00 -> blue magicred stage 1, day 2
09 12 90 00 -> blue magicred stage 1, day 3
09 12 20 01 -> blue magicred stage 2, day 1
09 12 10 01 -> blue magicred stage 2, day 2
09 12 A0 01 -> blue magicred stage 3, day 1
09 12 90 01 -> blue magicred stage 3, day 2
29 12 00 02 -> mature blue magicred

09 13 40 00 -> red magicred seeds, day 1
09 13 30 00 -> red magicred seeds, day 2
09 13 20 00 -> red magicred seeds, day 3
09 13 10 00 -> red magicred seeds, day 4
09 13 B0 00 -> red magicred stage 1, day 1
09 13 A0 00 -> red magicred stage 1, day 2
09 13 90 00 -> red magicred stage 1, day 3
09 13 20 01 -> red magicred stage 2, day 1
09 13 10 01 -> red magicred stage 2, day 2
09 13 A0 01 -> red magicred stage 3, day 1
09 13 90 01 -> red magicred stage 3, day 2
29 13 00 02 -> mature red magicred

09 14 40 00 -> grass seed, day 1
09 14 30 00 -> grass seed, day 2
09 14 20 00 -> grass seed, day 3
09 14 10 00 -> grass seed, day 4
09 14 40 01 -> grass stage 1, day 1
09 14 30 01 -> grass stage 1, day 2
09 14 20 01 -> grass stage 1, day 3
09 14 10 01 -> grass stage 1, day 4
09 14 C0 00 -> cut grass, day 1 / grass square during winter
09 14 B0 00 -> cut grass, day 2
09 14 A0 00 -> cut grass, day 3
09 14 90 00 -> cut grass, day 4
09 14 C0 01 -> grass stage 2, day 1
09 14 B0 01 -> grass stage 2, day 2
09 14 A0 01 -> grass stage 2, day 3
09 14 90 01 -> grass stage 2, day 4
29 14 00 02 -> mature grass

0C 00 00 36 -> shriveled turnip
0C 01 01 36 -> shriveled potato
0C 02 02 36 -> shriveled cucumber
0C 03 03 36 -> shriveled strawberry
0C 04 04 36 -> shriveled cabbage
0C 05 00 36 -> shriveled tomato
0C 06 01 36 -> shriveled corn
0C 07 04 36 -> shriveled onion
0C 08 02 36 -> shriveled pumpkin
0C 09 03 36 -> shriveled pineapple
0C 0F 05 36 -> shriveled moondrop
0C 10 06 36 -> shriveled toy flower
0C 11 05 36 -> shriveled pinkcat
------------------------------------------------------
0A 00 00 10 -> peach tree seed, lower left
0A 01 00 10 -> banana tree seed, lower left
0A 02 00 10 -> orange tree seed, lower left
0A 03 00 10 -> apple tree seed, lower left
02 04 00 10 -> grape tree stage 1 and 2, lower left
0A 04 00 10 -> grape tree seed, lower left

0A 00 00 18 -> peach tree seed, lower right
0A 01 00 18 -> banana tree seed, lower right
0A 02 00 18 -> orange tree seed, lower right
0A 03 00 18 -> apple tree seed, lower right
02 04 00 18 -> grape tree stage 1 and 2, lower right
0A 04 00 18 -> grape tree seed, lower right

0A 00 24 C0 -> peach tree seed, upper left
0A 01 28 C0 -> banana tree seed, upper left
0A 02 10 C0 -> orange tree seed, upper left
0A 03 28 C0 -> apple tree seed, upper left
0A 04 10 C0 -> grape tree seed, upper left
02 04 54 C1 -> grape tree stage 1, upper left
02 04 84 C2 -> grape tree stage 2, upper left

0A 00 10 C8 -> peach tree seed, upper right
0A 01 10 C8 -> banana tree seed, upper right
0A 02 10 C8 -> orange tree seed, upper right
0A 03 10 C8 -> grape tree seed, upper right
02 04 10 C8 -> grape tree stage 1 and 2, upper right
0A 04 10 C8 -> grape tree seed, upper right
------------------------------------------------------
FF FF FF FF -> wall or other unwalkable ground
# Libraries

## emulib

### void emu.frameadvance()
Advances the emulation by one frame.
This method can be used in conjunction with other methods such as joypad.set() to send precise inputs to the emulator.

### void emu.speedmode(SpeedMode mode)

### void emu.wait()

### void emu.pause()

### void emu.unpause()

### void emu.emulateframe()

### void emu.emulateframefastnoskipping()

### void emu.emulateframefast()

### void emu.emulateframeinvisible()

### void emu.redraw()

### int emu.framecount()

### int emu.lagcount()

### bool emu.lagged()

### bool emu.emulating()

### bool emu.atframeboundary()

### void emu.registerbefore(function func)

### void emu.registerafter(function func)

### void emu.registerstart(function func)

### void emu.registerexit(function func)

### void emu.persistglobalvariables(table variableTable)

### void emu.message(string str)

### void emu.print(...)

### void emu.openscript(string filename)

### void emu.reset()

### void emu.addmenu(string menuName, table menuEntries)

### void emu.setmenuiteminfo(function menuItem, table infoTable)

### void emu.registermenustart(function func)

## guilib

### void gui.register(function func)

### void gui.text(int x, int y, string str[, Color bgcolor='white'[, Color outline='black']])

### void gui.drawtext(int x, int y, string str[, Color bgcolor='white'[, Color outline='black']])

### void gui.box(int x1, int y1, int x2, int y2[, Color fill[, Color outline]])

### void gui.drawbox(int x1, int y1, int x2, int y2[, Color fill[, Color outline]])

### void gui.rect(int x1, int y1, int x2, int y2[, Color fill[, Color outline]])

### void gui.drawrect(int x1, int y1, int x2, int y2[, Color fill[, Color outline]])

### void gui.line(int x1, int y1, int x2, int y2[, Color bgcolor='white'[, bool skipfirst=false]])

### void gui.drawline(int x1, int y1, int x2, int y2[, Color bgcolor='white'[, bool skipfirst=false]])

### void gui.pixel(int x, int y[, Color bgcolor='white'])

### void gui.drawpixel(int x, int y[, Color bgcolor='white'])

### void gui.setpixel(int x, int y[, Color bgcolor='white'])

### void gui.writepixel(int x, int y[, Color bgcolor='white'])

### int gui.getpixel(int x, int y)

### int gui.readpixel(int x, int y)

### void gui.opacity(float alpha_0_to_1)

### void gui.transparency(float transparency_4_to_0)

### string gui.popup(string message[, string type='ok'[, string icon='message']])

### int gui.parsecolor(Color color)

### string gui.gdscreenshot([Screen whichScreen='both'])

### void gui.gdoverlay([int dx=0, int dy=0], string gdimage[, int sx=0, int sy=0, int width, int height][, float alphamul])

### void gui.drawimage([int dx=0, int dy=0], string gdimage[, int sx=0, int sy=0, int width, int height][, float alphamul])

### void gui.image([int dx=0, int dy=0], string gdimage[, int sx=0, int sy=0, int width, int height][, float alphamul])

### void emu.redraw()

### void gui.osdtext(int x, int y, string msg)

## styluslib

### table stylus.get()

### table stylus.read()

### table stylus.peek()

### void stylus.set(table inputtable)

### void stylus.write(table inputtable)

## statelib

### SavestateObj savestate.create([int location])

### void savestate.save(SavestateObj location[, string option])
The option parameter is not currently functional.

### void savestate.load(SavestateObj location[, string option])
The option parameter is not currently functional.

## memorylib
### int memory.readbyte(int address)

### int memory.readbyteunsigned(int address)

### int memory.readbytesigned(int address)

### int memory.readword(int address)

### int memory.readwordunsigned(int address)

### int memory.readshort(int address)

### int memory.readshortunsigned(int address)

### int memory.readwordsigned(int address)

### int memory.readshortsigned(int address)

### long memory.readdword(int address)

### long memory.readdwordunsigned(int address)

### long memory.readlong(int address)

### long memory.readlongunsigned(int address)

### long memory.readdwordsigned(int address)

### long memory.readlongsigned(int address)

### table memory.readbyterange(int address, int length)

### void memory.writebyte(int address, int value)

### void memory.writeword(int address, int value)

### void memory.writeshort(int address. int value)

### void memory.writedword(int address, int value)

### void memory.writelong(int address, int value)

### bool memory.isvalid(int address)

### int memory.getregister(string cpu_dot_registername_string)

### void memory.setregister(string cpu_dot_registername_string, int value)

### int vram.readword(int address)

### void vram.writeword(int address, int value)

### void memory.registerwrite(int address, [int size=1,][string cpuname='main',] function func)

### void memory.register(int address, [int size=1,][string cpuname='main',] function func)

### void memory.registerread(int address, [int size=1,][string cpuname='main',] function func)

### void memory.registerexec(int address, [int size=2,][string cpuname='main',] function func)

### void memory.registerrun(int address, [int size=2,][string cpuname='main',] function func)

### void memory.registerexecute(int address, [int size=2,][string cpuname='main',] function func)

## joylib

### table joypad.get()

### table joypad.read()

### table joypad.getdown()

### table joypad.readdown()

### table joypad.getup()

### table joypad.readup()

### table joypad.peek()

### table joypad.peekdown()

### table joypad.peekup()

### void joypad.set(table buttonTable)

### void joypad.write(table buttonTable)

## inputlib

### table input.get()

### table input.read()

### void input.registerhotkey(int keynum, function func)

### string input.popup(string message[, string type='yesno'[, string icon='question]])

## movielib

### bool movie.active()

### bool movie.recording()

### bool movie.playing()

### string movie.mode()

### int movie.length()

### string movie.name()

### string movie.getname()

### int movie.rerecordcount()

### void movie.setrerecordcount()

### bool movie.readonly()

### bool movie.getreadonly()

### void movie.setreadonly(bool readonly)

### int emu.framecount()

### void movie.play([string filename])

### void movie.open([string filename])

### void movie.playback([string filename])

### void movie.replay()

### void movie.stop()

### void movie.close()

## soundlib

### void sound.clear()

## bit_funcs

### int bit.tobit(float x)

### int bit.bnot(float x)

### int bit.band(float x1[, float x2...])

### int bit.bor(float x1[, float x2...])

### int bit.bxor(float x1[, float x2...])

### int bit.lshift(float x, int n)

### int bit.rshift(float x, int n)

### int bit.arshift(float x, int n)

### int bit.rol(float x, int n)

### int bit.ror(float x, int n)

### int bit.bswap(float x)

### string bit.tohex(float x[, float n=8])

## global functions

### void print()

### string tostring(...)

### int addressof(TableOrFunc table_or_function)

### table copytable(table origtable)
	
# Appendix
source: https://github.com/TASVideos/desmume/blob/master/desmume/src/lua-engine.cpp

## Meanings of symbols
Method arguments enclosed in square brackets `[]` are optional.

Default method arguments are indicated by `parametername=value`.

Ellipses used in conjuction with a data type indicate the method takes an arbitrary number of arguments of that type.

Ellipses without a data type indicate the method takes an arbitrary number of arguments of any type.

## Special data types
### SpeedMode
SpeedMode is an enum with 4 elements in the following order:
* SPEEDMODE_NORMAL
* SPEEDMODE_NOTHROTTLE
* SPEEDMODE_TURBO
* SPEEDMODE_MAXIMUM

### Color
Color can be one of the following:
* An integer
* A string
* A table

If a string, it must be one of the following:
* white
* black
* clear
* gray
* grey
* red
* orange
* yellow
* chartreuse
* green
* teal
* cyan
* blue
* purple
* magenta
* a hex string beginning with # (e.g. #FF0000)

### Screen
Screen can be one of the following:
* An integer
* A string

If an integer, it must be one of the following:
* -1 (for the top screen)
* 0 (for both screens)
* 1 (for the bottom screen)

If a string, it must be one of the following:
* top
* both
* bottom

### SavestateObj
SavestateObj can be one of the following:
* An integer (corresponding to save slots 1-9)
* A pointer to an allocated chunk of memory

### TableOrFunc
TableOrFunc can be one of the following:
* A table
* A function

## Methods with multiple return values
### gui.getpixel() 
Returns 3 integers, one for r, g, and b.
Invoke with `r, g, b = gui.getpixel(x, y)`.

### gui.parsecolor()
Returns 4 integers, one for r, g, b and alpha.
Invoke with `r, g, b, a = gui.parsecolor(color)`.
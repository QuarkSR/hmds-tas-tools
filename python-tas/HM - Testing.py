def meta_data():
	file.write("version 1\n")
	file.write("emuVersion 90900\n")
	file.write("rerecordCount 0\n")
	file.write("romFilename 0561 - Harvest Moon DS (U)(Legacy).nds\n")
	file.write("romChecksum 9590E91C\n")
	file.write("romSerial NTR-ABCE-USA\n")
	file.write("guid 84BE2329-6CE1-AED6-9052-49F1F1BBE9EB\n")
	file.write("useExtBios 0\n")
	file.write("advancedTiming 1\n")
	file.write("useExtFirmware 0\n")
	file.write("firmNickname yopyop\n")
	file.write("firmMessage DeSmuME makes you happy!\n")
	file.write("firmFavColour 10\n")
	file.write("firmBirthMonth 7\n")
	file.write("firmBirthDay 11\n")
	file.write("firmLanguage 1\n")
	file.write("rtcStartNew 2009-JAN-01 00:00:00:000 \n")
	file.write("comment author Exadrid\n")


def frame_loop(buttons, frames, comment="", p1="000", p2="000", ignore = 0):
	if touchscreen[0] != "" and touchscreen[1] != "" and p1 == "000" and p2 == "000" and ignore == 0:
		p1 = "%s" % touchscreen[0]
		p2 = "%s" % touchscreen[1]
	R = 'R' if 'R' in buttons else '.'
	L = 'L' if 'L' in buttons else '.'
	D = 'D' if 'D' in buttons else '.'
	U = 'U' if 'U' in buttons else '.'
	T = 'T' if 'T' in buttons else '.'
	S = 'S' if 'S' in buttons else '.'
	B = 'B' if 'B' in buttons else '.'
	A = 'A' if 'A' in buttons else '.'
	Y = 'Y' if 'Y' in buttons else '.'
	X = 'X' if 'X' in buttons else '.'
	W = 'W' if 'W' in buttons else '.'
	E = 'E' if 'E' in buttons else '.'
	if p1 == "000" and p2 == "000" :
		P = "0"
	else:
		P = "1"
	for i in range(0,frames):
		file.write("|0|%s%s%s%s%s%s%s%s%s%s%s%s.%s %s %s| %s\n" % (R,L,D,U,T,S,B,A,Y,X,W,E,p1,p2,P,comment))
	global frame
	frame = frame + frames


def enter_name(comment, p1, p2):
	frame_loop("", 1, comment, p1, p2)
	frame_loop("", 1, "Ok", "020", "180")
	frame_loop("", 2)
	frame_loop("", 1, "Yes", "037", "034")
	frame_loop("A", 1)


def enter_calendar():
	frame_loop("", 1, "2nd", "081", "081")
	frame_loop("", 1)
	frame_loop("", 1, "Spring", "078", "047")
	frame_loop("", 1)
	frame_loop("", 1, "OK", "007", "184")
	frame_loop("", 2)
	frame_loop("", 1, "Yes", "031", "028")
	frame_loop("A", 1)


def dialog(frames):
    frame_loop("B", frames-1, p1="244", p2="049")
    frame_loop("", 1)


def out_to_bed(delay=0):
    frame_loop("A", 1)
    frame_loop("BU", 40)
    frame_loop("", delay, "Rain delay")
    frame_loop("BL", 20)
    frame_loop("BU", 17)
    frame_loop("BL", 25)
    frame_loop("BU", 13)
    frame_loop("BL", 1)
    frame_loop("A", 1)
    frame_loop("", 29)
    frame_loop("A", 1)
    frame_loop("", 331)


def wake_to_sleep(delay=0):
    frame_loop("BU", 9)
    frame_loop("", delay, "Rain delay")
    frame_loop("BL", 21)
    frame_loop("A", 1)
    frame_loop("", 29)
    frame_loop("A", 1)
    frame_loop("", 331)


def leave_farm():
	frame_loop("BR", 82)
	frame_loop("BD", 107)


def hoe_milk():
	frame_loop("Y", 1)
	frame_loop("", 5)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 20)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 8)


def milk_horse(wait=0):
	frame_loop("Y", 1)
	frame_loop("", 8)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 53)
	for x in range(0, wait):
		frame_loop("", 1)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 8)
	frame_loop("A", 1)
	frame_loop("", 20)


file = open('D:\Dropbox\Speed Run\DeSmuME 9.11\Movies\HMT.dsm', 'w')
meta_data()

global frame
frame = 0
touchscreen = ["", ""]

# INTRO
frame_loop("", 354)
frame_loop("", 1, "Menu", "001", "001")
frame_loop("", 100, "New Game", "100", "100")
# dialog(205)
# enter_name("E is for Exadrid", "114", "047")
# dialog(153)
# enter_calendar()
# dialog(171)
# enter_name("F is for Farm", "048", "063")
# dialog(159)
# enter_name("D is for Dog", "098", "049")
# dialog(159)
# enter_name("C is for Cat", "084", "049")
# dialog(15990)
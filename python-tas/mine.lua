-- version of HMDS (will work as a index+1 for memory lists)
local hmdsv = 0

-- floor memory locations
local mem_farmland = {0x023D83DC}

function main(attemps)

    savestate.create(9)

    for a = 0, attemps do

        savestate.load(9)

        for fl = 0, a do
            gui.text(0, 10, a .. " (" .. math.floor(a/7) .. ") ")
            emu.frameadvance()
        end

        for fl = 0, 300 do
            gui.text(0, 10, a .. " (" .. math.floor(a/7) .. ") ")
            joypad.set({B=true})
            emu.frameadvance()
        end

    end

end

main(200)


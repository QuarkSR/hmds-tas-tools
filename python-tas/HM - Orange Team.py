def meta_data():
	file.write("version 1\n")
	file.write("emuVersion 90900\n")
	file.write("rerecordCount 0\n")
	file.write("romFilename 0561 - Harvest Moon DS (U)(Legacy).nds\n")
	file.write("romChecksum 9590E91C\n")
	file.write("romSerial NTR-ABCE-USA\n")
	file.write("guid 84BE2329-6CE1-AED6-9052-49F1F1BBE9EB\n")
	file.write("useExtBios 0\n")
	file.write("advancedTiming 1\n")
	file.write("useExtFirmware 0\n")
	file.write("firmNickname yopyop\n")
	file.write("firmMessage DeSmuME makes you happy!\n")
	file.write("firmFavColour 10\n")
	file.write("firmBirthMonth 7\n")
	file.write("firmBirthDay 11\n")
	file.write("firmLanguage 1\n")
	file.write("rtcStartNew 2009-JAN-01 00:00:00:000 \n")
	file.write("comment author Exadrid\n")


def frame_loop(buttons, frames, comment="", p1="000", p2="000", ignore = 0):
	if touchscreen[0] != "" and touchscreen[1] != "" and p1 == "000" and p2 == "000" and ignore == 0:
		p1 = "%s" % touchscreen[0]
		p2 = "%s" % touchscreen[1]
	R = 'R' if 'R' in buttons else '.'
	L = 'L' if 'L' in buttons else '.'
	D = 'D' if 'D' in buttons else '.'
	U = 'U' if 'U' in buttons else '.'
	T = 'T' if 'T' in buttons else '.'
	S = 'S' if 'S' in buttons else '.'
	B = 'B' if 'B' in buttons else '.'
	A = 'A' if 'A' in buttons else '.'
	Y = 'Y' if 'Y' in buttons else '.'
	X = 'X' if 'X' in buttons else '.'
	W = 'W' if 'W' in buttons else '.'
	E = 'E' if 'E' in buttons else '.'
	if p1 == "000" and p2 == "000" :
		P = "0"
	else:
		P = "1"
	for i in range(0,frames):
		file.write("|0|%s%s%s%s%s%s%s%s%s%s%s%s.%s %s %s| %s\n" % (R,L,D,U,T,S,B,A,Y,X,W,E,p1,p2,P,comment))
	global frame
	frame = frame + frames


def enter_name(comment, p1, p2):
	frame_loop("", 1, comment, p1, p2)
	frame_loop("", 1, "Ok", "020", "180")
	frame_loop("", 2)
	frame_loop("", 1, "Yes", "037", "034")
	frame_loop("A", 1)


def enter_calendar():
	frame_loop("", 1, "2nd", "081", "081")
	frame_loop("", 1)
	frame_loop("", 1, "Spring", "078", "047")
	frame_loop("", 1)
	frame_loop("", 1, "OK", "007", "184")
	frame_loop("", 2)
	frame_loop("", 1, "Yes", "031", "028")
	frame_loop("A", 1)


def dialog(frames):
    frame_loop("B", frames-1, p1="244", p2="049")
    frame_loop("", 1)


def out_to_bed(delay=0):
    frame_loop("A", 1)
    frame_loop("BU", 40)
    frame_loop("", delay, "Rain delay")
    frame_loop("BL", 20)
    frame_loop("BU", 17)
    frame_loop("BL", 25)
    frame_loop("BU", 13)
    frame_loop("BL", 1)
    frame_loop("A", 1)
    frame_loop("", 29)
    frame_loop("A", 1)
    frame_loop("", 331)


def wake_to_sleep(delay=0):
    frame_loop("BU", 9)
    frame_loop("", delay, "Rain delay")
    frame_loop("BL", 21)
    frame_loop("A", 1)
    frame_loop("", 29)
    frame_loop("A", 1)
    frame_loop("", 331)


def leave_farm():
	frame_loop("BR", 82)
	frame_loop("BD", 107)


def hoe_milk():
	frame_loop("Y", 1)
	frame_loop("", 5)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 20)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 8)


def milk_horse(wait=0):
	frame_loop("Y", 1)
	frame_loop("", 8)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 53)
	for x in range(0, wait):
		frame_loop("", 1)
	frame_loop("", 1, p1="102", p2="109")
	frame_loop("", 1, p1="061", p2="109")
	frame_loop("", 8)
	frame_loop("A", 1)
	frame_loop("", 20)


file = open('D:\Dropbox\Speed Run\DeSmuME 9.11\Movies\HM - Orange.dsm', 'w')
meta_data()

global frame
frame = 0
touchscreen = ["", ""]

# INTRO
frame_loop("", 354)
frame_loop("", 2, "Menu", "001", "001")
frame_loop("", 84)
frame_loop("", 1, "Rain delay")
frame_loop("", 1, "New Game", "100", "100")
dialog(205)
enter_name("E is for Exadrid", "114", "047")
dialog(153)
enter_calendar()
dialog(171)
enter_name("F is for Farm", "048", "063")
dialog(159)
enter_name("D is for Dog", "098", "049")
dialog(159)
enter_name("C is for Cat", "084", "049")
dialog(15994)

# Spring 2 (Monday)
frame_loop("BR", 4)
frame_loop("BD", 60)
dialog(273)
frame_loop("D", 1)
frame_loop("A", 1)
dialog(230)
frame_loop("", 272)
frame_loop("BR", 83)
frame_loop("BD", 108)
frame_loop("BR", 48)
frame_loop("BD", 72)
frame_loop("BR", 620)
frame_loop("BU", 355)
frame_loop("BR", 50)
frame_loop("U", 1)
frame_loop("", 249)
frame_loop("A", 1)
dialog(150)
frame_loop("BR", 123)
frame_loop("BU", 55)
frame_loop("BD", 50)
frame_loop("BU", 70)
dialog(2600)
frame_loop("BL", 95)
frame_loop("", 7)
frame_loop("BU", 70)
frame_loop("BL", 1)
frame_loop("BU", 1)
frame_loop("", 26)
frame_loop("BU", 12)
dialog(76)
frame_loop("", 3)
frame_loop("BR", 4)
dialog(76)
frame_loop("", 4)
frame_loop("BR", 4)
dialog(76)
frame_loop("", 8)
frame_loop("BR", 4)
dialog(76)
frame_loop("BD", 3)
frame_loop("", 31)
frame_loop("BD", 3)
dialog(76)
frame_loop("", 10)
frame_loop("BD", 3)
dialog(76)
frame_loop("BR", 4)
frame_loop("", 8)
frame_loop("BR", 3)
dialog(76)
frame_loop("", 16)
frame_loop("BR", 3)
dialog(76)
frame_loop("", 14)
frame_loop("BR", 3)
dialog(76)
frame_loop("BR", 7)
dialog(550)
frame_loop("BL", 120)
frame_loop("", 7)
frame_loop("", 2, p1="016", p2="030")
frame_loop("BR", 115)
frame_loop("BD", 80)
frame_loop("BU", 80)
dialog(2555)
frame_loop("BL", 21)
frame_loop("BU", 1)
frame_loop("", 28)
frame_loop("BU", 70)
frame_loop("", 34)
frame_loop("BU", 10)
dialog(76)
frame_loop("BL", 10)
frame_loop("BD", 13)
frame_loop("", 6)
frame_loop("BL", 10)
dialog(76)
frame_loop("BD", 5)
frame_loop("", 20)
frame_loop("BR", 10)
dialog(76)
frame_loop("BR", 11)
frame_loop("BU", 5)
frame_loop("", 45)
frame_loop("BU", 10)
dialog(76)
frame_loop("BU", 7)
frame_loop("BL", 21)
frame_loop("", 4)
frame_loop("BU", 7)
dialog(76)
frame_loop("BU", 14)
frame_loop("", 4)
frame_loop("BL", 38)
dialog(76)
frame_loop("BL", 31)
frame_loop("", 1)
frame_loop("BU", 15)
dialog(76)
frame_loop("BL", 7)
frame_loop("BD", 13)
frame_loop("BR", 5)
dialog(76)
frame_loop("BD", 5)
frame_loop("", 3)
frame_loop("BL", 30)
dialog(76)
frame_loop("BD", 7)
frame_loop("", 1)
frame_loop("BR", 13)
dialog(76)
frame_loop("BR", 15)
frame_loop("", 5)
frame_loop("BD", 20)
dialog(76)
frame_loop("BR", 10)
frame_loop("", 44)
frame_loop("BU", 7)
dialog(76)
frame_loop("BU", 9)
frame_loop("", 14)
frame_loop("BR", 5)
dialog(76)
frame_loop("BD", 17)
frame_loop("", 7)
frame_loop("BL", 14)
dialog(76)
frame_loop("BD", 17)
frame_loop("", 10)
frame_loop("BR", 5)
dialog(76)
frame_loop("BR", 17)
frame_loop("", 7)
frame_loop("BU", 14)
dialog(76)
frame_loop("BL", 18)
frame_loop("", 19)
frame_loop("BU", 20)
# frame_loop("", 7)
# frame_loop("BU", 14)

# Spring 3 (Tuesday)


# Spring 4 (Wednesday)


# Spring 5 (Thursday)


# Spring 6 (Friday)


# Spring 7 (Saturday)


# Spring 8 (Sunday)


# Spring 9 (Monday)


# Spring 10 (Tuesday)


# Spring 11 (Wednesday)


# Spring 12 (Thursday)


# Spring 13 (Friday)


# Spring 14 (Saturday)


# Spring 15 (Sunday)


# Spring 16 (Monday)


# Spring 17 (Tuesday)


# Spring 18 (Wednesday)


# Spring 19 (Thursday)


# Spring 20 (Friday)


# Spring 21 (Saturday)


# Spring 22 (Sunday)


# Spring 23 (Monday)


# Spring 24 (Tuesday)


# Spring 25 (Wednesday)


# Spring 26 (Thursday)


# Spring 27 (Friday)


# Spring 28 (Saturday)


# Spring 29 (Sunday)


# Spring 30 (Monday)


# Summer 1 (Tuesday)


# Summer 2 (Wednesday)


# Summer 3 (Thursday)


# Summer 4 (Friday)


# Summer 5 (Saturday)


# Summer 6 (Sunday)


# Summer 7 (Monday)


# Summer 6 (Sunday) - Summer 19 (Saturday)

# Summer 20 (Sunday)


# Summer 21 (Monday) - Fall 9 (Friday)


# Fall 9 (Friday)


# Fall 10 (Saturday)








file.close() 

















local coords = {}

function coords.coords(mapName)
	-- map internal ROM coords <-> emu coords
	-- special values:
	--     nil: not yet known
	local map = {
					-- mines
					mine0 = {x = {xScrollStart = 128, xScrollStop = 128}, y = {yScrollStart = 97, yScrollStop = 128}},
					mine1 = {x = {xScrollStart = 128, xScrollStop = 128}, y = {yScrollStart = 97, yScrollStop = 128}},
					mine2 = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					mine3 = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					mine4 = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					-- houses
					jacksHouse1 = {x = {xScrollStart = 128, xScrollStop = 128}, y = {yScrollStart = 97, yScrollStop = 128}},
					jacksHouse2 = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					jacksHouse3 = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					basement = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					takakurasHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					florasTent = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					celiasHouse1F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					celiasHouse2F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 112}},
					ninasHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					wallysHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					grantsHouse1F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					grantsHouse2F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					hardysClinic = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					rocksRoom = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 97}},
					luminasMansion1F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 240}},
					luminasMansion2F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 240}},
					luminasMansionKitchen = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 97}},
					codysHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					patricksHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 144}},
					daryllsLab = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					witchsHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					circusTent = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					-- farm buildings
					stable = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 112}},
					animalBarn = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					birdShed = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					mushroomHouse = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					makerShed = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					-- outside areas
					miningGrounds = {x = {xScrollStart = 128, xScrollStop = 639}, y = {yScrollStart = 97, yScrollStop = 528}},
					vestasFarm = {x = {xScrollStart = 128, xScrollStop = 1112}, y = {yScrollStart = 97, yScrollStop = 480}},
					circusGrounds = {x = {xScrollStart = 128, xScrollStop = 1407}, y = {yScrollStart = 97, yScrollStop = 512}},
					town = {x = {xScrollStart = 128, xScrollStop = 1407}, y = {yScrollStart = 97, yScrollStop = 480}},
					goddessPond = {x = {xScrollStart = 128, xScrollStop = 639}, y = {yScrollStart = 97, yScrollStop = 528}},
					farm = {x = {xScrollStart = 128, xScrollStop = 639}, y = {yScrollStart = 97, yScrollStop = 528}},
					mansionGrounds = {x = {xScrollStart = 128, xScrollStop = 383}, y = {yScrollStart = 97, yScrollStop = 528}},
					turtlePond = {x = {xScrollStart = 128, xScrollStop = 1407}, y = {yScrollStart = 97, yScrollStop = 512}},
					beach = {x = {xScrollStart = 128, xScrollStop = 383}, y = {yScrollStart = 97, yScrollStop = 480}},
					-- misc
					hiddenFarmland = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = nil, yScrollStop = nil}},
					vestasShop = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					inn1F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 161}},
					innKitchen = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					inn2F = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 240}},
					innVansCorridor = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 128, yScrollStop = 144}},
					blueBar = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 128}},
					blueBarBack = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 97}},
					muffysRoom = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 112}},
					spriteTree = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 208}},
					spriteCasino = {x = {xScrollStart = nil, xScrollStop = nil}, y = {yScrollStart = 97, yScrollStop = 192}}
				}
	
	local yScrollStop = map[mapName].y.yScrollStop
	local yScrollStart = map[mapName].y.yScrollStart
	local xScrollStop = map[mapName].x.xScrollStop
	local xScrollStart = map[mapName].x.xScrollStart
	
	local jackX = 0
	local jackY = 0
	
	if memory.readword(0x023D7AD0) > xScrollStop then
		jackX = 0 + memory.readword(0x023D7AD0) - (xScrollStop - xScrollStart)
	elseif memory.readword(0x023D7AD0) < xScrollStart then
		jackX = 0 + memory.readword(0x023D7AD0)
	else
		jackX = 0 + memory.readword(0x023D7AD0) - (memory.readword(0x023D7AD0) - xScrollStart)
	end
	
	if memory.readword(0x023D7AD2) > yScrollStop then
		jackY = -192 + memory.readword(0x023D7AD2) - (yScrollStop - yScrollStart)
	elseif memory.readword(0x023D7AD2) < yScrollStart then
		jackY = -192 + memory.readword(0x023D7AD2)
	else
		jackY = -192 + memory.readword(0x023D7AD2) - (memory.readword(0x023D7AD2) - yScrollStart)
	end
	
	coords = {x = jackX, y = jackY}
	--gui.drawline(jackX, -192, jackX, 192, "red")
	--gui.drawline(0, jackY, 255, jackY, "purple")
	
	return coords
end

return coords
local intro = {}

local com = require("common")

function intro.waitUntilInput()
	r, g, b, a = 0
	while r ~= 255 and g ~= 247 and b ~= 115 do
		if com.debug then print("Input unavailable") end
		emu.frameadvance()
		r, g, b, a = gui.getpixel(63, 127) -- the specific coords don't matter as long as it's yellow ONLY when input is accepted
	end
	if com.debug then print("Input available") end
end

function intro.inputName()
	-- tap "I" tile, then "OK" tile, then select "yes"
	local iTileXCoord = 100
	local iTileYCoord = 64
	local okTileXCoord = 16
	local okTileYCoord = 180
	
	stylus.set({x=iTileXCoord, y=iTileYCoord, touch=true})
	emu.frameadvance()
	if com.debug then print("Tapped i") end
	stylus.set({x=okTileXCoord, y=okTileYCoord, touch=true})
	emu.frameadvance()
	if com.debug then print("Tapped OK") end
	emu.frameadvance()
	emu.frameadvance()
	joypad.set({up=true})
	emu.frameadvance()
	joypad.set({A=true})
	emu.frameadvance()
	if com.debug then print("Finished inputting name") end
end

function intro.inputBirthday()
	local springTileXCoord = 80
	local springTileYCoord = 50
	local firstTileXCoord = 50
	local firstTileYCoord = 80
	local okTileXCoord = 16
	local okTileYCoord = 180

	stylus.set({x=springTileXCoord, y=springTileYCoord, touch=true})
	emu.frameadvance()
	emu.frameadvance()
	stylus.set({x=firstTileXCoord, y=firstTileYCoord, touch=true})
	emu.frameadvance()
	stylus.set({x=okTileXCoord, y=okTileYCoord, touch=true})
	emu.frameadvance()
	emu.frameadvance()
	emu.frameadvance()
	joypad.set({up=true})
	emu.frameadvance()
	joypad.set({A=true})
	emu.frameadvance()
end

function intro.intro()
	os.execute("date 01-01-2000 00:00 & time 00:00")
	emu.reset()

	com.waitNFrames(354)
	stylus.set({x=1, y=1, touch=true})
	emu.frameadvance()

	com.waitNFrames(84)
	stylus.set({x=1, y=1, touch=true})
	emu.frameadvance()

	-- text boxes 1-2
	com.advanceThroughTextBoxes(2)
	intro.waitUntilInput()
	intro.inputName()

	-- text box 3
	com.advanceThroughTextBoxes(1)
	intro.waitUntilInput()
	intro.inputBirthday()

	-- text box 4
	com.advanceThroughTextBoxes(1)
	intro.waitUntilInput()
	intro.inputName()

	-- text box 5
	com.advanceThroughTextBoxes(1)
	intro.waitUntilInput()
	intro.inputName()

	-- text box 6
	com.advanceThroughTextBoxes(1)
	intro.waitUntilInput()
	intro.inputName()

	-- text boxes 7-94 (opening cinematic)
	com.advanceThroughTextBoxes(89)

	-- leave house
	com.leaveHouse()

	-- mayor
	com.advanceThroughTextBoxes(3)
	emu.frameadvance()
	emu.frameadvance()
	joypad.set({down=true})
	emu.frameadvance()
	joypad.set({A=true})
	emu.frameadvance()

	com.advanceThroughTextBoxes(2)
end

intro.intro()

return intro
com = require("common")
gd = require("gd")

function clearfile()
	f = io.open("weatherdata.csv", "w")
	f:close()
end

function main()
	os.execute("date 01-01-2000 & time 00:00")
	clearfile()
	emu.unpause()
	local state = savestate.create()
	savestate.save(state)
	
	for i = 1, 100 do
		print("Waiting", i, "frames")
		com.waitNFrames(i)
		joypad.set({A=true})
		emu.frameadvance()
		com.waitNFrames(322)
		stylus.set({x=16, y=180, touch=true})
		emu.frameadvance()
		stylus.set({touch=false})
		com.waitNFrames(17)
		stylus.set({x=80, y=90, touch=true})
		emu.frameadvance()
		stylus.set({touch=false})
		emu.frameadvance()
		com.advanceThroughTextBoxes(1)
		r, g, b = gui.getpixel(195, 105)
		
		f = io.open("weatherdata.csv", "a")
		if r == 255 and g == 214 and b == 0 then
			print(r, g, b, "-- sunny")
			f:write(string.format("%d, %d %d %d, sunny, %s\n", i, r, g, b, os.date("%Y-%m-%d %H:%M")))
		elseif r == 66 and g == 173 and b == 239 then 
			print(r, g, b, "-- rainy")
			f:write(string.format("%d, %d %d %d, rainy, %s\n", i, r, g, b, os.date("%Y-%m-%d %H:%M")))
		else 
			print(r, g, b, "-- some other weather")
			filename = os.date("dbg/%Y-%m-%d %H;%M.png")
			gd.createFromGdStr(gui.gdscreenshot()):png(filename)
			f:write(string.format("%d, %d %d %d, some other weather, %s\n", i, r, g, b, os.date("%Y-%m-%d %H:%M")))
		end
		f:close()
		
		savestate.load(state)
	end
	emu.pause()
end

main()
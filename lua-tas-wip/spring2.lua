local spring2 = {}

function getJackCoords_Mine1

function spring2.sp2()
	com = require("common")
	intro = require("intro")
	intro.intro()
	-- Run to mine
	com.farmToTown()
	com.townToVesta()
	com.vestaToMine()
	com.enterTent()
end

return spring2
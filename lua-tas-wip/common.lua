local common = {}

common.verbose = false
common.debug = false

function common.advanceThroughTextBoxes(amount)
	for i = 1, amount do
		if common.verbose then print("Textbox", i, "/", amount) end
		while not common.greenArrowExists() do -- while text is being rendered
			-- hold L each frame to speed up text rendering
			joypad.set({L=true})
			emu.frameadvance()
		end
		stylus.set({touch=false})
		emu.frameadvance()
		-- text is done rendering for this text box, press A to move on to the next one
		if common.verbose then print("Tapping A") end
		joypad.set({L=true, A=true})
		emu.frameadvance()
		while common.greenArrowExists() do -- the arrow stays on screen for a few frames after you press A
			emu.frameadvance()
		end
	end
end

function common.greenArrowExists()
	local r, g, b = gui.getpixel(244, 50)
	if (r == 140 and g == 255 and b == 189) or (r == 0 and g == 231 and b == 99) then
		if common.verbose then print("\tFound arrow on frame", emu.framecount()) end
		return true
	else
		return false
	end
end

function common.waitNFrames(n)
	if common.verbose then print("Waiting", n, "frames") end
	for i = 1, n do
		emu.frameadvance()
	end
end

function common.moveX(x, prevX)
	local xNow = memory.readword(0x023D7AD0)

	if x % 2 ~= xNow % 2 then
		joypad.set({right=true})
		common.waitNFrames(4)
		joypad.set({right=true})
		common.waitNFrames(4)
		joypad.set({left=true})
		emu.frameadvance()
	end

	--if xNow == prevX then
	--	common.getUnstuck()
	--end

	if xNow > x then
		joypad.set({B=true, left=true})
	elseif xNow < x then
		joypad.set({B=true, right=true})
	else
		return
	end
	emu.frameadvance()
	
	common.moveX(x, xNow)
end

function common.moveY(y, prevY)
--	local prevY = nil
--	while memory.readword(0x023D7AD2) ~= y do
--		if common.debug then print("Y coord:", memory.readword(0x023D7AD2)) end
--		
--		if memory.readword(0x023D7AD2) == prevY then
--			if common.debug then print("Y stuck") end
--			common.pickup()
--			common.pocket()
--			common.moveY(memory.readword(0x023D7AD2) + 1)
--			common.useTool("hammer")
--			common.moveY(memory.readword(0x023D7AD2) + 1)
--			common.useTool("axe")
--			common.moveY(memory.readword(0x023D7AD2) + 1)
--		end
--		if memory.readword(0x023D7AD2) > y then
--			joypad.set({B=true, up=true})
--		else
--			joypad.set({B=true, down=true})
--		end
--		prevY = memory.readword(0x023D7AD2)
--		emu.frameadvance()
--	end
	local yNow = memory.readword(0x023D7AD2)

	if y % 2 ~= yNow % 2 then
		joypad.set({down=true})
		common.waitNFrames(4)
	end
	
	--if yNow == prevY then
	--	common.getUnstuck()
	--end
	
	if yNow > y then
		joypad.set({B=true, up=true})
	elseif yNow < y then
		joypad.set({B=true, down=true})
	else
		return
	end
	emu.frameadvance()
	
	common.moveY(y, yNow)
end

-- TODO: actually implement this
function common.getUnstuck()
	common.useTool("hammer")
	common.pocket()
end

function common.pickup()
	joypad.set({A=true})
	emu.frameadvance()
	emu.frameadvance()
	common.pocket()
end

function common.pocket()
	if memory.readbyteunsigned(0x023B5A88) ~= 255 then
		joypad.set({Y=true})
		emu.frameadvance()
		common.waitNFrames(30)
	end
end

function common.useTool(tool)
	toolToID = {axe = 0, hammer = 2, hoe = 3, sickle = 4, wateringCan = 5, turnip = 199, empty = 255}
	
	while memory.readbyteunsigned(0x023B5A80) ~= toolToID[tool] do
		common.switchTool()
	end
	joypad.set({Y=true})
	common.waitNFrames(30)
end

function common.switchTool()
	joypad.set({R=true})
	emu.frameadvance()
	joypad.set({R=true, Y=true})
	emu.frameadvance()
	common.waitNFrames(20)
end

function common.leaveHouse()
	common.moveX(104)
	common.moveY(217)
end

function common.farmToTown()
	common.moveX(568)
	common.moveY(617)
end

function common.townToGoddessPond()
	common.moveY(81)
	common.moveX(1392)
	common.moveY(6)
end

function common.townToPondR()
	common.moveY(81)
	common.moveX(1192)
	common.moveY(224)
	common.moveX(1240)
	common.moveY(569)
end

function common.townToPondL()
	common.moveY(169)
	common.moveX(721)
	common.moveY(425)
	common.moveX(631)
	common.moveY(568)
end	

function common.pondToBeach()
	if memory.readword(0x023D7AD0) >= 1167 then
		print("In if statement")
		common.moveY(65)
		common.moveX(1247)
		common.moveY(385)
		common.moveX(1143)
	else
		common.moveY(432)
		common.moveX(896)
	end
	common.moveY(601)
end

function common.townToVesta()
	common.moveY(257)
	common.moveX(1528)
end

function common.vestaToCircusL()
	common.moveX(48)
	common.moveY(569)
end

function common.vestaToCircusR()
	common.moveX(800)
	common.moveY(569)
end

function common.townToMansion()
	common.moveY(169)
	common.moveX(295)
	common.moveY(6)
end

function common.vestaToMine()
	common.moveX(744)
	common.moveY(6)
end

-- TODO: mine -> Carter's tent, mine -> vesta -> town -> home etc

return common
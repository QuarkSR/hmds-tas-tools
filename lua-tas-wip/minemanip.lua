com = require("common")
coords = require("jackCoords")
gd = require("gd")

function atPrompt()
	r, g, b = gui.getpixel(18, 50)
	
	if (r == 148 and g == 148 and b == 148) or (r == 198 and g == 198 and b == 198) or (r == 231 and g == 231 and b == 231) then
		return true
	else
		return false
	end
end

function collectResources()
	-- Get to top-left corner
	com.moveX(23)
	com.moveY(80)
	com.useTool("hoe")
	com.pocket()
	com.moveY(72)
	
	
	--for i = 1, 7 do
	--	for j = 1, 14 do
	--		com.useTool("hoe")
	--		com.moveX(memory.readbyte(0x023D7AD0) - 8)
	--	end
	--	com.moveY(memory.readbyte(0x023D7AD2) - 8)
	--	if i % 2 == 0 then com.moveX(memory.readbyte(0x023D7AD0) + 8) else com.moveX(memory.readbyte(0x023D7AD0) - 8) end
	--end
end

function main()
	local state = savestate.create()
	savestate.save(state)
	
	joypad.set({B=true, down=true})
	emu.frameadvance()
	com.advanceThroughTextBoxes(1)
	com.waitNFrames(30)
	collectResources()
end

main()
hmdsv = 1 -- 1 for v1.0 and 2 for v1.1

-- Location
mem_char_loc = {0x023D7AD8}
mem_screen_loc = {0x023D3AC0}

-- Sprites
mem_sprite = {0x023DC54C}

-- Weather
mem_todays_weather = {0x023D6B00}
mem_tomorrows_weather = {0x023D6B02}

-- Inventory
mem_green = {0x023D6B14}
mem_backpack = {0x023D6B28}
mem_inv18= {0x023D6B6C} -- Fix this to be programmically defined (mem_inv1 to mem_inv18)

-- Screen
mem_screenX = {0x023D3E71}
mem_screenY = {0x023D3E75}
mem_charX = {0x023D7AD4}
mem_charY = {0x023D7AD6}

-- Character info
mem_stamina = {0x023D3DEC}
mem_fatigue = {0x023D3DE8}
mem_gold = {0x023D6B08}
mem_medals = {0x023D6B0C}
mem_minute = {0x021CE53F}

-- Counters
mem_shop = {0x023DE2B6}
mem_grass = {0x023DE2A7}

-- Tool experience
mem_axe = {0x023DDF24}
mem_rod = {0x023DDF26}
mem_hammer = {0x023DDF28}
mem_sickle = {0x023DDF2C}
mem_can = {0x023DDF2E}
mem_hoe = {0x023DDF2A}
mem_milker = {0x023DDF30}

-- NPC
mem_NPCArray = {0x023DBD30}

-- Mines
mem_floorX = {0x022C3EC4, 0x022C6C84}
mem_floorY = {0x022C3EC5, 0x022C6C86}
mem_floorArr = {0x023DB964, 0x023DB960}

-- Farmland
mem_farmland = {0x023D83DC}

-- Fish list
mem_fishlist = {0x023DDFAC}
frontend = {}

function frontend.showSpawnWindow()
	tabindex = 0

	btn = iup.button{title="OK"}

	----------------------------------------------------------------Lists-------------------------------------------------------------------------------

	quantityList = iup.list{1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
							11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
							21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
							31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
							41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
							51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
							61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
							71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
							81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
							91, 92, 93, 94, 95, 96, 97, 98, 99, 100; 
							dropdown="YES", expand="NO", value=1}

	toolLists =
	{
		tools = iup.list{"Axe", "Fishing rod", "Hammer", "Hoe", "Sickle", "Watering can", "Clipper", "Milker", "Sword", "Medicine", "Bell", "Blue feather", "Brush", "Cow miracle potion", "Sheep miracle potion"; dropdown="YES", expand="YES", value=1},
		qualities = iup.list{"Stone", "Copper", "Silver", "Gold", "Mystrile", "Cursed", "Blessed", "Mythic"; dropdown="YES", expand="NO", value=1}
	}
	
	fishLists =
	{
		fish = iup.list{"Fish bones", "Boot", "Bottle", "Empty can", "Fish fossil", "Pirate treasure", "Fish S", "Fish M", "Fish L", "Branch", "Fish kings"; dropdown="YES", expand="YES", value=1},
		fishKings = iup.list{"Angler fish", "Coelacanth", "Squid", "Huchen", "Carp", "Spa catfish"; dropdown="YES", expand="NO", value=1, active="NO"}
	}
	
	mineLists =
	{
		mines = iup.list{"Mine 0", "Mine 1", "Mine 2", "Mine 3", "Mine 4"; dropdown="YES", expand="NO", value=1},
		
		mine0 = 
		{
			mine = iup.list{"Coin bag", "Bracelet", "Brooch", "Dress", "Earrings", "Facial pack", "Choker", "Perfume", "Skin lotion", "Sun block"; dropdown="YES", expand="NO", size="200x", value=1}
		},
		
		mine1 = 
		{
			mine = iup.list{"Junk ore", "Coin bag", "Copper", "Silver", "Gold", "Mystrile", "Black grass"; dropdown="YES", expand="NO", size="200x", value=1}
		},
		
		mine2 =
		{	
			mine = iup.list{"Sword", "Junk ore", "Lithograph", "Coin bag", "Agate", "Fluorite", "Moon stone", "Amethyst", "Emerald", "Ruby", "Topaz", "Diamond", "Peridot", "Pink diamond", "Black grass", "Sand rose", "Alexandrite"; dropdown="YES", expand="NO", size="200x", value=1},
			alexandriteVarieties = iup.list{"Green", "Red"; dropdown="YES", expand="YES", value=1, active="NO"}
		},
		
		mine3 =
		{
			mine = iup.list{"Junk ore", "Lithograph", "Coin bag", "Copper", "Silver", "Gold", "Mystrile", "Orichalc", "Mythic stone", "Adamantite", "Black grass", "Spring sun", "Summer sun", "Fall sun", "Winter sun", "Cursed tools", "Cursed accessories"; dropdown="YES", expand="NO", size="200x", value=1},
			cursedTools = iup.list{"Axe", "Fishing rod", "Hammer", "Hoe", "Sickle", "Watering can"; dropdown="YES", expand="YES", value=1, active="NO"},
			cursedAccessories = iup.list{"Slow shoes", "Goddess earrings", "Kappa earrings", "Witch earrings", "Friendship pendant", "Goddess pendant", "Kappa pendant", "Time ring", "Goddess hat", "Witch hat"; dropdown="YES", expand="YES", value=1, active="NO"}
		},
		
		mine4 = 
		{
			mine = iup.list{"Goddess' gift", "Lithograph", "Yellow wonderful", "Orange wonderful", "Blue wonderful", "Green wonderful", "Indigo wonderful", "Purple wonderful", "Red wonderful", "White wonderful", "Black wonderful", "Bronze coin", "Silver coin", "Gold coin", "Black grass"; dropdown="YES", expand="NO", size="200x", value=1}
		}
	}
	
	makerLists =
	{
		makers = iup.list{"Yarn", "Cheese", "Yoghurt", "Mayonnaise"; dropdown="YES", expand="YES", value=1, active="YES"},
		sizes = iup.list{"S", "M", "L"; dropdown="YES", expand="NO", value=1, active="YES"}
	}
	
	produceLists =
	{
		produce = iup.list{"Wool", "Milk", "Chicken egg", "Duck egg"; dropdown="YES", expand="YES", value=1},
		sizes = iup.list{"S", "M", "L"; dropdown="YES", expand="NO", value=1}
	}
	
	kitchenLists =
	{
		utensils = iup.list{"Frying pan", "Cooking pot", "Oven", "Mixer", "Steamer", "No utensil"; dropdown="YES", expand="NO", value=1},
		
		pan = 
		{
			pan = iup.list{"Stir fry", "Fried rice", "Savory pancake", "French fries", "Croquette", "Popcorn", "Cornflakes", "Happy eggplant", "Scrambled eggs", "Omelet", "Omelet rice", "Apple souffle", "Curry bread", "French toast", "Doughnut", "Grilled fish", "Fried thick noodles", "Fried noodles", "Tempura", "Pancake", "Pot sticker", "Risotto", "Dry curry", "Failed dish"; dropdown="YES", expand="YES", value=1}
		},
		
		pot = 
		{
			pot = iup.list{"Hot milk", "Hot chocolate", "Wild grape wine", "Pumpkin stew", "Fish stew", "Boiled spinach", "Boiled egg", "Candied potato", "Dumplings", "Cheese fondue", "Noodles", "Curry noodles", "Tempura noodles", "Buckwheat noodles", "Tempura buckwheat noodles", "Mountain stew", "Rice soup", "Porridge", "Tempura rice", "Egg over rice", "Stew", "Relaxtea", "Turbojolt", "Bodigizer", "Failed dish", "Jams", "Curries"; dropdown="YES", expand="YES", value=1},
			jams = iup.list{"Strawberry jam", "Apple jam", "Grape jam", "Marmalade"; dropdown="YES", expand="YES", value=1, active="NO"},
			curries = iup.list{"Curry rice", "Blue curry", "Green curry", "Red curry", "Yellow curry", "Orange curry", "Purple curry", "Indigo curry", "Black curry", "White curry", "Rainbow curry", "Ultimate curry", "Finest curry"; dropdown="YES", expand="YES", value=1, active="NO"}
		},
		
		oven = 
		{
			oven = iup.list{"Baked corn", "Toasted rice balls", "Roasted rice cake", "Baked yam", "Toast", "Jambun", "Dinner roll", "Pizza", "Doria", "Gratin", "Buckwheat ball", "Sweet potatoes", "Cookies", "Chocolate cookies", "Cake", "Chocolate cake", "Cheesecake", "Apple pie", "Failed dish"; dropdown="YES", expand="YES", value=1}
		},
		
		mixer =
		{
			mixer = iup.list{"Ketchup", "Butter", "Fish sticks", "Turbojolt XL", "Bodigizer XL", "Failed dish", "Juices"; dropdown="YES", expand="YES", value=1},
			juices = iup.list{"Pineapple juice", "Tomato juice", "Grape juice", "Peach juice", "Banana juice", "Orange juice", "Apple juice", "Strawberry milk", "Fruit juice", "Fruit latte", "Vegetable juice", "Vegetable latte", "Mixed juice", "Mixed latte"; dropdown="YES", expand="YES", value=1, active="NO"}
		},
		
		steamer = 
		{
			steamer = iup.list{"Moon dumplings", "Green dumplings", "Bamboo dumplings", "Steamed bun", "Cheese steamed bun", "Shaomi", "Steamed egg", "Chinese bun", "Curry bun", "Steamed dumplings", "Sponge cake", "Steamed cake", "Pudding", "Pumpkin pudding", "Failed dish"; dropdown="YES", expand="YES", value=1}
		},
		
		none = 
		{
			none = iup.list{"Small mayonnaise", "Medium mayonnaise", "Large mayonnaise", "Salad", "Sandwich", "Fruit sandwich", "Pickled turnips", "Pickled cucumber", "Bamboo rice", "Matsutake rice", "Mushroom rice", "Sushi", "Raisin bread", "Sashimi", "Chirashi sushi", "Buckwheat chips", "Ice cream", "Relaxtea leaves", "Elli grass", "Failed dish"; dropdown="YES", expand="YES", value=1}
		}
	}
	
	seedLists =
	{
		seasons = iup.list{"Spring crops", "Summer crops", "Autumn crops", "Mushrooms"; dropdown="YES", expand="NO", value=1},
		
		spring = 
		{
			spring = iup.list{"Turnip seeds", "Potato seeds", "Cucumber seeds", "Strawberry seeds", "Cabbage seeds", "Moondrop seeds", "Toy seeds", "Grass seeds"; dropdown="YES", expand="YES", value=1}
		},
		
		summer = 
		{
			summer = iup.list{"Tomato seeds", "Corn seeds", "Onion seeds", "Pumpkin seeds", "Pineapple seeds", "Pinkcat seeds", "Peach seeds", "Banana seeds", "Orange seeds", "Grass seeds"; dropdown="YES", expand="YES", value=1}
		},
		
		autumn =
		{
			autumn = iup.list{"Eggplant seeds", "Carrot seeds", "Yam seeds", "Spinach seeds", "Bell pepper seeds", "Magic red seeds", "Apple seeds", "Grape seeds", "Grass seeds"; dropdown="YES", expand="YES", value=1}
		},
		
		mushrooms = 
		{
			mushrooms = iup.list{"Shiitake seeds", "Matsutake seeds", "Toadstool seeds"; dropdown="YES", expand="YES", value=1}
		}
	}
	
	cropLists =
	{
		seasons = iup.list{"Spring crops", "Summer crops", "Autumn crops", "Mushrooms"; dropdown="YES", expand="NO", value=1},
		
		spring =
		{
			spring = iup.list{"Turnip", "Potato", "Cucumber", "Strawberry", "Cabbage", "Moondrop flower", "Toy flower", "Fodder"; dropdown="YES", expand="YES", value=1}
		},
		
		summer = 
		{
			summer = iup.list{"Tomato", "Corn", "Onion", "Pumpkin", "Pineapple", "Pinkcat flower", "Peach", "Banana", "Orange", "Fodder"; dropdown="YES", expand="YES", value=1}
		},
		
		autumn = 
		{
			autumn = iup.list{"Eggplant", "Carrot", "Yam", "Spinach", "Bell pepper", "Red magicred flower", "Blue magicred flower", "Apple", "Grape", "Fodder"; dropdown="YES", expand="YES", value=1}
		},
		
		mushrooms =
		{
			mushrooms = iup.list{"Shiitake", "Matsutake", "Toadstool"; dropdown="YES", expand="YES", value=1},
			sizes = iup.list{"S", "M", "L"; dropdown="YES", expand="NO", value=1, active="NO"}
		}
	}
	
	seasonalPickupsLists =
	{
		seasons = iup.list{"Spring", "Summer", "Autumn", "Winter"; dropdown="YES", expand="NO", value=1},
		
		spring =
		{
			spring = iup.list{"Moondrop flower", "Toy flower", "Yellow grass", "Orange grass", "Bamboo shoot", "Weed", "Branch", "Stone"; dropdown="YES", expand="YES", value=1}
		},
		
		summer =
		{
			summer = iup.list{"Pinkcat flower", "Blue grass", "Green grass", "Indigo grass", "Purple grass", "Red grass", "Wild grape", "Weed", "Branch", "Stone"; dropdown="YES", expand="YES", value=1}
		},
		
		autumn =
		{
			autumn = iup.list{"Blue magicred flower", "Red magicred flower", "Matsutake S", "Toadstool S", "Orange grass", "Red grass", "Weed", "Branch", "Stone"; dropdown="YES", expand="YES", value=1}
		},
		
		winter =
		{
			winter = iup.list{"White grass", "Weed", "Branch", "Stone"; dropdown="YES", expand="YES", value=1}
		}
	}
	
	accessoriesLists =
	{
		accessories = iup.list{"Teleport stone", "Pedometer", "Clock", "Red cloak", "Truth bangle", "Love bangle", "Godhand", "Miracle gloves", "Touch screen gloves", "Necklace", "Goddess earrings", "Kappa earrings", "Goddess hat", "Kappa hat", "Witch earrings", "Time ring", "Boots", "Goddess pendant", "Kappa pendant", "Friendship pendant"; dropdown="YES", expand="YES", value=1},
		
		qualities =
		{
			necklace = iup.list{"Base", "Copper", "Silver", "Gold", "Mystrile", "Blessed", "Mythic"; dropdown="YES", expand="NO", value=1, active="NO"},
			hatsAndEarrings = iup.list{"Red", "Blue", "Cursed", "Blessed"; dropdown="YES", expand="NO", value=1, active="NO"},
			mine3 = iup.list{"Cursed", "Blessed"; dropdown="YES", expand="NO", value=1, active="NO"},
			other = iup.list{"White", "Green", "Cursed", "Blessed"; dropdown="YES", expand="NO", value=1, active="NO"}
		}
	}
	
	miscLists =
	{
		misc = iup.list{"Fodder", "Goddess' gift", "Title bonus", "Witch's gift", "Bird feed", "10K ticket", "1M ticket", "Shaved ice", "Spa-boiled egg", "Buckwheat flour", "Butter", "Chocolate", "Curry powder", "Dumpling mix", "Flour", "Oil", "Relaxtea leaves", "Ball", "Basket", "Lumber", "Material stone", "Golden lumber"; dropdown="YES", expand="YES", value=1}
	}
	
	internalLists =
	{
		internals = iup.list{"Rucksack", "Big rucksack", "Book", "Music disc", "Blender", "Pan", "Microwave", "Pot", "Big pot", "Fish card 1", "Fish card 2", "Fish card 3", "Fish card 4", "Fish card 5", "Fish card 6", "Mine rock", "Cat", "Dog", "Chick", "Chicken", "Duckling", "Duck"; dropdown="YES", expand="YES", value=1}
	}
	
	----------------------------------------------------------zboxes----------------------------------------------------------------------------------
	
	toolFrameZbox1 = iup.zbox
	{
		toolLists.tools
	};
	value = toolLists.tools
	
	toolFrameZbox2 = iup.zbox
	{
		toolLists.qualities
	};
	value = toolLists.qualities
	
	fishFrameZbox1 = iup.zbox
	{
		fishLists.fish
	};
	value = fishLists.fish
	
	fishFrameZbox2 = iup.zbox
	{
		fishLists.fishKings
	};
	value = fishLists.fishKings
	
	mineFrameZbox1 = iup.zbox
	{
		mineLists.mines
	};
	value = mineLists.mines
	
	mineFrameZbox2 = iup.zbox
	{
		mineLists.mine0.mine,
		mineLists.mine1.mine,
		mineLists.mine2.mine,
		mineLists.mine3.mine,
		mineLists.mine4.mine
	};
	value = mineLists.mine0.mine
	
	mineFrameZbox3 = iup.zbox
	{
		mineLists.mine2.alexandriteVarieties,
		mineLists.mine3.cursedTools,
		mineLists.mine3.cursedAccessories
	};
	value = mineLists.mine2.alexandriteVarieties
	
	makerFrameZbox1 = iup.zbox
	{
		makerLists.makers
	};
	value = makerLists.makers
	
	makerFrameZbox2 = iup.zbox
	{
		makerLists.sizes
	};
	value = makerLists.sizes
	
	produceFrameZbox1 = iup.zbox
	{
		produceLists.produce
	};
	value = produceLists.produce
	
	produceFrameZbox2 = iup.zbox
	{
		produceLists.sizes
	};
	value = produceLists.sizes
	
	kitchenFrameZbox1 = iup.zbox
	{
		kitchenLists.utensils
	};
	value = kitchenLists.utensils
	
	kitchenFrameZbox2 = iup.zbox
	{
		kitchenLists.pan.pan,
		kitchenLists.pot.pot,
		kitchenLists.oven.oven,
		kitchenLists.mixer.mixer,
		kitchenLists.steamer.steamer,
		kitchenLists.none.none
	};
	value = kitchenLists.pan.pan
	
	kitchenFrameZbox3 = iup.zbox
	{
		kitchenLists.pot.jams,
		kitchenLists.pot.curries,
		kitchenLists.mixer.juices
	};
	value = kitchenLists.pot.jams
	
	seedFrameZbox1 = iup.zbox
	{
		seedLists.seasons
	};
	value = seedLists.seasons
	
	seedFrameZbox2 = iup.zbox
	{
		seedLists.spring.spring,
		seedLists.summer.summer,
		seedLists.autumn.autumn,
		seedLists.mushrooms.mushrooms
	};
	value = seedLists.spring.spring
	
	cropFrameZbox1 = iup.zbox
	{
		cropLists.seasons
	};
	value = cropLists.seasons
	
	cropFrameZbox2 = iup.zbox
	{
		cropLists.spring.spring,
		cropLists.summer.summer,
		cropLists.autumn.autumn,
		cropLists.mushrooms.mushrooms
	};
	value = cropLists.spring.spring
	
	cropFrameZbox3 = iup.zbox
	{
		cropLists.mushrooms.sizes
	};
	value = cropLists.mushrooms.sizes
	
	seasonalPickupsFrameZbox1 = iup.zbox
	{
		seasonalPickupsLists.seasons
	};
	value = seasonalPickupsLists.seasons
	
	seasonalPickupsFrameZbox2 = iup.zbox
	{
		seasonalPickupsLists.spring.spring,
		seasonalPickupsLists.summer.summer,
		seasonalPickupsLists.autumn.autumn,
		seasonalPickupsLists.winter.winter
	};
	value = seasonalPickupsLists.spring.spring
	
	accessoriesFrameZbox1 = iup.zbox
	{
		accessoriesLists.accessories
	};
	value = accessoriesLists.accessories
	
	accessoriesFrameZbox2 = iup.zbox
	{
		accessoriesLists.qualities.necklace,
		accessoriesLists.qualities.hatsAndEarrings,
		accessoriesLists.qualities.mine3,
		accessoriesLists.qualities.other
	};
	value = accessoriesLists.qualities.necklace
	
	miscFrameZbox1 = iup.zbox
	{
		miscLists.misc
	};
	value = miscLists.misc
	
	internalsFrameZbox1 = iup.zbox
	{
		internalLists.internals
	};
	value = internalLists.internals
	
	--------------------------------------------------------------------Frames----------------------------------------------------------------------
	
	toolFrame = iup.frame
	{
		iup.hbox
		{
			toolFrameZbox1,
			toolFrameZbox2
		};
		title="Spawn tool", size="350x", gap="10", tabtitle="Tools"
	}
	
	fishFrame = iup.frame
	{
		iup.hbox
		{
			fishFrameZbox1,
			fishFrameZbox2
		};
		title="Spawn fish", size="350x", gap="10", tabtitle="Fish"
	}
	
	mineFrame = iup.frame
	{
		iup.hbox
		{
			mineFrameZbox1,
			mineFrameZbox2,
			mineFrameZbox3
		};
		title="Spawn mine item", size="350x", gap="10", tabtitle="Mine"
	}
	
	makerFrame = iup.frame
	{
		iup.hbox
		{
			makerFrameZbox1,
			makerFrameZbox2
		};
		title="Spawn maker item", size="350x", gap="10", tabtitle="Maker"
	}
	
	produceFrame = iup.frame
	{
		iup.hbox
		{
			produceFrameZbox1,
			produceFrameZbox2
		};
		title="Spawn produce", size="350x", gap="10", tabtitle="Produce"
	}
	
	kitchenFrame = iup.frame
	{
		iup.hbox
		{
			kitchenFrameZbox1,
			kitchenFrameZbox2,
			kitchenFrameZbox3
		};
		title="Spawn dish", size="350x", gap="10", tabtitle="Dishes"
	}
	
	seedFrame = iup.frame
	{
		iup.hbox
		{
			seedFrameZbox1,
			seedFrameZbox2
		};
		title="Spawn seed", size="350x", gap="10", tabtitle="Seeds"
	}
	
	cropFrame = iup.frame
	{
		iup.hbox
		{
			cropFrameZbox1,
			cropFrameZbox2,
			cropFrameZbox3
		};
		title="Spawn crop", size="350x", gap="10", tabtitle="Crops"
	}
	
	seasonalPickupsFrame = iup.frame
	{
		iup.hbox
		{
			seasonalPickupsFrameZbox1,
			seasonalPickupsFrameZbox2
		};
		title="Spawn seasonal pickup", size="350x", gap="10", tabtitle="Seasonal pickups"
	}
	
	accessoriesFrame = iup.frame
	{
		iup.hbox
		{
			accessoriesFrameZbox1,
			accessoriesFrameZbox2
		};
		title="Spawn accessory", size="350x", gap="10", tabtitle="Accessories"
	}
	
	miscFrame = iup.frame
	{
		iup.hbox
		{
			miscFrameZbox1
		};
		title="Spawn misc item", size="350x", gap="10", tabtitle="Misc"
	}
	
	internalFrame = iup.frame
	{
		iup.hbox
		{
			internalsFrameZbox1
		};
		title="Spawn misc item", size="350x", gap="10", tabtitle="Internal"
	}
	
	--------------------------------------------------------------------Tabs----------------------------------------------------------------------
	
	tabs = iup.tabs
	{
		toolFrame,
		fishFrame,
		mineFrame,
		makerFrame,
		produceFrame,
		kitchenFrame,
		seedFrame,
		cropFrame,
		seasonalPickupsFrame,
		accessoriesFrame,
		miscFrame,
		internalFrame;
		multiline="YES"
	};
	
	--------------------------------------------------------------------Dialog--------------------------------------------------------------------------

	dlg = iup.dialog
	{
		iup.vbox
		{
			tabs,
			iup.hbox
			{
				quantityList,
				btn; padding="15"
			};
			alignment="ARIGHT"
		}; 
		title="Spawn item", size="400x100"
	};

	dlg:show()

	--------------------------------------------------------------------Helpers-------------------------------------------------------------------------
	
	function spawn(item, n)
		itemToHex = {
			["Tools"] = 
			{
				["Axe"] = 
				{
					["Stone"] = 0x000000, 
					["Copper"] = 0x010000, 
					["Silver"] = 0x020000, 
					["Gold"] = 0x030000, 
					["Mystrile"] = 0x040000, 
					["Blessed"] = 0x050000, 
					["Cursed"] = 0x060000, 
					["Mythic"] = 0x070000
				},
				["Fishing rod"] = 
				{
					["Stone"] = 0x000001, 
					["Copper"] = 0x010001, 
					["Silver"] = 0x020001, 
					["Gold"] = 0x030001, 
					["Mystrile"] = 0x040001, 
					["Blessed"] = 0x050001, 
					["Cursed"] = 0x060001, 
					["Mythic"] = 0x070001
				},
				["Hammer"] = 
				{
					["Stone"] = 0x000002, 
					["Copper"] = 0x010002, 
					["Silver"] = 0x020002, 
					["Gold"] = 0x030002, 
					["Mystrile"] = 0x040002, 
					["Blessed"] = 0x050002, 
					["Cursed"] = 0x060002, 
					["Mythic"] = 0x070002
				},
				["Hoe"] = 
				{
					["Stone"] = 0x000003, 
					["Copper"] = 0x010003, 
					["Silver"] = 0x020003, 
					["Gold"] = 0x030003, 
					["Mystrile"] = 0x040003, 
					["Blessed"] = 0x050003, 
					["Cursed"] = 0x060003, 
					["Mythic"] = 0x070003
				},
				["Sickle"] = 
				{
					["Stone"] = 0x000004, 
					["Copper"] = 0x010004, 
					["Silver"] = 0x020004, 
					["Gold"] = 0x030004, 
					["Mystrile"] = 0x040004, 
					["Blessed"] = 0x050004, 
					["Cursed"] = 0x060004, 
					["Mythic"] = 0x070004
				},
				["Watering can"] = 
				{
					["Stone"] = 0x000005, 
					["Copper"] = 0x010005, 
					["Silver"] = 0x020005, 
					["Gold"] = 0x030005, 
					["Mystrile"] = 0x040005, 
					["Blessed"] = 0x050005, 
					["Cursed"] = 0x060005, 
					["Mythic"] = 0x070005
				},
				["Clipper"] = 0x0006,
				["Milker"] = 0x0007,
				["Sword"] = 0x0008,
				["Medicine"] = 0x0009,
				["Bell"] = 0x000A,
				["Blue feather"] = 0x000B,
				["Brush"] = 0x000C,
				["Cow miracle potion"] = 0x001D,
				["Sheep miracle potion"] = 0x001E,
			},
			["Fish"] = 
			{
				["Fish bones"] = 0x0011,
				["Boot"] = 0x0013,
				["Bottle"] = 0x0014,
				["Empty can"] = 0x0015,
				["Fish fossil"] = 0x0017,
				["Pirate treasure"] = 0x003B,
				["Fish S"] = 0x0013F,
				["Fish M"] = 0x00140,
				["Fish L"] = 0x00141,
				["Branch"] = 0x018A,
				["Fish kings"] = 
				{
					["Angler fish"] = 0x00139, 
					["Coelacanth"] = 0x0013A, 
					["Squid"] = 0x0013B, 
					["Huchen"] = 0x0013C, 
					["Carp"] = 0x0013D, 
					["Spa catfish"] = 0x0013E
				}
			},
			["Mine"] = 
			{
				["Mine 0"] = 
				{
					["Coin bag"] = 0x001B,
					["Bracelet"] = 0x0048,
					["Brooch"] = 0x0049,
					["Dress"] = 0x004A,
					["Earrings"] = 0x004B,
					["Facial pack"] = 0x004C,
					["Choker"] = 0x004D,
					["Perfume"] = 0x004E,
					["Skin lotion"] = 0x004F,
					["Sun block"] = 0x0050
				},
				["Mine 1"] = 
				{
					["Junk ore"] = 0x0016,
					["Coin bag"] = 0x001B,
					["Copper"] = 0x0034,
					["Silver"] = 0x0035,
					["Gold"] = 0x0036,
					["Mystrile"] = 0x0039,
					["Black grass"] = 0x010D
				},
				["Mine 2"] = 
				{
					["Sword"] = 0x0008,
					["Junk ore"] = 0x0016,
					["Lithograph"] = 0x0019,
					["Coin bag"] = 0x001B,
					["Agate"] = 0x0037,
					["Fluorite"] = 0x0038,
					["Moon stone"] = 0x003E,
					["Amethyst"] = 0x0041,
					["Emerald"] = 0x0042,
					["Ruby"] = 0x0043,
					["Topaz"] = 0x0044,
					["Diamond"] = 0x0045,
					["Peridot"] = 0x0046,
					["Pink diamond"] = 0x0047,
					["Black grass"] = 0x010D,
					["Sand rose"] = 0x0186,
					["Alexandrite"] = 
					{
						["Green"] = 0x003F, 
						["Red"] = 0x0040
					}
				},
				["Mine 3"] = 
				{
					["Junk ore"] = 0x0016,
					["Lithograph"] = 0x0019,
					["Coin bag"] = 0x001B,
					["Copper"] = 0x0034,
					["Silver"] = 0x0035,
					["Gold"] = 0x0036,
					["Mystrile"] = 0x0039,
					["Orichalc"] = 0x003A,
					["Mythic stone"] = 0x003C,
					["Adamantite"] = 0x003D,
					["Black grass"] = 0x010D,
					["Spring sun"] = 0x0182,
					["Summer sun"] = 0x0183,
					["Autumn sun"] = 0x0184,
					["Winter sun"] = 0x0185,
					["Cursed tools"] = 
					{
						["Axe"] = 0x060000,
						["Fishing rod"] = 0x060001,
						["Hammer"] = 0x060002,
						["Hoe"] = 0x060003,
						["Sickle"] = 0x060004,
						["Watering can"] = 0x060005
					},
					["Cursed accessories"] = 
					{
						["Slow shoes"] = 0x0129,
						["Goddess earrings"] = 0x0130,
						["Kappa earrings"] = 0x0131,
						["Witch earrings"] = 0x0132,
						["Friendship pendant"] = 0x0133,
						["Goddess pendant"] = 0x0134,
						["Kappa pendant"] = 0x0135,
						["Time ring"] = 0x0136,
						["Goddess hat"] = 0x0137,
						["Witch hat"] = 0x0138
					}
				},
				["Mine 4"] = 
				{
					["Goddess' Gift"] = 0x0010,
					["Lithograph"] = 0x0019,
					["Yellow wonderful"] = 0x0022,
					["Orange wonderful"] = 0x0023,
					["Blue wonderful"] = 0x0024,
					["Green wonderful"] = 0x0025,
					["Indigo wonderful"] = 0x0026,
					["Purple wonderful"] = 0x0027,
					["Red wonderful"] = 0x0028,
					["White wonderful"] = 0x0029,
					["Black wonderful"] = 0x002A,
					["Bronze coin"] = 0x0031,
					["Silver coin"] = 0x0032,
					["Gold coin"] = 0x0033,
					["Black grass"] = 0x010D,
				}
			},
			["Maker"] = 
			{
				["Yarn"] = 
				{
					["S"] = 0x002B,
					["M"] = 0x002C,
					["L"] = 0x002D
				},
				["Cheese"] = 
				{
					["S"] = 0x0166,
					["M"] = 0x0167,
					["L"] = 0x0168
				},
				["Yoghurt"] = 
				{
					["S"] = 0x0169,
					["M"] = 0x016A,
					["L"] = 0x016B
				},
				["Mayonnaise"] = 
				{
					["S"] = 0x016C,
					["M"] = 0x016D,
					["L"] = 0x016E
				}
			},
			["Produce"] = 
			{
				["Wool"] = 
				{
					["S"] = 0x002E,
					["M"] = 0x002F,
					["L"] = 0x0030
				},
				["Milk"] = 
				{
					["S"] = 0x016F,
					["M"] = 0x0170,
					["L"] = 0x0171
				},
				["Chicken egg"] = 
				{
					["S"] = 0x0172,
					["M"] = 0x0173,
					["L"] = 0x0174
				},
				["Duck egg"] = 
				{
					["S"] = 0x0176,
					["M"] = 0x0177,
					["L"] = 0x0178
				}
			},
			["Dishes"] = 
			{
				["Frying pan"] = 
				{
					["Curry bread"] = 0x005E,
					["French toast"] = 0x0060,
					["Pancake"] = 0x0067,
					["Apple souffle"] = 0x0070,
					["French fries"] = 0x0072,
					["Popcorn"] = 0x0073,
					["Doughnut"] = 0x0074,
					["Cornflakes"] = 0x007D,
					["Tempura"] = 0x008E,
					["Fried thick noodles"] = 0x0091,
					["Fried noodles"] = 0x0092,
					["Risotto"] = 0x0097,
					["Happy eggplant"] = 0x009B,
					["Grilled fish"] = 0x009C,
					["Stir fry"] = 0x00A1,
					["Fried rice"] = 0x00AC,
					["Croquette"] = 0x00AE,
					["Savory pancake"] = 0x00AF,
					["Omelet"] = 0x00B0,
					["Omelet rice"] = 0x00B1,
					["Scrambled eggs"] = 0x00B2,
					["Pot sticker"] = 0x00BB,
					["Dry curry"] = 0x00BC,
					["Failed dish"] = 0x00
				},
				["Cooking pot"] = 
				{
					["Candied potato"] = 0x0069, -- nice
					["Cheese fondue"] = 0x007C,
					["Pumpkin stew"] = 0x007F,
					["Mountain stew"] = 0x0080,
					["Boiled spinach"] = 0x0081,
					["Egg over rice"] = 0x0082,
					["Tempura rice"] = 0x0083,
					["Stew"] = 0x0088,
					["Rice soup"] = 0x0089,
					["Noodles"] = 0x008A,
					["Curry noodles"] = 0x008B,
					["Tempura noodles"] = 0x008C,
					["Tempura buckwheat noodles"] = 0x008D,
					["Buckwheat noodles"] = 0x008F,
					["Dumplings"] = 0x0093,
					["Porridge"] = 0x0096,
					["Boiled egg"] = 0x0099,
					["Fish stew"] = 0x00C0,
					["Hot milk"] = 0x0149,
					["Hot chocolate"] = 0x014A,
					["Relaxtea"] = 0x014B,
					["Turbojolt"] = 0x014C,
					["Bodigizer"] = 0x014D,
					["Wild grape wine"] = 0x0160,
					["Failed dish"] = 0x00,
					["Jams"] = 
					{
						["Apple jam"] = 0x0162,
						["Grape jam"] = 0x0163,
						["Strawberry jam"] = 0x0164,
						["Marmalade"] = 0x0165
					},
					["Curries"] = 
					{
						["Yellow curry"] = 0x00A2,
						["Orange curry"] = 0x00A3,
						["Blue curry"] = 0x00A4,
						["Green curry"] = 0x00A5,
						["Indigo curry"] = 0x00A6,
						["Purple curry"] = 0x00A7,
						["Red curry"] = 0x00A8,
						["White curry"] = 0x00A9,
						["Black curry"] = 0x00AA,
						["Curry rice"] = 0x00AB,
						["Rainbow curry"] = 0x00BD,
						["Ultimate curry"] = 0x00BE,
						["Finest curry"] = 0x00BF
					}
				},
				["Oven"] = 
				{
					["Dinner roll"] = 0x005D,
					["Toast"] = 0x005F,
					["Jambun"] = 0x0062,
					["Apple pie"] = 0x0063,
					["Cake"] = 0x0064,
					["Cheesecake"] = 0x0065,
					["Chocolate cake"] = 0x0066,
					["Baked yam"] = 0x006A,
					["Sweet potatoes"] = 0x006B,
					["Chocolate cookies"] = 0x006C,
					["Cookies"] = 0x006D,
					["Baked corn"] = 0x0071,
					["Buckwheat ball"] = 0x0075,
					["Doria"] = 0x0094,
					["Gratin"] = 0x0095,
					["Pizza"] = 0x00B4,
					["Toasted rice balls"] = 0x00B6,
					["Roasted rice cake"] = 0x00B8,
					["Failed dish"] = 0x00,
				},
				["Mixer"] = 
				{
					["Fish sticks"] = 0x009A,
					["Turbojolt XL"] = 0x015E,
					["Bodigizer XL"] = 0x015F,
					["Butter"] = 0x017A,
					["Ketchup"] = 0x017F,
					["Failed dish"] = 0x00,
					["Juices"] = 
					{
						["Fruit juice"] = 0x014E,
						["Fruit latte"] = 0x014F,
						["Grape juice"] = 0x0150,
						["Mixed juice"] = 0x0152,
						["Mixed latte"] = 0x0153,
						["Pineapple juice"] = 0x0154,
						["Strawberry milk"] = 0x0155,
						["Tomato juice"] = 0x0156,
						["Vegetable juice"] = 0x0157,
						["Vegetable latte"] = 0x0158,
						["Peach juice"] = 0x015A,
						["Banana juice"] = 0x015B,
						["Orange juice"] = 0x015C,
						["Apple juice"] = 0x015D,
					}
				},
				["Steamer"] = 
				{
					["Steamed egg"] = 0x0051,
					["Cheese steamed bun"] = 0x0052,
					["Bamboo dumplings"] = 0x0053,
					["Curry bun"] = 0x0054,
					["Sponge cake"] = 0x0055,
					["Steamed bun"] = 0x0056,
					["Steamed cake"] = 0x0057,
					["Steamed dumplings"] = 0x0058,
					["Shaomi"] = 0x0059,
					["Chinese bun"] = 0x005A,
					["Moon dumplings"] = 0x006E,
					["Green dumplings"] = 0x006F,
					["Pudding"] = 0x0078,
					["Pumpkin pudding"] = 0x0079,
					["Failed dish"] = 0x00
				},
				["No utensil"] = 
				{
					["Raisin bread"] = 0x0061,
					["Ice cream"] = 0x0077,
					["Matsutake rice"] = 0x0084,
					["Mushroom rice"] = 0x0085,
					["Bamboo rice"] = 0x0086,
					["Buckwheat chips"] = 0x0087,
					["Chirashi sushi"] = 0x0090,
					["Pickled turnip"] = 0x009D,
					["Pickled cucumber"] = 0x009E,
					["Sashimi"] = 0x009F,
					["Sushi"] = 0x00A0,
					["Salad"] = 0x00B9,
					["Sandwich"] = 0x00BA,
					["Fruit sandwich"] = 0x00C1,
					["Elli grass"] = 0x010E,
					["Small mayonnaise"] = 0x016C,
					["Medium mayonnaise"] = 0x016D,
					["Large mayonnaise"] = 0x016F,
					["Relaxtea leaves"] = 0x0181,
					["Failed dish"] = 0x00
				}
			},
			["Seeds"] = 
			{
				["Spring crops"] = 
				{
					["Turnip seeds"] = 0x00C7,
					["Potato seeds"] = 0x00C8,
					["Cucumber seeds"] = 0x00C9,
					["Strawberry seeds"] = 0x00CA,
					["Cabbage seeds"] = 0x00CB,
					["Moondrop seeds"] = 0x00D6,
					["Toy seeds"] = 0x00D7,
					["Grass seeds"] = 0x00DA
				},
				["Summer crops"] = 
				{
					["Tomato seeds"] = 0x00CC,
					["Corn seeds"] = 0x00CD,
					["Onion seeds"] = 0x00CE,
					["Pumpkin seeds"] = 0x00CF,
					["Pineapple seeds"] = 0x00D0,
					["Pinkcat seeds"] = 0x00D8,
					["Peach seeds"] = 0x00DB,
					["Banana seeds"] = 0x00DC,
					["Orange seeds"] = 0x00DD,
					["Grass seeds"] = 0x00DA
				},
				["Autumn crops"] = 
				{
					["Eggplant seeds"] = 0x00D1,
					["Carrot seeds"] = 0x00D2,
					["Yam seeds"] = 0x00D3,
					["Spinach seeds"] = 0x00D4,
					["Bell pepper seeds"] = 0x00D5,
					["Magic red seeds"] = 0x00D9,
					["Apple seeds"] = 0x00DE,
					["Grape seeds"] = 0x00DF,
					["Grass seeds"] = 0x00DA
				},
				["Mushrooms"] = 
				{
					["Shiitake seeds"] = 0x00E0,
					["Matsutake seeds"] = 0x00E1,
					["Toadstool seeds"] = 0x00E2
				}
			},
			["Crops"] = 
			{
				["Spring crops"] = 
				{
					["Turnip"] = 0x00E3,
					["Potato"] = 0x00E4,
					["Cucumber"] = 0x00E5,
					["Strawberry"] = 0x00E6,
					["Cabbage"] = 0x00E7,
					["Moondrop flower"] =  0x00F2,
					["Toy flower"] = 0x00F3,
					["Fodder"] = 0x000F
				},
				["Summer crops"] = 
				{
					["Tomato"] = 0x00E8,
					["Corn"] = 0x00E9,
					["Onion"] = 0x00EA,
					["Pumpkin"] = 0x00EB,
					["Pineapple"] = 0x00EC,
					["Pinkcat flower"] = 0x00F4,
					["Peach"] = 0x00F7,
					["Banana"] = 0x00F8,
					["Orange"] = 0x00F9,
					["Fodder"] = 0x000F
				},
				["Autumn crops"] = 
				{
					["Eggplant"] = 0x00ED,
					["Carrot"] = 0x00EE,
					["Yam"] = 0x00EF,
					["Spinach"] = 0x00F0,
					["Bell pepper"] = 0x00F1,
					["Red magicred flower"] = 0x00F6,
					["Blue magicred flower"] = 0x00F7,
					["Apple"] = 0x00FA,
					["Grape"] = 0x00FB,
					["Fodder"] = 0x000F
				},
				["Mushrooms"] = 
				{
					["Shiitake"] = 
					{
						["S"] = 0x00FC,
						["M"] = 0x00FD,
						["L"] = 0x00FE
					},
					["Matsutake"] = 
					{
						["S"] = 0x00FF,
						["M"] = 0x0100,
						["L"] = 0x0101
					},
					["Toadstool"] = 
					{
						["S"] = 0x0102,
						["M"] = 0x0103,
						["L"] = 0x0104
					}
				}
			},
			["Seasonal pickups"] = 
			{
				["Spring"] = 
				{
					["Moondrop flower"] = 0x00F2,
					["Toy flower"] = 0x00F3,
					["Yellow grass"] = 0x0105,
					["Orange grass"] = 0x0106,
					["Bamboo shoot"] = 0x10F,
					["Weed"] = 0x0189,
					["Branch"] = 0x018A,
					["Stone"] = 0x018B
				},
				["Summer"] = 
				{
					["Pinkcat flower"] = 0x00F4,
					["Blue grass"] = 0x0107,
					["Green grass"] = 0x0108,
					["Indigo grass"] = 0x0109,
					["Purple grass"] = 0x010A,
					["Red grass"] = 0x010B,
					["Wild grape"] = 0x0110,
					["Weed"] = 0x0189,
					["Branch"] = 0x018A,
					["Stone"] = 0x018B
				},
				["Autumn"] = 
				{
					["Blue magicred flower"] = 0x00F7,
					["Red magicred flower"] = 0x00F8,
					["Matsutake S"] = 0x00FF,
					["Toadstool S"] = 0x0102,
					["Orange grass"] = 0x0106,
					["Red grass"] = 0x010B,
					["Weed"] = 0x0189,
					["Branch"] = 0x018A,
					["Stone"] = 0x018B
				},
				["Winter"] = 
				{
					["White grass"] = 0x010C,
					["Weed"] = 0x0189,
					["Branch"] = 0x018A,
					["Stone"] = 0x018B
				}
			},
			["Accessories"] = 
			{
				["Teleport stone"] = 0x0125,
				["Pedometer"] = 0x0126,
				["Clock"] = 0x0127,
				["Red cloak"] = 0x0128,
				["Truth bangle"] = 0x012A,
				["Love bangle"] = 0x012B,
				["Godhand"] = 0x012C,
				["Miracle gloves"] = 0x012D,
				["Touch screen gloves"] = 0x012E,
				["Necklace"] = 
				{
					["Base"] = 0x00012F,
					["Copper"] = 0x01012F,
					["Silver"] = 0x02012F,
					["Gold"] = 0x03012F,
					["Mystrile"] = 0x04012F,
					["Blessed"] = 0x05012F,
					["Mythic"] = 0x06012F
				},
				["Goddess earrings"] = 
				{
					["Red"] = 0x010130,
					["Blue"] = 0x020130,
					["Cursed"] = 0x000130,
					["Blessed"] = 0x030130
				},
				["Kappa earrings"] = 
				{
					["Red"] = 0x010131,
					["Blue"] = 0x020131,
					["Cursed"] = 0x000131,
					["Blessed"] = 0x030131
				},
				["Goddess hat"] = 
				{
					["Red"] = 0x010137,
					["Blue"] = 0x020137,
					["Cursed"] = 0x000137,
					["Blessed"] = 0x030137
				},
				["Kappa hat"] = 
				{
					["Red"] = 0x010138,
					["Blue"] = 0x020138,
					["Cursed"] = 0x000138,
					["Blessed"] = 0x030138
				},
				["Boots"] = 
				{
					["Cursed"] = 0x000129,
					["Blessed"] = 0x010129
				},
				["Witch earrings"] = 
				{
					["Cursed"] = 0x000132,
					["Blessed"] = 0x010132
				},
				["Time ring"] = 
				{
					["Cursed"] = 0x000136,
					["Blessed"] = 0x010136
				},
				["Friendship pendant"] = 
				{
					["White"] = 0x010133,
					["Green"] = 0x020133,
					["Cursed"] = 0x000133,
					["Blessed"] = 0x030133
				},
				["Goddess pendant"] = 
				{
					["White"] = 0x010134,
					["Green"] = 0x020134,
					["Cursed"] = 0x000134,
					["Blessed"] = 0x030134
				},
				["Kappa pendant"] = 
				{
					["White"] = 0x010135,
					["Green"] = 0x020135,
					["Cursed"] = 0x000135,
					["Blessed"] = 0x030135
				}
			},
			["Misc"] = 
			{
				["Fodder"] = 0x000F,
				["Goddess' gift"] = 0x0010,
				["Title bonus"] = 0x0018,
				["Witch's gift"] = 0x001A,
				["Bird feed"] = 0x001C,
				["10K ticket"] = 0x001F,
				["1M ticket"] = 0x0020,
				["Shaved ice"] = 0x0076,
				["Spa-boiled egg"] = 0x0175,
				["Buckwheat flour"] = 0x0179,
				["Butter"] = 0x017A,
				["Chocolate"] = 0x017B,
				["Curry powder"] = 0x017C,
				["Dumpling mix"] = 0x017D,
				["Flour"] = 0x017E,
				["Oil"] = 0x0180,
				["Relaxtea leaves"] = 0x0181,
				["Ball"] = 0x0187,
				["Basket"] = 0x0188,
				["Lumber"] = 0x018D,
				["Material stone"] = 0x018E,
				["Golden lumber"] = 0x018F
			},
			["Internal"] = 
			{
				["Rucksack"] = 0x000D,
				["Big rucksack"] = 0x000E,
				["Book"] = 0x0012,
				["Music disc"] = 0x0021,
				["Blender"] = 0x00C2,
				["Pan"] = 0x00C3,
				["Microwave"] = 0x00C4,
				["Pot"] = 0x00C5,
				["Big pot"] = 0x00C6,
				["Fish card 1"] = 0x0142,
				["Fish card 2"] = 0x0143,
				["Fish card 3"] = 0x0144,
				["Fish card 4"] = 0x0145,
				["Fish card 5"] = 0x0146,
				["Fish card 6"] = 0x0147,
				["Mine rock"] = 0x018C,
				["Cat"] = 0x0190,
				["Dog"] = 0x0191,
				["Chick"] = 0x0192,
				["Chicken"] = 0x0193,
				["Duckling"] = 0x0194,
				["Duck"] = 0x0195
			}
		}
		
		tabName = iup.GetAttributeId(tabs, "TABTITLE", tabindex)
		
		if #item == 1 then
			spawnThis = itemToHex[tabName][item[1]]
		elseif #item == 2 then
			spawnThis = itemToHex[tabName][item[1]][item[2]]
		elseif #item == 3 then
			spawnThis = itemToHex[tabName][item[1]][item[2]][item[3]]
		else
			print("Table length too long.")
		end
		
		memory.writedword(mem_inv18[hmdsv], bit.bor(bit.lshift(n, 7*8), spawnThis))
	end
	
	function getListItems()
		-- handler of the hbox in the current tab
		hboxHandler = tabs[tabindex + 1][1]
		
		-- will be populated with currently selected list items
		local listItems = {[1] = nil, [2] = nil, [3] = nil}
		
		-- gets the number of dropdown lists - in the case of tools for example it's two, one for tools and one for tool qualities
		hboxChildCount = iup.GetChildCount(hboxHandler)
		
		for i = 1, hboxChildCount do
			-- gets currently active list in the zbox; then adds the currently active item of each active list to listItems at its corresponding index
			zboxChildCount = iup.GetChildCount(hboxHandler[i])
			for j = 1, zboxChildCount do
				if hboxHandler[i][j]["visible"] == "YES" then
					if hboxHandler[i][j]["active"] == "YES" then
						itemIndex = hboxHandler[i][j]["value"]
						listItems[i] = hboxHandler[i][j][itemIndex]
					end
				end
			end
		end
		
		return listItems
	end
	
	function mineLists.mine2.checkActive()
		if mineLists.mine2.mine.value == "17" then
			mineLists.mine2.alexandriteVarieties.active = "YES"
		else
			mineLists.mine2.alexandriteVarieties.active = "NO"
		end
	end
	
	function mineLists.mine3.checkActive()
		if mineLists.mine3.mine.value == "16" then
			mineFrameZbox3.value = mineLists.mine3.cursedTools
			mineLists.mine3.cursedTools.active = "YES"
			mineLists.mine3.cursedAccessories.active = "NO"
		elseif mineLists.mine3.mine.value == "17" then
			mineFrameZbox3.value = mineLists.mine3.cursedAccessories
			mineLists.mine3.cursedTools.active = "NO"
			mineLists.mine3.cursedAccessories.active = "YES"
		else
			mineFrameZbox3.value = mineLists.mine3.cursedTools
			mineLists.mine3.cursedTools.active = "NO"
			mineLists.mine3.cursedAccessories.active = "NO"
		end
	end
	
	function kitchenLists.pot.checkActive()
		if kitchenLists.pot.pot.value == "26" then
			kitchenFrameZbox2.value = kitchenLists.pot.jams
			kitchenLists.pot.jams.active = "YES"
			kitchenLists.pot.curries.active = "NO"
		elseif kitchenLists.pot.pot.value == "27" then
			kitchenFrameZbox2.value = kitchenLists.pot.curries
			kitchenLists.pot.jams.active = "NO"
			kitchenLists.pot.curries.active = "YES"
		else
			kitchenFrameZbox2.value = kitchenLists.pot.jams
			kitchenLists.pot.jams.active = "NO"
			kitchenLists.pot.curries.active = "NO"
		end
	end
	
	function kitchenLists.mixer.checkActive()
		if kitchenLists.mixer.mixer.value == "7" then
			kitchenLists.mixer.juices.active = "YES"
		else
			kitchenLists.mixer.juices.active = "NO"
		end
	end

	-------------------------------------------------------------------Callbacks-----------------------------------------------------------------------

	function btn:action()
		item = getListItems()
		number = quantityList.value
		spawn(item, number)
		iup.ExitLoop()
		dlg:destroy()
		return iup.IGNORE
	end
	
	function tabs:tabchangepos_cb(new)
		tabindex = new
	end
	
	function toolLists.tools:action(text, number)
		if number <= 6 then
			toolLists.qualities.active = "YES"
		else
			toolLists.qualities.active = "NO"
		end
	end
	
	function fishLists.fish:action(text, number)
		if number <= 10 then
			fishLists.fishKings.active = "NO"
		else
			fishLists.fishKings.active = "YES"
		end
	end
	
	function mineLists.mines:action(text, number)
		mineLists.mine2.alexandriteVarieties.active = "NO"
		mineLists.mine3.cursedTools.active = "NO"
		mineLists.mine3.cursedAccessories.active = "NO"
		
		if number <= 3 then
			mineFrameZbox3.value = mineLists.mine2.alexandriteVarieties
		else
			mineFrameZbox3.value = mineLists.mine3.cursedTools
		end
		
		if number == 1 then
			mineFrameZbox2.value = mineLists.mine0.mine
		elseif number == 2 then
			mineFrameZbox2.value = mineLists.mine1.mine
		elseif number == 3 then
			mineFrameZbox2.value = mineLists.mine2.mine
			mineLists.mine2.checkActive()
		elseif number == 4 then
			mineFrameZbox2.value = mineLists.mine3.mine
			mineLists.mine3.checkActive()
		elseif number == 5 then
			mineFrameZbox2.value = mineLists.mine4.mine
		else
			print("Error: list index out of range")
		end
	end
	
	function mineLists.mine2.mine:action(text, number)
		mineLists.mine2.checkActive()
	end
	
	function mineLists.mine3.mine:action(text, number)
		if number <= 16 then
			mineFrameZbox3.value = mineLists.mine3.cursedTools
		else
			mineFrameZbox3.value = mineLists.mine3.cursedAccessories
		end
		
		mineLists.mine3.checkActive()
	end
	
	function produceLists.produce:action(text, number)
		if number == 1 then
			produceFrameZbox2.value = produceLists.sheep
		elseif number == 2 then
			produceFrameZbox2.value = produceLists.cow
		elseif number == 3 then
			produceFrameZbox2.value = produceLists.chicken
		elseif number == 4 then
			produceFrameZbox2.value = produceLists.duck
		else
			print("Error: list index out of range")
		end
	end
	
	function kitchenLists.utensils:action(text, number)
		kitchenLists.pot.jams.active = "NO"
		kitchenLists.pot.curries.active = "NO"
		kitchenLists.mixer.juices.active = "NO"
		
		if number <= 3 then
			kitchenFrameZbox3.value = kitchenLists.pot.jams
		else
			kitchenFrameZbox3.value = kitchenLists.mixer.juices
		end
		
		if number == 1 then
			kitchenFrameZbox2.value = kitchenLists.pan.pan
		elseif number == 2 then
			kitchenFrameZbox2.value = kitchenLists.pot.pot
			kitchenLists.pot.checkActive()
		elseif number == 3 then
			kitchenFrameZbox2.value = kitchenLists.oven.oven
		elseif number == 4 then
			kitchenFrameZbox2.value = kitchenLists.mixer.mixer
			kitchenLists.mixer.checkActive()
		elseif number == 5 then
			kitchenFrameZbox2.value = kitchenLists.steamer.steamer
		elseif number == 6 then
			kitchenFrameZbox2.value = kitchenLists.none.none
		else
			print("Error: list index out of range")
		end
	end
	
	function kitchenLists.pot.pot:action(text, number)
		if number <= 26 then
			kitchenFrameZbox3.value = kitchenLists.pot.jams
		else
			kitchenFrameZbox3.value = kitchenLists.pot.curries
		end
		
		kitchenLists.pot.checkActive()
	end
	
	function kitchenLists.mixer.mixer:action(text, number)
		kitchenLists.mixer.checkActive()
	end
	
	function seedLists.seasons:action(text, number)
		if number == 1 then
			seedFrameZbox2.value = seedLists.spring.spring
		elseif number == 2 then
			seedFrameZbox2.value = seedLists.summer.summer
		elseif number == 3 then
			seedFrameZbox2.value = seedLists.autumn.autumn
		elseif number == 4 then
			seedFrameZbox2.value = seedLists.mushrooms.mushrooms
		else
			print("Error: list index out of range")
		end
	end
	
	function cropLists.seasons:action(text, number)
		if number == 1 then
			cropFrameZbox2.value = cropLists.spring.spring
			cropLists.mushrooms.sizes.active = "NO"
		elseif number == 2 then
			cropFrameZbox2.value = cropLists.summer.summer
			cropLists.mushrooms.sizes.active = "NO"
		elseif number == 3 then
			cropFrameZbox2.value = cropLists.autumn.autumn
			cropLists.mushrooms.sizes.active = "NO"
		elseif number == 4 then
			cropFrameZbox2.value = cropLists.mushrooms.mushrooms
			cropLists.mushrooms.sizes.active = "YES"
		else
			print("Error: list index out of range")
		end
	end
	
	function seasonalPickupsLists.seasons:action(text, number)
		if number == 1 then
			seasonalPickupsFrameZbox2.value = seasonalPickupsLists.spring.spring
		elseif number == 2 then
			seasonalPickupsFrameZbox2.value = seasonalPickupsLists.summer.summer
		elseif number == 3 then
			seasonalPickupsFrameZbox2.value = seasonalPickupsLists.autumn.autumn
		elseif number == 4 then
			seasonalPickupsFrameZbox2.value = seasonalPickupsLists.winter.winter
		else
			print("Error: list index out of range")
		end
	end
	
	function accessoriesLists.accessories:action(text, number)
		if number <= 9 then
			accessoriesFrameZbox2.value = accessoriesLists.qualities.necklace
			accessoriesLists.qualities.necklace.active = "NO"
			accessoriesLists.qualities.hatsAndEarrings.active = "NO"
			accessoriesLists.qualities.mine3.active = "NO"
			accessoriesLists.qualities.other.active = "NO"
		elseif number >= 10 and number <= 10 then
			accessoriesFrameZbox2.value = accessoriesLists.qualities.necklace
			accessoriesLists.qualities.necklace.active = "YES"
			accessoriesLists.qualities.hatsAndEarrings.active = "NO"
			accessoriesLists.qualities.mine3.active = "NO"
			accessoriesLists.qualities.other.active = "NO"
		elseif number >= 11 and number <= 14 then
			accessoriesFrameZbox2.value = accessoriesLists.qualities.hatsAndEarrings
			accessoriesLists.qualities.necklace.active = "NO"
			accessoriesLists.qualities.hatsAndEarrings.active = "YES"
			accessoriesLists.qualities.mine3.active = "NO"
			accessoriesLists.qualities.other.active = "NO"
		elseif number >= 15 and number <= 17 then
			accessoriesFrameZbox2.value = accessoriesLists.qualities.mine3
			accessoriesLists.qualities.necklace.active = "NO"
			accessoriesLists.qualities.hatsAndEarrings.active = "NO"
			accessoriesLists.qualities.mine3.active = "YES"
			accessoriesLists.qualities.other.active = "NO"
		elseif number >= 18 and number <= 20 then
			accessoriesFrameZbox2.value = accessoriesLists.qualities.other
			accessoriesLists.qualities.necklace.active = "NO"
			accessoriesLists.qualities.hatsAndEarrings.active = "NO"
			accessoriesLists.qualities.mine3.active = "NO"
			accessoriesLists.qualities.other.active = "YES"
		else
			print("Error: list index out of range")
		end
	end
	
	function dlg:close_cb()
		iup.ExitLoop()
		dlg:destroy()
		return iup.IGNORE
	end

	-------------------------------------------------------------------Enter main loop-------------------------------------------------------------

	if (iup.MainLoopLevel() == 0) then
		iup.MainLoop()
	end
end
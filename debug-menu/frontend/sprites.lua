sprites = {}

function sprites.showSpriteWindow()

    teamNames = {"Red Team", "Orange Team", "Yellow Team", "Green Team", "Purple Team", "Indigo Team", "Blue Team"}
    teamSprites = {
        ["Red Team"] = {"Red Ribbon", "Wooly", "Laine", "Essa", "Betty", "Chamy", "Ole", "Sue", "Magic", "Bali", "Mick", "Woohoo"},
        ["Orange Team"] = {"Oran", "Bran", "Pierre", "Rosh", "Tilus", "Stony", "Woody", "Decoy", "Alpen", "Rocky", "Valie", "Fen"},
        ["Yellow Team"] = {"Canary", "Kali", "Johnny", "Brushy", "Mouton", "Boohoo", "Canal", "Meow", "Beta", "Aiylee", "Pompom", "Zoo"},
        ["Green Team"] = {"Forest", "Fraw", "Ridge", "Ali", "Cady", "Paddy", "Veggie", "Kevin", "Kamar", "Moor", "Vail", "Matthew"},
        ["Purple Team"] = {"Blue", "Paige", "Fry", "Rod", "Gigi", "Pedro", "Tricky", "Sammy", "Fisher", "Yacht", "Riviera", "Reese"},
        ["Indigo Team"] = {"Violetto", "Nette", "Whitney", "Juna", "Sante", "Anime", "Powery", "Koto", "Spirity", "Souly", "Carey", "Sacci"},
        ["Blue Team"] = {"Ceruleano", "Chorori", "Walter", "Rainy", "Joro", "Patty", "Misty", "Roli", "Maddie", "Karaf", "Eviran", "Owen"},
        ["Black Team"] = {"Guts", "Roller", "Hops", "Tep", "Jum", "Jet", "Jackie"}
    }    

    spriteTable = {}
    count = 0

    -- Yes/No Already Unlocked
    for t = 1, #teamNames do
        spriteTeamTable = {}
        local team = teamNames[t]
        for s = 1, #teamSprites[teamNames[t]] do
            local sprite = teamSprites[teamNames[t]][s]

            local isUnlocked = 0
            if memory.readbyte(mem_sprite[hmdsv]+(count*12)) > 0 then
                isUnlocked = 1
            end

            spriteTeamTable[sprite] = isUnlocked
            count = count + 1
        end
        spriteTable[team] = spriteTeamTable
    end

    -- Ok button in box
    btn = iup.button{title="OK"}

    toggleList = iup.hbox{}
    toggleInfo = {}

    for t = 1, #teamNames do
        subtoggleList = iup.vbox{}
        for s = 1, #teamSprites[teamNames[t]] do
            -- Default button value
            local team = teamNames[t]
            local sprite = teamSprites[teamNames[t]][s]
            local spriteValue = "OFF"
            if spriteTable[team][sprite] == 1 then
                spriteValue = "ON"
            end

            -- Add each sprite, by team, to the list
            toggleInfo[sprite] = iup.toggle{title = sprite; value = spriteValue}

            -- Add each sprite, by team, to the vbox
            iup.Append(subtoggleList, toggleInfo[sprite])
        end
        iup.Append(toggleList, subtoggleList)
    end

    -- Main window
    dlg = iup.dialog {
		iup.vbox {
            toggleList,
			btn; alignment="ARIGHT", padding="15"
		}; 
		title="Sprites", size="425x150", margin="3x3"
	};

    dlg:show()
    
    for t = 1, #teamNames do
        for s = 1, #teamSprites[teamNames[t]] do
            -- Change value
            local team = teamNames[t]
            local sprite = teamSprites[teamNames[t]][s]

            toggles = toggleInfo[sprite]

            function toggles:action(f)
                if f == 1 then
                    spriteTable[team][sprite] = 1
                else
                    spriteTable[team][sprite] = 0
                end
            end
        end
    end

    -- Write 0x09 to sprite to unlock, write 0x00 to lock
    function writeSprites()
        count = 0
        for t = 1, #teamNames do
            for s = 1, #teamSprites[teamNames[t]] do

                local team = teamNames[t]
                local sprite = teamSprites[teamNames[t]][s]

                if spriteTable[team][sprite] == 1 then
                    memory.writebyte(mem_sprite[hmdsv]+(count*12), 0x09)
                else
                    memory.writebyte(mem_sprite[hmdsv]+(count*12), 0x00)
                end

                count = count + 1
            end
        end
    end

    function btn:action()
		iup.ExitLoop()
        dlg:destroy()
        writeSprites()
		return iup.DEFAULT
    end
    
    function dlg:close_cb()
		iup.ExitLoop()
        dlg:destroy()
		return iup.IGNORE
	end

	if (iup.MainLoopLevel() == 0) then
		iup.MainLoop()
    end

end
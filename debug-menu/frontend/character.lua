character = {}

function character.showCharWindow()

    charAttribute = {"Gold", "Medals", "Stamina", "Fatigue"}
    charMem	= {
        ["Gold"] = {mem_gold[hmdsv], 4},
        ["Medals"] = {mem_medals[hmdsv], 4},
        ["Stamina"] = {mem_stamina[hmdsv], 1},
        ["Fatigue"] = {mem_fatigue[hmdsv], 1}
    }

    -- Ok button in box
    btn = iup.button{title="OK"}

    textList = iup.vbox{}
    lableInfo = {}
    textInfo = {}

    new_values = {}
    for c = 1, #charAttribute do
        local currAtt = charAttribute[c]
        local memloc = charMem[currAtt][1]
        local memsize = charMem[currAtt][2]

        new_values[currAtt] = nil

        length_of_string = (8-string.len(currAtt))
        padding = ""
        for i = 1, length_of_string do
            padding = padding.." "
        end

        if memsize == 1 then
            memvalue = memory.readbyte(memloc)
        elseif memsize == 2 then
            memvalue = memory.readword(memloc)
        elseif memsize == 4 then
            memvalue = memory.readdword(memloc)
        end

        -- Add each sprite, by team, to the list
        lableInfo[currAtt] = iup.label{title = currAtt..padding; alignment="ARIGHT", padding= "5", font="Courier New, 11"}
        textInfo[currAtt] = iup.text{value=memvalue; alignment="ARIGHT", padding="1"}

        item = iup.hbox{lableInfo[currAtt], textInfo[currAtt], margin="3x3"}

        -- Add each sprite, by team, to the vbox
        iup.Append(textList, item)
    end

    -- Main window
    dlg = iup.dialog {
        iup.vbox {
            textList,
            btn; alignment="ARIGHT", padding="15"
        };
        title="Character", size="150x300", margin="3x3"
    };

    dlg:show()

    for c = 1, #charAttribute do
        local currAtt = charAttribute[c]
        local memloc = charMem[currAtt][1]
        local memsize = charMem[currAtt][2]

        textAction = textInfo[currAtt]

        function textAction:action(t, nv)
            new_values[currAtt] = nv
        end
    end

    function changeValues()

        for c = 1, #charAttribute do
            local currAtt = charAttribute[c]
            local memloc = charMem[currAtt][1]
            local memsize = charMem[currAtt][2]

            if new_values[currAtt] ~= nil then
                if memsize == 1 then
                    memory.writebyte(memloc, new_values[currAtt])
                elseif memsize == 2 then
                    memory.writeword(memloc, new_values[currAtt])
                elseif memsize == 4 then
                    memory.writedword(memloc, new_values[currAtt])
                end
            end
        end
    end

    function btn:action()
        iup.ExitLoop()
        dlg:destroy()
        changeValues()
        return iup.DEFAULT
    end

    function dlg:close_cb()
        iup.ExitLoop()
        dlg:destroy()
        return iup.IGNORE
    end

    if (iup.MainLoopLevel() == 0) then
        iup.MainLoop()
    end

end
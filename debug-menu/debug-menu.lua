-- Adds current location of file in the path
package.path = debug.getinfo(1,"S").source:match[[^@?(.*[\/])[^\/]-$]] .."?.lua;".. package.path

require("iuplua")

require("..lua-tas-wip.memoryLocation")

require("frontend.spawn-item")
require("frontend.sprites")
require("frontend.character")

emu.addmenu("Debug", {
				{"Character...", character.showCharWindow},
				{"Spawn item...", frontend.showSpawnWindow},
				{"Sprites...", sprites.showSpriteWindow}
			})

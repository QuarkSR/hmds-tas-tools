import pyautogui as gui
import time

from ctypes import windll
user32 = windll.user32
user32.SetProcessDPIAware()

gui.PAUSE = 0.1

time.sleep(2)
print(gui.position())

def number(i):
    numlist = list(str(i))
    for n in numlist:
        gui.keyDown("%s" % n)
        gui.keyUp("%s" % n)

for i in range(1, 200):
    gui.click(x=1054, y=77)
    time.sleep(1)
    gui.keyDown("g")
    gui.keyUp("g")
    number(i)
    gui.keyDown("enter")
    gui.keyUp("enter")
    gui.click(x=1600, y=272)
    gui.keyDown("down")
    gui.keyDown("add")
    gui.keyUp("add")
    gui.keyUp("down")
    time.sleep(1)
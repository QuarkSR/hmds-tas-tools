
def hex_to_mem(mem_loc):
    if 0x02000000 <= mem_loc < 0x02400000:
        val = mem_loc - 0x02000000
    elif 0x027E0000 < mem_loc < 0x027E4000:
        val = mem_loc - 0x027E0000
    return val

def word(mem, mem_loc):
    word = int.from_bytes([mem[mem_loc+3], mem[mem_loc+2], mem[mem_loc+1], mem[mem_loc+0]], byteorder='big', signed=False)
    return word

def hword(mem, mem_loc):
    hword = int.from_bytes([mem[mem_loc+1], mem[mem_loc+0]], byteorder='big', signed=False)
    return hword

def tohex(num):
    return "0x%s" % f'{num:0>8X}'
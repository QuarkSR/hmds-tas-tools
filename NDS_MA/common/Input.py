import argparse

def import_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-g", "--gif", help="create gif of NDS memory in a folder")

    parser.add_argument("-j", "--jpg", action='store_true', help="output jpg of one (or two) memory depending on main selection")

    parser.add_argument("-s", "--size", default=4194304, help="size of memory in bytes (default = 4 mb)")

    parser.add_argument("-c", "--compare", action='store_true', help="compare between memory in image form")

    parser.add_argument("-p", "--pointers", action='store_true', help="highlight pointers")

    parser.add_argument("-a", "--analysis", help="analysis of a bug")

    parser.add_argument("-f", "--folder", help="folder to pull bin from")

    parser.add_argument("-b", "--file", nargs="*", help="bin file")

    parser.add_argument("-o", "--out", help="out file")

    args = parser.parse_args()

    return args
from common import convert as c

def process(all_mems, extra_mems):

    curr_gold = 0x023D7ACC
    price_list = 0x0215AB28

    # print(c.hex_to_mem(0x023FFFFF))
    # print(c.hex_to_mem(0x02000000))
    # print(c.hex_to_mem(0x02100000))
    # print(c.hex_to_mem(0x027E2AE0))
    # x = c.tohex(c.word(extra_mems[0], c.hex_to_mem(0x027E3478)))
    # print(x)
    # print(c.hex_to_mem(int(x, 0)))

    for m in range(0, len(all_mems)):
        print("===== Frame #%s ===== " % m)
        print("Curr Gold: %s" % c.word(all_mems[m], c.hex_to_mem(curr_gold)))
        print("Next Price #1: %s" % c.word(all_mems[m], c.hex_to_mem(price_list+(c.hword(extra_mems[m], c.hex_to_mem(0x027E3918))*12))))
        print("Next Price #2: %s" % c.word(all_mems[m], c.hex_to_mem(price_list+(c.hword(extra_mems[m], c.hex_to_mem(0x027E3A60))*12))))

        print("T1: %s" % c.tohex(c.hword(all_mems[m], c.hex_to_mem(0x02292822))))
        print("T1: %s" % c.tohex(c.word(all_mems[m], c.hex_to_mem(0x02292822))))

        # print("T1: %s" % c.word(all_mems[m], c.hex_to_mem(0x0237C91C)))
        # print("T2: %s" % c.word(all_mems[m], c.hex_to_mem(0x0237C920)))
        # print("T3: %s" % c.word(all_mems[m], c.hex_to_mem(0x0237C92C)))
        # print("T4: %s" % c.word(all_mems[m], c.hex_to_mem(0x0237C930)))
        # print("T5: %s" % c.word(all_mems[m], c.hex_to_mem(0x0237C934)))


        # print("Test 1: %s" % c.tohex(c.word(all_mems[m], c.hex_to_mem(0x023F7C44))))
        # print("Test 2: %s" % c.tohex(c.word(extra_mems[m], c.hex_to_mem(0x027E39E4))))

        # print("Look for extra")
        # for b in range(0, len(all_mems[m]), 4):
        #     if 0x027E0000 <= c.word(all_mems[m], b) < 0x027EFFFF:
        #         print(c.tohex(b+0x02000000), c.tohex(c.word(all_mems[m], b)))

        # print("Look for 0x023F7C44")
        # for b in range(0, len(all_mems[m]), 4):
        #     if c.word(all_mems[m], b) == 0x02292822:
        #         print(c.tohex(b+0x027E0000))

        # for b in range(0, len(extra_mems[m]), 2):
        #     if c.hword(extra_mems[m], b) == 0x013F:
        #         print(c.tohex(b+0x027E0000))

        # for b in range(0, len(all_mems[m]), 2):
        #     if c.hword(all_mems[m], b) == 0x013F:
        #         print(c.tohex(b+0x02000000))

        print("\n")

# 0x02189F88 - 0x0215AB28 = 0x2F460/0x12 = 0x3F08
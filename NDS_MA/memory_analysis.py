import math
import re
import glob
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import multiprocessing as mp
from common import Input
from analysis import bdg


def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)


def add_context(frame, tpad, spad, width, xseg, yseg):
    draw = ImageDraw.Draw(frame)
    font = ImageFont.truetype("arial.ttf", 50)
    draw.text((spad+xseg//4+xseg*0+width*0, 10), "0x02000000" , (0, 0, 0), font=font)
    draw.text((spad+xseg//4+xseg*1+width*1, 10), "0x02100000" , (0, 0, 0), font=font)
    draw.text((spad+xseg//4+xseg*2+width*2, 10), "0x02200000" , (0, 0, 0), font=font)
    draw.text((spad+xseg//4+xseg*3+width*3, 10), "0x02300000" , (0, 0, 0), font=font)

    hex = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
    for h in range(0, 16):
        draw.text((spad//2-15, tpad+yseg//4+yseg*h+width*h), "%s" % hex[h] , (0, 0, 0), font=font)

    for w in range(0, width):
        for r in range(0, 17):
            draw.line([(spad-width, (tpad-width+w)+(yseg*r)+(width*r)), (spad-1+xseg*4+width*4, (tpad-width+w)+(yseg*r)+(width*r))], fill=(0, 0, 0), width=0)
        for c in range(0, 5):
            draw.line([((spad-width+w)+(xseg*c)+(width*c), (tpad-width)), ((spad-width+w)+(xseg*c)+(width*c), (tpad-1+yseg*16+width*16))], fill=(0, 0, 0), width=0)

    pix = frame.load()

    return pix


def canvas(data, size):
    tpad = 75
    spad = 100
    width = 4
    im = Image.new("RGB", (size+spad+(width*(4+1)), size+tpad+(width*(16+1))), color=(255,255,255))
    pix = im.load()

    xseg = int(size/4)
    yseg = int(size/16)

    pix = add_context(im, tpad, spad, width, xseg, yseg)

    for c in range(0, 4):
        for r in range(0, 16):
            for y in range(0, yseg):
                for x in range(0, xseg):
                    pix[x+(c*xseg)+spad+(c*width),y+(r*yseg)+tpad+(r*width)] = data[(y*xseg)+(r*xseg*yseg)+(c*size*xseg)+x]

    return im


def frame_compare(size, mem1, mem2):
    data = []
    for i in range(0, size*size):
        if mem1[i] == mem2[i]:
            data.append((255,255,255))
        else:
            data.append((255,0,0))

    return data


def group_word(mem):
    words = []
    for i in range(0, len(mem), 4):
        word = int.from_bytes([mem[i+3], mem[i+2], mem[i+1], mem[i+0]], byteorder='big', signed=False)
        words.append(word)
    return words


def pointers(size, mem):
    data = []
    words = group_word(mem)
    for i in range(0, (size*size//4)):
        # if 0x02000000 <= words[i] < 0x02400000:
        if 0x027E0000 < words[i] < 0x027F0000:
            data.append((255,0,0))
            data.append((255,0,0))
            data.append((255,0,0))
            data.append((255,0,0))
        else:
            data.append((255,255,255))
            data.append((255,255,255))
            data.append((255,255,255))
            data.append((255,255,255))
    return data


def add_number(frame, number):
    draw = ImageDraw.Draw(frame)
    font = ImageFont.truetype("arial.ttf", 50)
    draw.text((10, 10), "#%s" % number , (0, 0, 0), font=font)


def import_memory(mem, max_size):
    full_ram = bytearray()
    extra_ram = bytearray()
    size = 0
    with open(mem, "rb") as f:
        while (byte_data := f.read(1)):
            if size < max_size:
                full_ram += bytearray(byte_data)
                size += 1
            elif 0x00900000 <= size < 0x00904000:
                extra_ram += bytearray(byte_data)
                size += 1
            else:
                size += 1
    return full_ram, extra_ram


def main():
    args = Input.import_arguments()

    if args.file != None:
        db = args.file
    elif args.folder != None:
        db = glob.glob("%s\\*.bin" % args.folder)

    db = natural_sort(db)
    
    size_of_bin = args.size # Default 4 MB
    sqrt_of_bin = int(math.sqrt(size_of_bin))

    pool = mp.Pool(processes=mp.cpu_count())
    all_memory, extra_memory = zip(*pool.starmap(import_memory, [(m, size_of_bin) for m in db]))
    
    if args.analysis != None:
        bdg.process(all_memory, extra_memory)
    else:
        if args.compare:
            data_frames = pool.starmap(frame_compare, [(sqrt_of_bin, all_memory[m], all_memory[m+1]) for m in range(0, len(all_memory)-1)])

        if args.pointers:
            data_frames = pool.starmap(pointers, [(sqrt_of_bin, all_memory[m]) for m in range(0, len(all_memory))])
            
        frames = pool.starmap(canvas, [(data_frames[df], sqrt_of_bin) for df in range(0, len(data_frames))])

        # If gif option is selected
        if args.gif is not None:
            for f in range(0, len(frames)):
                add_number(frames[f], f)
            frames[0].save(args.out, save_all=True, append_images=frames[1:], optimize=False, duration=300, loop=0)

        if args.jpg is not None:
            frames[0].save(args.out)
        

if __name__ == '__main__':
    main()

# python memory_analysis.py --compare --gif ".\bin" --out ".\out\out.gif"
# python memory_analysis.py --compare --jpg ".\bin\g1.bin" ".\bin\g65.bin" --out ".\out\out.jpg"
# python memory_analysis.py --pointers --jpg ".\bin\g35.bin"  --out ".\out\out2.jpg"
# python memory_analysis.py --analysis bdg --folder ".\bin"


# things to look at:
    # 00902AE0 = 027E2AE0

def dialog(dsm, frames):
    dsm.frame_loop("B", frames-1, p1=244, p2=49)
    dsm.frame_loop("", 1)


def enter_name(dsm, p1, p2):
	dsm.frame_loop("", 1, p1, p2)
	dsm.frame_loop("", 1, 20, 180)
	dsm.frame_loop("", 2)
	dsm.frame_loop("", 1, 37, 34)
	dsm.frame_loop("A", 1)


def enter_calendar(dsm):
	dsm.frame_loop("", 1, 81, 81)
	dsm.frame_loop("", 1)
	dsm.frame_loop("", 1, 78, 47)
	dsm.frame_loop("", 1)
	dsm.frame_loop("", 1, 7, 184)
	dsm.frame_loop("", 2)
	dsm.frame_loop("", 1, 31, 28)
	dsm.frame_loop("A", 1)

def intro(dsm):
	dsm.frame_loop("", 440)
	dsm.frame_loop("", 1, p1=100, p2=100)
	dsm.frame_loop("", 120)
	dsm.frame_loop("", 1, p1=100, p2=100)
	dialog(dsm, 230)
	enter_name(dsm, 114, 47) # "E is for Exadrid"
	dialog(dsm, 160)
	enter_calendar(dsm)
	dialog(dsm, 180)
	enter_name(dsm, 48, 63) # "F is for Farm"
	dialog(dsm, 165)
	enter_name(dsm, 98, 49) # "D is for Dog"
	dialog(dsm, 165)
	enter_name(dsm, 84, 49) # "C is for Cat"
	dialog(dsm, 16070)

def wake_to_bed(dsm):
	dsm.frame_loop("BU", 10)
	dsm.frame_loop("BL", 22)
	dsm.frame_loop("A", 1)
	dsm.frame_loop("", 40)
	dsm.frame_loop("A", 1)
	dsm.frame_loop("", 350)


def door_to_bed(dsm):
	dsm.frame_loop("A", 1)
	dsm.frame_loop("BU", 100)
	dsm.frame_loop("BL", 40)
	dsm.frame_loop("A", 1)
	dsm.frame_loop("", 40)
	dsm.frame_loop("A", 1)
	dsm.frame_loop("", 350)
from desmume.emulator import DeSmuME, DeSmuME_Date
from desmume.controls import keymask, Keys
from PIL import ImageFont
from PIL import ImageDraw
import time
import csv
from . import tools
from tqdm import trange

class HMDS:
    def __init__(self, nds, folder, volume, movie='', author='Exdrid', date=[2000, 1, 1, 0, 0, 0, 0], headless=True):

        save, null_fds = tools.suppress_output()

        self.emu = DeSmuME()
        self.emu.open(nds)
        self.emu.set_savetype(4)
        self.folder = folder
        self.emu.volume_set(volume)
        if movie != '':
            self.emu.movie.record('%s%s' % (folder, movie), author, rtc_date=DeSmuME_Date(date[0], date[1], date[2], date[3], date[4], date[5], date[6]))
        self.headless = headless
        self.frame = 0
        self.quit = False

        if not self.headless:
            self.window = self.emu.create_sdl_window()
            time.sleep(1)

        self.emu.input.keypad_update(0)
        self.mem_search = []
        self.mem_watched = []
        self.frozen = []
        self.mem_values = []

        tools.reset_output(save, null_fds)
 
    def destroy(self):
        self.emu.destroy()
        del self.emu

    def reset_memory(self):
        self.mem_search = []
        self.mem_watched = []
        self.frozen = []

    def save(self, loc):
        if isinstance(loc, int):
            save, null_fds = tools.suppress_output()
            self.emu.savestate.save(loc)
            tools.reset_output(save, null_fds)
        else:
            save, null_fds = tools.suppress_output()
            self.emu.savestate.save_file("%s%s" %(self.folder, loc))
            tools.reset_output(save, null_fds)
            with open("%s%s%s" %(self.folder, loc, ".frame"), "w") as frame_file:
                frame_file.write(str(self.frame))

    def load(self, loc, reset_memory=False):
        if isinstance(loc, int):
            save, null_fds = tools.suppress_output()
            self.emu.savestate.load(loc)
            tools.reset_output(save, null_fds)
        else:
            save, null_fds = tools.suppress_output()
            self.emu.savestate.load_file("%s%s" %(self.folder, loc))
            tools.reset_output(save, null_fds)
            with open("%s%s%s" %(self.folder, loc, ".frame"), "r") as frame_file:
                self.frame = int(frame_file.read())
        self.emu.input.keypad_update(0)
        if reset_memory == True:
            self.reset_memory()

    def blank_keys(self):
        self.emu.input.keypad_rm_key(Keys.KEY_RIGHT)
        self.emu.input.keypad_rm_key(Keys.KEY_LEFT)
        self.emu.input.keypad_rm_key(Keys.KEY_DOWN)
        self.emu.input.keypad_rm_key(Keys.KEY_UP) 
        self.emu.input.keypad_rm_key(Keys.KEY_SELECT) 
        self.emu.input.keypad_rm_key(Keys.KEY_START) 
        self.emu.input.keypad_rm_key(Keys.KEY_B) 
        self.emu.input.keypad_rm_key(Keys.KEY_A) 
        self.emu.input.keypad_rm_key(Keys.KEY_Y) 
        self.emu.input.keypad_rm_key(Keys.KEY_X) 
        self.emu.input.keypad_rm_key(Keys.KEY_L) 
        self.emu.input.keypad_rm_key(Keys.KEY_R)

    def add_keys(self, key):
        self.emu.input.keypad_add_key(key)

    def rm_keys(self, key):
        self.emu.input.keypad_rm_key(key)

    def frame_loop(self, buttons, f, p1=0, p2=0, run_argument=0):

        # Decode buttons for trange
        button_display = "Waiting" if buttons == "" else buttons
        button_display = "%s (%s)" % (button_display, "Arg") if run_argument != 0 else button_display
        button_display = f"{button_display:<15}"

        for lf in trange(0, f, desc='%s' % button_display):
            if run_argument != 0:
                run_argument()

            for i, watch in enumerate(self.mem_watched):
                new_value = self.read_memory(watch[0], watch[1], watch[2])
                if watch[4] != new_value:
                    print ("%s (%s - %s): %s -> %s" % (self.frame, watch[3], hex(watch[0]), hex(watch[4]), hex(new_value)))
                    self.mem_watched[i][4] = new_value

            for i, freeze in enumerate(self.frozen):
                self.write_memory(freeze[0], freeze[1], freeze[2])

            if not self.headless:
                if self.window.has_quit():
                    break
            else:
                if self.quit:
                    break

            if not (p1 == 0 and p2 == 0):
                self.emu.input.touch_set_pos(p1, p2)
            else:
                self.emu.input.touch_release()

            if not self.headless:
                self.window.process_input()

            # For some reason, the common keys from the py-desmume package don't work
            if 'R' in buttons: self.add_keys(16)
            if 'L' in buttons: self.add_keys(32)
            if 'D' in buttons: self.add_keys(128)
            if 'U' in buttons: self.add_keys(64) 
            if 'T' in buttons: self.add_keys(0) # Unknown 
            if 'S' in buttons: self.add_keys(8)
            if 'B' in buttons: self.add_keys(2) 
            if 'A' in buttons: self.add_keys(1) 
            if 'Y' in buttons: self.add_keys(2048)
            if 'X' in buttons: self.add_keys(1024) 
            if 'W' in buttons: self.add_keys(512) 
            if 'E' in buttons: self.add_keys(256)

            self.emu.cycle()
            if not self.headless:
                self.window.draw()

            if 'R' in buttons: self.rm_keys(16)
            if 'L' in buttons: self.rm_keys(32)
            if 'D' in buttons: self.rm_keys(128)
            if 'U' in buttons: self.rm_keys(64) 
            if 'T' in buttons: self.rm_keys(0) # Unknown 
            if 'S' in buttons: self.rm_keys(8)
            if 'B' in buttons: self.rm_keys(2) 
            if 'A' in buttons: self.rm_keys(1) 
            if 'Y' in buttons: self.rm_keys(2048) 
            if 'X' in buttons: self.rm_keys(1024)
            if 'W' in buttons: self.rm_keys(512) 
            if 'E' in buttons: self.rm_keys(256)

            self.frame += 1

    def continual(self, run_argument=0):
        if not self.headless:
            while not self.window.has_quit():
                if run_argument != 0:
                    run_argument()
                self.window.process_input()
                self.emu.cycle()
                self.window.draw()

    def add_number(self, pic, text):
        draw = ImageDraw.Draw(pic)
        font = ImageFont.truetype("arial.ttf", 24)
        draw.text((10, 10), "%s" % text , (0, 0, 0), font=font)
        return pic

    def show(self, name="", text=""):
        if name == "":
            if text == "":
                self.emu.screenshot().show()
            else:
                self.add_number(self.emu.screenshot(), text).show()
        else:
            if text == "":
                self.emu.screenshot().save("%s%s" % (self.folder, name))
            else:
                self.add_number(self.emu.screenshot(), text).save("%s%s" % (self.folder, name))

    def reset(self):
        self.emu.reset()
        self.emu.input.keypad_update(0)

    def read_memory(self, loc, size, signed):
        return self.emu.memory.read(loc, loc, size, signed)

    def write_memory(self, loc, size, val):
        return self.emu.memory.write(loc, loc, size, [val])

    def freeze_value(self, loc, size, val):
        self.frozen.append([loc, size, val]) 

    def add_watch(self, loc, size, signed, name):
        item = -1
        for i, watch in enumerate(self.mem_watched):
            if watch[0] == loc:
                item = i
        if item == -1:
            self.mem_watched.append([loc, size, signed, name, self.read_memory(loc, size, signed)])
        else:
            self.mem_watched[item] = [loc, size, signed, name, self.read_memory(loc, size, signed)]

    def search_memory(self, val, size, signed, not_equal=False, to_csv=0, replace="X", save=False):
        temp = []
        if replace != "X":
            for m in range(0x02000000, 0x027FFFFF, size):
                if self.read_memory(m, size, signed) == val:
                    print("Changed: ", hex(m))
                    self.write_memory(m, size, replace)
        else:
            if self.mem_search == []:
                for m in range(0x02000000, 0x027FFFFF, size):
                    if not not_equal:
                        if self.read_memory(m, size, signed) == val:
                            temp.append(m)
                    else:
                        if self.read_memory(m, size, signed) != val:
                            temp.append(m)
            else:
                for m in self.mem_search:
                    if not not_equal:
                        if self.read_memory(m, size, signed) == val:
                            temp.append(m)
                    else:
                        if self.read_memory(m, size, signed) != val:
                            temp.append(m)
            if save:
                self.mem_search = temp
            if to_csv != 0:
                with open("%s%s" %(self.folder, to_csv), 'w', newline='') as csv_file:
                    wr = csv.writer(csv_file)
                    for r in self.mem_search:
                        wr.writerow([r])
            temp_print = []
            for m in temp:
                temp_print.append(hex(m))
            if len(temp_print) < 1000:
                print(temp_print)

    def compare_memory(self, size, signed, origin=False):
        if origin:
            for m in range(0x02000000, 0x027FFFFF, size):
                self.mem_values.append([m, self.read_memory(m, size, signed)])
        else:
            for m in self.mem_values:
                if self.read_memory(m[0], size, signed) != m[1]:
                    print(hex(m[0]), m[1], " -> ", self.read_memory(m[0], size, signed), " (", self.read_memory(m[0], size, signed)-m[1],  ")")

    def dump_memory(self, start_mem, end_mem):
        mem_dump = []
        for m in range(start_mem, end_mem, 1):
            mem_dump.append(self.read_memory(m, 1, False))
        return mem_dump

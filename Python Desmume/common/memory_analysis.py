import math
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

class Memory_Analysis:
    def __init__(self, folder):
        self.size = 0
        self.size_sqrt = 0
        self.folder = folder
        self.data_prev = []
        self.data = []
        self.colour_data = []
        self.frame = 0
        self.im_list = []

    def reset(self):
        self.data_prev = self.data
        self.size = 0
        self.size_sqrt = 0
        self.colour_data = []

    def load_data(self, data_list):
        self.size = len(data_list)
        self.size_sqrt = int(math.sqrt(self.size))
        self.data = data_list
        self.frame += 1

    def identify_data(self):
        if len(self.data) == len(self.data_prev):
            for i in range(0, self.size):
                if self.data[i] != self.data_prev[i]:
                    self.colour_data.append((255,0,0))
                else:
                    self.colour_data.append((255,255,255))
        else:
            for i in range(0, self.size):
                self.colour_data.append((255,255,255))

    def add_number(self, frame, number):
        draw = ImageDraw.Draw(frame)
        font = ImageFont.truetype("arial.ttf", 50)
        draw.text((10, 10), "#%s" % number , (0, 0, 0), font=font)

    def add_context(self, frame, tpad, spad, width, xseg, yseg):
        draw = ImageDraw.Draw(frame)
        font = ImageFont.truetype("arial.ttf", 50)
        draw.text((spad+xseg//4+xseg*0+width*0, 10), "0x02000000" , (0, 0, 0), font=font)
        draw.text((spad+xseg//4+xseg*1+width*1, 10), "0x02100000" , (0, 0, 0), font=font)
        draw.text((spad+xseg//4+xseg*2+width*2, 10), "0x02200000" , (0, 0, 0), font=font)
        draw.text((spad+xseg//4+xseg*3+width*3, 10), "0x02300000" , (0, 0, 0), font=font)

        hex = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
        for h in range(0, 16):
            draw.text((spad//2-15, tpad+yseg//4+yseg*h+width*h), "%s" % hex[h] , (0, 0, 0), font=font)

        for w in range(0, width):
            for r in range(0, 17):
                draw.line([(spad-width, (tpad-width+w)+(yseg*r)+(width*r)), (spad-1+xseg*4+width*4, (tpad-width+w)+(yseg*r)+(width*r))], fill=(0, 0, 0), width=0)
            for c in range(0, 5):
                draw.line([((spad-width+w)+(xseg*c)+(width*c), (tpad-width)), ((spad-width+w)+(xseg*c)+(width*c), (tpad-1+yseg*16+width*16))], fill=(0, 0, 0), width=0)

        pix = frame.load()

        return pix

    def canvas(self):
        tpad = 75
        spad = 100
        width = 4
        im = Image.new("RGB", (self.size_sqrt+spad+(width*(4+1)), self.size_sqrt+tpad+(width*(16+1))), color=(255,255,255))
        pix = im.load()

        xseg = int(self.size_sqrt/4)
        yseg = int(self.size_sqrt/16)

        pix = self.add_context(im, tpad, spad, width, xseg, yseg)

        for c in range(0, 4):
            for r in range(0, 16):
                for y in range(0, yseg):
                    for x in range(0, xseg):
                        pix[x+(c*xseg)+spad+(c*width),y+(r*yseg)+tpad+(r*width)] = self.colour_data[(y*xseg)+(r*xseg*yseg)+(c*self.size_sqrt*xseg)+x]

        self.add_number(im, self.frame)

        return im

    def add_frame(self, data):
        self.load_data(data)
        self.identify_data()
        self.im_list.append(self.canvas())
        self.reset()

    def finalize(self):
        self.im_list[0].save("%s%s" % (self.folder, "out.gif"), save_all=True, append_images=self.im_list[1:], optimize=False, duration=300, loop=0)


class mem_loc:
    def __init__(self, hmds_version):
        if hmds_version == "1.0":
            self.v = 0
        else:
            self.v = 1

        self.memory = {}

        # Randomness
        self.random = [0x02193010]

        # Location
        self.char_loc = [0x023D7AD8]
        self.screen_loc = [0x023D3AC0]

        # Screen
        self.screenX = [0x023D3E71]
        self.screenY = [0x023D3E75]
        self.charX = [0x023D7AD4]
        self.charY = [0x023D7AD6]

        # Weather
        self.todays_weather = [0x023D6B00]
        self.tomorrows_weather = [0x023D6B02]

        # Character info
        self.stamina = [0x023D3DEC]
        self.fatigue = [0x023D3DE8]
        self.gold = [0x023D6B08]
        self.shipping = [0x023D7ACC]
        self.medals = [0x023D6B0C]

        # Time
        self.minute = [0x021CE53F]

        # Tool experience
        self.axe = [0x023DDF24]
        self.rod = [0x023DDF26]
        self.hammer = [0x023DDF28]
        self.sickle = [0x023DDF2C]
        self.can = [0x023DDF2E]
        self.hoe = [0x023DDF2A]
        self.milker = [0x023DDF30]

        # Hidden sprites
        self.hidden_sprites = [0x023DE2A8]

        # NPC + Sprites
        self.friends = [ "Celia", "Muffy", "Nami", "Romana", "Sebastian", "Lumina", "Wally", "Chris", "Grant", "Kate",
                         "Hugh", "Carter", "Flora", "Vesta", "Marlin", "Ruby", "Rock", "Dr. Hardy", "Galen", "Nina",
                         "Daryl", "Cody", "Gustafa", "Griffin", "Vans", "Kassey", "Patrick", "Murrey", "Takakura",
                         "Uknown 2", "Uknown 3", "Uknown 4", "Uknown 5", "Uknown 6", "Uknown 7", "Uknown 8", "Uknown 9", 
                         "Uknown 10", "Kai", "Uknown 11", "Uknown 12", "Uknown 13", "Uknown 14", "Harvest G.",
                         "Thomas", "Gotz", "Uknown 16", "Leia", "Keira", "Witch P."]
        self.NPCArray = [0x023DBD30]
        self.NPCLocation = [0x023D7AE0]

        # Inventory
        self.green = [0x023D6B14]
        self.backpack = [0x023D6B28]
        self.inv18= [0x023D6B6C]

        # Mines
        self.floorX = [0x022C3EC4, 0x022C6C84]
        self.floorY = [0x022C3EC5, 0x022C6C86]
        self.floorArr = [0x023DB964, 0x023DB960]

        # Farmland
        self.farmland = [0x023D83DC]

        # Fish list
        self.fishlist = [0x023DDFAC]


    def process_memory(self, dsm):
        
        # Randomness
        self.memory["Random"] = dsm.read_memory(self.random[self.v], 4, False)

        # Location
        self.memory["Character's Location"] = dsm.read_memory(self.char_loc[self.v], 1, False)
        self.memory["Screen's Location"] = dsm.read_memory(self.screen_loc[self.v], 1, False)
        self.memory["Screen X"] = (dsm.read_memory(self.screenX[self.v], 1, False) + dsm.read_memory(self.screenX[self.v]+1, 1, False)*255)
        self.memory["Screen Y"] = (dsm.read_memory(self.screenY[self.v], 1, False) + dsm.read_memory(self.screenY[self.v]+1, 1, False)*255)
        self.memory["Character X"] = (dsm.read_memory(self.charX[self.v], 1, False) + dsm.read_memory(self.charX[self.v]+1, 1, False)*255)
        self.memory["Character Y"] = (dsm.read_memory(self.charY[self.v], 1, False) + dsm.read_memory(self.charY[self.v]+1, 1, False)*255)

        # Weather
        weather = ["Sunny", "Rain", "Snow", "Stormy", "Heavy Snow"]
        self.memory["Today's Weather"] = weather[dsm.read_memory(self.todays_weather[self.v], 1, False)]
        self.memory["Tomorrow's Weather"] = weather[dsm.read_memory(self.tomorrows_weather[self.v], 1, False)]

        # Character info
        self.memory["Stamina"] = dsm.read_memory(self.stamina[self.v], 1, False)
        self.memory["Fatigue"] = dsm.read_memory(self.stamina[self.v], 1, False)
        self.memory["Gold"] = dsm.read_memory(self.gold[self.v], 4, False)
        self.memory["Shipping"] = dsm.read_memory(self.shipping[self.v], 4, False)
        self.memory["Medals"] = dsm.read_memory(self.medals[self.v], 4, False)

        # Time
        self.memory["Minute"] = dsm.read_memory(self.minute[self.v], 1, False)

        # Tool experience
        self.memory["Axe"] = dsm.read_memory(self.axe[self.v], 2, False)
        self.memory["Fishing Rod"] = dsm.read_memory(self.rod[self.v], 2, False)
        self.memory["Hammer"] = dsm.read_memory(self.hammer[self.v], 2, False)
        self.memory["Sickle"] = dsm.read_memory(self.sickle[self.v], 2, False)
        self.memory["Watering Can"] = dsm.read_memory(self.can[self.v], 2, False)
        self.memory["Hoe"] = dsm.read_memory(self.hoe[self.v], 2, False)
        self.memory["Milker"] = dsm.read_memory(self.milker[self.v], 2, False)

        # Hidden sprites
        self.memory["Hidden Sprite #1"] = [dsm.read_memory(self.hidden_sprites[self.v], 1, False), dsm.read_memory(self.hidden_sprites[self.v]+1, 1, False), dsm.read_memory(self.hidden_sprites[self.v]+2, 1, False)]
        self.memory["Hidden Sprite #2"] = [dsm.read_memory(self.hidden_sprites[self.v]+3, 1, False), dsm.read_memory(self.hidden_sprites[self.v]+4, 1, False), dsm.read_memory(self.hidden_sprites[self.v]+5, 1, False)]
        self.memory["Hidden Sprite #3"] = [dsm.read_memory(self.hidden_sprites[self.v]+6, 1, False), dsm.read_memory(self.hidden_sprites[self.v]+7, 1, False), dsm.read_memory(self.hidden_sprites[self.v]+8, 1, False)]

        # NPC and Sprites
        for f in range(0, len(self.friends)):
            self.memory[self.friends[f]] = {}
            self.memory[self.friends[f]]["FP"] = dsm.read_memory(self.NPCArray[self.v] + 4 + (f*40) + 40, 1, False)
            self.memory[self.friends[f]]["LP"] = dsm.read_memory(self.NPCArray[self.v] + 8 + (f*40) + 40, 2, False)

        # Fish Count
        self.memory["Fish Count"] = 0
        for f in range(0, 51):
            self.memory["Fish Count"] = self.memory["Fish Count"] + dsm.read_memory(self.fishlist[self.v] + (f*4), 4, False)


        # self.NPCArray = [0x023DBD30]
        # self.NPCLocation = [0x023D7AE0]

from common import hmds_desmume as dsm
from common import tools
from common import memory_location
from common import movie_functions as mf
from common import memory_analysis
import csv

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"
date = [2000, 1, 1, 1, 1, 1, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=False)
mem = memory_location.mem_loc(hmds_version="1.0")
ma = memory_analysis.Memory_Analysis(folder=out)

def test():
    # ma.add_frame(hmds.dump_memory(0x02000000, 0x02400000))
    pass

def main():

    hmds.frame_loop("", 440,  run_argument=lambda: test())
    hmds.frame_loop("", 1, p1=100, p2=100,  run_argument=lambda: test())
    hmds.frame_loop("", 120,  run_argument=lambda: test())
    hmds.frame_loop("", 1, p1=100, p2=100,  run_argument=lambda: test())

    # dialog(dsm, 230)
	# enter_name(dsm, 114, 47) # "E is for Exadrid"
	# dialog(dsm, 160)
	# enter_calendar(dsm)
	# dialog(dsm, 180)
	# enter_name(dsm, 48, 63) # "F is for Farm"
	# dialog(dsm, 165)
	# enter_name(dsm, 98, 49) # "D is for Dog"
	# dialog(dsm, 165)
	# enter_name(dsm, 84, 49) # "C is for Cat"
	# dialog(dsm, 16070)

    # ma.finalize()
    

if __name__ == "__main__":
    with tools.Timer("Main"):
        main()
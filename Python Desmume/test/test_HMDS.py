import unittest

from common import hmds_desmume as dsm
from common import movie_functions as mf
from common import memory_location

from os import path
import time

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\test\\test_out\\"
date = [2000, 1, 1, 0, 0, 0, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=True)
mem = memory_location.mem_loc(hmds_version="1.0")

class TestHMDS(unittest.TestCase):

    def HMDS_frame(self):
        self.assertEqual(hmds.frame, 0)
        hmds.frame_loop("", 110)
        self.assertEqual(hmds.frame, 110)

    def HMDS_frame2(self):
        self.assertEqual(hmds.frame, 110)
        hmds.frame_loop("", 110)
        self.assertEqual(hmds.frame, 220)

    def HMDS_run_argument(self):
        self.value = 0

        def return_value():
            self.value += 5

        self.assertEqual(self.value, 0)
        hmds.frame_loop("", 2, run_argument=lambda:return_value())
        self.assertEqual(self.value, 10)

    def HMDS_save(self):
        hmds.save('s2.ds0')

        # Files exists
        self.assertEqual(path.exists("%s%s" % (out, 's2.ds0')), True)
        self.assertEqual(path.exists("%s%s" % (out, 's2.ds0.frame')), True)

        # Correct number of frames?
        with open("%s%s" % (out, 's2.ds0.frame'), 'r') as f:
            self.assertEqual(int(f.read()), 222)

    def dsm_file(self, char, p1=0, p2=0):
        hmds.frame_loop(char, 200, p1=p1, p2=p2)
        with open("%s%s" % (out, 'test.dsm'), 'r') as f:
            return f.read().splitlines()[-2]
        
    def HMDS_dsm_file(self):
        self.assertEqual(self.dsm_file("A"), "|0|.......A.....000 000 0 000|")

    def HMDS_keys(self):
        self.assertEqual(self.dsm_file("R"), "|0|R............000 000 0 000|")
        self.assertEqual(self.dsm_file("L"), "|0|.L...........000 000 0 000|")
        self.assertEqual(self.dsm_file("D"), "|0|..D..........000 000 0 000|")
        self.assertEqual(self.dsm_file("U"), "|0|...U.........000 000 0 000|")
        # self.assertEqual(self.dsm_file("T"), "|0|....T........000 000 0 000|") # Unknown
        self.assertEqual(self.dsm_file("S"), "|0|.....S.......000 000 0 000|")
        self.assertEqual(self.dsm_file("B"), "|0|......B......000 000 0 000|")
        self.assertEqual(self.dsm_file("A"), "|0|.......A.....000 000 0 000|")
        self.assertEqual(self.dsm_file("Y"), "|0|........Y....000 000 0 000|")
        self.assertEqual(self.dsm_file("X"), "|0|.........X...000 000 0 000|")
        self.assertEqual(self.dsm_file("W"), "|0|..........W..000 000 0 000|")
        self.assertEqual(self.dsm_file("E"), "|0|...........E.000 000 0 000|")
        self.assertEqual(self.dsm_file("BU"), "|0|...U..B......000 000 0 000|")
        self.assertEqual(self.dsm_file("ABU", p1=100, p2=123), "|0|...U..BA.....100 123 1 000|")
        hmds.reset()
    
    def HMDS_load(self):
        self.assertEqual(hmds.frame, 3022)
        hmds.load('s2.ds0')
        self.assertEqual(hmds.frame, 222)
        hmds.reset()

    def HMDS_intro(self):
        mf.intro(hmds)
        self.assertEqual(hmds.frame, 222)

    def HMDS_memory(self):
        self.assertEqual(hmds.frame, 222)

    def test_run(self):
        self.HMDS_frame()
        self.HMDS_frame2()
        self.HMDS_run_argument()
        self.HMDS_save()
        self.HMDS_dsm_file()
        self.HMDS_keys()
        self.HMDS_load()
        self.HMDS_intro()


if __name__ == '__main__':
    unittest.main()
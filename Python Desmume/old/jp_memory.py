from common import hmds_desmume as dsm
from common import tools
from common import memory_location
from common import movie_functions as mf
import csv

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\JP\\0483 - Bokujou Monogatari - Korobokkuru Station (J)(NdsBbs).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"
date = [2000, 1, 1, 0, 0, 0, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=True)
mem = memory_location.mem_loc(hmds_version="1.0")


def main():
    hmds.frame_loop("", 765)

    # intro()
    short_intro()

    for m in range(0x02000000, 0x027FFFFF, 1):
        if hmds.read_memory(m, 1, False) == 2:
            if hmds.read_memory(m+3, 1, False) == 3:
                if hmds.read_memory(m+6, 1, False) == 6:
                    print(tools.tohex(m))

    hmds.frame_loop("", 0)

    # hmds.show()

    hmds.continual()

def short_intro():
    hmds.frame_loop("", 1, p1=100, p2=100)
    hmds.frame_loop("B", 150, p1=244, p2=49)

def intro():
    hmds.frame_loop("", 1, p1=100, p2=100)
    hmds.frame_loop("B", 150, p1=244, p2=49)
    hmds.frame_loop("", 1, 58, 45)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 58, 45)
    hmds.frame_loop("", 1, 20, 180)
    hmds.frame_loop("", 2)
    hmds.frame_loop("", 1, 37, 34)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("B", 150, p1=244, p2=49)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 81, 81)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 78, 47)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 7, 184)
    hmds.frame_loop("", 2)
    hmds.frame_loop("", 1, 31, 28)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("B", 150, p1=244, p2=49)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 58, 45)
    hmds.frame_loop("", 1, 20, 180)
    hmds.frame_loop("", 2)
    hmds.frame_loop("", 1, 37, 34)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("B", 150, p1=244, p2=49)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 58, 45)
    hmds.frame_loop("", 1, 20, 180)
    hmds.frame_loop("", 2)
    hmds.frame_loop("", 1, 37, 34)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("B", 150, p1=244, p2=49)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1, 58, 45)
    hmds.frame_loop("", 1, 20, 180)
    hmds.frame_loop("", 2)
    hmds.frame_loop("", 1, 37, 34)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("B", 1850, p1=244, p2=49)


if __name__ == "__main__":
    with tools.Timer("Main"):
        main()
from common import hmds_desmume as dsm
from common import tools
from common import memory_location
from common import movie_functions as mf
import csv

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"
date = [2000, 1, 1, 1, 1, 1, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=True)
mem = memory_location.mem_loc(hmds_version="1.0")


def csv_changes(day, attempt, fp, lp):
	with open('%s%s' % (out, 'hmds.csv'), 'a', newline='') as csvfile:
		fieldnames = ['day','attempt','fp','lp']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writerow(
			{
				'day':day,
				'attempt':attempt,
				'fp':fp,
				'lp':lp
			}
		)


def number_of_fish(num):
    global n_fish
    mem.process_memory(hmds)
    if num == 1:
        n_fish = mem.memory["Fish Count"]
        return 0
    else:
        diff_fish = mem.memory["Fish Count"] - n_fish
        return diff_fish


def main():
    # mf.intro(hmds)
    # hmds.save('s1.ds0')

    # hmds.load('s1.ds0')
    # set_up()
    # hmds.load('s2.ds0')
    # pick_location(9)
    # pick_location(7)
    # pick_location(2)
    # pick_location(99)
    # pick_location(4)
    # hmds.load('s3.ds0')
    # get_more_sprites(1)
    # get_more_sprites(2)
    hmds.load('s3.ds0')

    for d in range(1, 2):

        if d != 6:

            hmds.frame_loop("BU", 10)
            hmds.frame_loop("BL", 22)
            hmds.frame_loop("A", 1)
            hmds.frame_loop("", 40)
            
            hmds.save('s4.ds0')

            for i in range(0, 1000):
                print("Attempt: ", d, i)
                hmds.load('s4.ds0')
                if i % 50 == 0:
                    hmds.frame_loop("", 50)
                    hmds.save('s4.ds0')
                changes = number_of_fish(1)
                hmds.frame_loop("", i % 50)
                hmds.frame_loop("A", 1)
                hmds.frame_loop("", 190)
                changes = number_of_fish(2)
                csv_changes(d, i, 99, changes)

            hmds.frame_loop("A", 1)
            hmds.frame_loop("", 350)

        if d == 6:
            hmds.frame_loop("BU", 10)
            hmds.frame_loop("BL", 22)
            hmds.frame_loop("A", 1)
            hmds.frame_loop("", 40)
            hmds.frame_loop("A", 1)
            hmds.frame_loop("", 350)

    hmds.continual()


def get_more_sprites(ms):
    hmds.write_memory(mem.fishlist[mem.v], 4, 50)
    hmds.frame_loop("E", 1)
    hmds.frame_loop("EX", 1)
    hmds.frame_loop("", 50)
    hmds.frame_loop("", 1, p1=21, p2=131)
    hmds.frame_loop("", 50)
    hmds.frame_loop("", 2, p1=164, p2=12)
    hmds.frame_loop("", 100)
    hmds.frame_loop("U", 3)
    hmds.frame_loop("", 2, p1=60, p2=21)
    hmds.frame_loop("", 1)
    hmds.frame_loop("", 1500)
    hmds.frame_loop("Y", 1)
    mf.dialog(hmds, 1000)
    hmds.frame_loop("", 100)
    hmds.frame_loop("Y", 1)
    hmds.frame_loop("", 100)
    if ms == 2:
        hmds.write_memory(mem.fishlist[mem.v], 4, 500)
        hmds.frame_loop("", 2, p1=164, p2=12)
        hmds.frame_loop("", 100)
        hmds.frame_loop("U", 3)
        hmds.frame_loop("", 2, p1=60, p2=21)
        hmds.frame_loop("", 1)
        hmds.frame_loop("", 300)
        hmds.frame_loop("Y", 1)
        mf.dialog(hmds, 1000)
        hmds.frame_loop("", 100)
        hmds.frame_loop("Y", 1)
        hmds.frame_loop("", 100)
    hmds.frame_loop("BU", 10)
    hmds.frame_loop("BL", 22)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("", 40)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("", 350)
    hmds.save('s3.ds0')



def pick_location(loc):
    if loc == 9 or loc == 98:
        hmds.frame_loop("", 100)
        hmds.frame_loop("L", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("L", 1)
        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 30)
        hmds.frame_loop("", 3, p1=45, p2=160)
        hmds.frame_loop("", 30)
        hmds.frame_loop("A", 1)
    if loc == 7:
        hmds.frame_loop("", 100)
        hmds.frame_loop("L", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 30)
        hmds.frame_loop("", 3, p1=45, p2=160)
        hmds.frame_loop("", 30)
        hmds.frame_loop("A", 1)
    if loc == 2:
        hmds.frame_loop("", 100)
        hmds.frame_loop("D", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 30)
        hmds.frame_loop("", 3, p1=45, p2=160)
        hmds.frame_loop("", 30)
        hmds.frame_loop("A", 1)
    if loc == 99:
        hmds.frame_loop("", 100)
        hmds.frame_loop("D", 1)
        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 3, p1=76, p2=154)
        hmds.frame_loop("", 30)
        hmds.frame_loop("", 3, p1=45, p2=160)
        hmds.frame_loop("", 30)
        hmds.frame_loop("A", 1)
    if loc == 4:
        hmds.frame_loop("", 100)
        hmds.frame_loop("D", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("D", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("D", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("R", 1)
        hmds.frame_loop("", 1)
        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 30)
        hmds.frame_loop("", 3, p1=45, p2=160)
        hmds.frame_loop("", 30)
        hmds.frame_loop("A", 1)
    hmds.frame_loop("", 30)
    hmds.frame_loop("D", 1)
    hmds.frame_loop("", 1)
    hmds.frame_loop("D", 1)
    hmds.frame_loop("", 1)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("", 30)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("", 200)
    hmds.frame_loop("A", 1)
    for i in range(0, 140):
        hmds.frame_loop("Y", 1)
        hmds.frame_loop("", 30)
    mf.dialog(hmds, 300)
    hmds.frame_loop("", 200)
    hmds.save('s3.ds0')


def set_up():
    for i in range(0, 5):
        mf.wake_to_bed(hmds)
    hmds.frame_loop("BR", 10)
    hmds.frame_loop("BD", 100)
    mf.dialog(hmds, 250)
    hmds.frame_loop("D", 5)
    hmds.frame_loop("A", 1)
    mf.dialog(hmds, 250)
    hmds.frame_loop("BR", 370)
    hmds.frame_loop("BD", 210)
    hmds.frame_loop("BR", 300)
    hmds.frame_loop("BD", 350)
    hmds.frame_loop("BR", 130)
    hmds.frame_loop("BU", 100)
    hmds.frame_loop("BR", 20)
    hmds.frame_loop("BD", 40)
    hmds.frame_loop("", 1100)
    hmds.frame_loop("BU", 3)
    hmds.frame_loop("BR", 100)
    hmds.frame_loop("A", 1)
    mf.dialog(hmds, 250)
    hmds.frame_loop("", 7000)
    hmds.frame_loop("BL", 100)
    hmds.frame_loop("BU", 30)
    hmds.frame_loop("A", 1)
    mf.dialog(hmds, 1500)
    hmds.frame_loop("BD", 100)
    hmds.frame_loop("BL", 100)
    hmds.frame_loop("BU", 40)
    hmds.frame_loop("BL", 100)
    hmds.frame_loop("BD", 100)
    hmds.frame_loop("BL", 150)
    hmds.frame_loop("BU", 380)
    hmds.frame_loop("BL", 310)
    hmds.frame_loop("BU", 210)
    hmds.frame_loop("BL", 180)
    hmds.frame_loop("Y", 1)
    hmds.frame_loop("", 5)
    mf.dialog(hmds, 900)
    hmds.frame_loop("BR", 100)
    hmds.frame_loop("BU", 10)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("BU", 90)
    hmds.frame_loop("BL", 20)
    mf.wake_to_bed(hmds)
    hmds.frame_loop("BR", 10)
    hmds.frame_loop("BD", 100)
    mf.dialog(hmds, 400)
    hmds.frame_loop("", 300)
    hmds.frame_loop("Y", 1)
    hmds.frame_loop("", 30)
    hmds.frame_loop("BR", 100)
    hmds.frame_loop("BD", 210)
    hmds.frame_loop("BR", 300)
    hmds.frame_loop("BD", 350)
    hmds.frame_loop("BR", 130)
    mf.dialog(hmds, 5000)
    hmds.frame_loop("BU", 100)
    hmds.frame_loop("", 8000)
    hmds.frame_loop("BU", 1)
    hmds.frame_loop("", 150)
    hmds.frame_loop("A", 1)
    hmds.write_memory(mem.medals[mem.v], 4, 1000000)
    hmds.frame_loop("BU", 100)
    hmds.frame_loop("A", 1)
    mf.dialog(hmds, 450)
    hmds.frame_loop("", 100)
    hmds.frame_loop("A", 1)
    hmds.frame_loop("", 300)
    hmds.frame_loop("A", 1)
    hmds.save('s2.ds0')




with tools.Timer("Main"):
	main()


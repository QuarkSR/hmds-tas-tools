from common import hmds_desmume as dsm
from common import tools
from common import memory_location
from common import movie_functions as mf
import csv

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"
date = [2000, 1, 1, 1, 1, 1, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=True)
mem = memory_location.mem_loc(hmds_version="1.0")
FP_LP = [0, 0]

def strbytes(s):
    return s.encode('utf-8')

def csv_changes(day, attempt, fp, lp):
	with open('%s%s' % (out, 'hmds.csv'), 'a', newline='') as csvfile:
		fieldnames = ['day','attempt','fp','lp']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writerow(
			{
				'day':day,
				'attempt':attempt,
				'fp':fp,
				'lp':lp
			}
		)

def measure_FP_LP(num):
    global FP_LP
    mem.process_memory(hmds)
    if num == 0:
        FP_LP = [mem.memory["Leia"]["FP"], mem.memory["Leia"]["LP"]]
        return 0
    else:
        changes = [0, 0]
        if FP_LP[0] > mem.memory["Leia"]["FP"]:
            changes[0] = 1
        if FP_LP[1] > mem.memory["Leia"]["LP"]:
            changes[1] = 1
        return changes


def main():
    # mf.intro(hmds)
    # hmds.save('s1.ds0')

    hmds.load('s1.ds0')

    # Give Celia 10,000 LP and 100 FP
    hmds.write_memory(mem.NPCArray[mem.v] + 4 + (47*40) + 40, 1, 99)
    hmds.write_memory(mem.NPCArray[mem.v] + 8 + (47*40) + 40, 2, 10000)

    mf.wake_to_bed(hmds)

    hmds.save('s2.ds0')

    hmds.load('s2.ds0')

    for d in range(4, 5):

        hmds.frame_loop("BU", 10)
        hmds.frame_loop("BL", 22)
        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 40)
        
        hmds.save('s3.ds0')

        for i in range(0, 1000):
            hmds.load('s3.ds0')
            print("Num: ", d, i)
            if i % 50 == 0:
                hmds.frame_loop("", 50)
                hmds.save('s3.ds0')
            changes = measure_FP_LP(0)
            hmds.frame_loop("", i % 50)
            hmds.frame_loop("A", 1)
            hmds.frame_loop("", 190)
            changes = measure_FP_LP(1)
            csv_changes(d, i, changes[0], changes[1])

        hmds.frame_loop("A", 1)
        hmds.frame_loop("", 350)


with tools.Timer("Main"):
	main()


from common import hmds_desmume as dsm
from common import tools
from common import memory_location
from common import movie_functions as mf
from desmume.emulator import DeSmuME_Date
import time
import csv
from ctypes import cdll, create_string_buffer, cast, c_char_p, POINTER, c_int, c_char, c_uint16, c_uint8, Structure, \
    c_uint, CFUNCTYPE, c_int8, c_int16, c_uint32, c_int32

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"
date = [2000, 1, 1, 1, 1, 1, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=True)
mem = memory_location.mem_loc(hmds_version="1.0")

def strbytes(s):
    return s.encode('utf-8')

def hidden_sprites(nds, h, m, s, f):
	mem.process_memory(nds)
	with open('%s%s' % (out, 'hmds.csv'), 'a', newline='') as csvfile:
		fieldnames = ['h','m','s','f', 'l1','x1','y1','l2','x2','y2','l3','x3','y3']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writerow(
			{
				'h':h,
				'm':m,
				's':s,
				'f':f,
				'l1':mem.memory["Hidden Sprite #1"][0],
				'x1':mem.memory["Hidden Sprite #1"][1],
				'y1':mem.memory["Hidden Sprite #1"][2],
				'l2':mem.memory["Hidden Sprite #2"][0],
				'x2':mem.memory["Hidden Sprite #2"][1],
				'y2':mem.memory["Hidden Sprite #2"][2],
				'l3':mem.memory["Hidden Sprite #3"][0],
				'x3':mem.memory["Hidden Sprite #3"][1],
				'y3':mem.memory["Hidden Sprite #3"][2]
			}
		)

def main():

	for h in range(10, 11):
		for m in range(40, 41):
			for s in range(1, 60):
				hmds.emu.movie.record('%s%s' % (out, 'test.dsm'), 'Exdrid', rtc_date=DeSmuME_Date(date[0], date[1], date[2], h, m, s, date[6]))
				hmds.emu.input.keypad_update(0)
				hmds.frame_loop("", 1040)
				hmds.save('x.ds1')
				for f in range(0, 60, 5):
					hmds.frame_loop("", 4+f)
					hmds.frame_loop("", 1, p1=100, p2=100)
					hmds.frame_loop("", 9)
					hmds.frame_loop("", 1, run_argument=lambda: hidden_sprites(hmds, h, m, s, f))
					hmds.load('x.ds1')
				hmds.emu.movie.stop()
				print("Hour: %s, Minute: %s, Second: %s" % (h, m, s))


with tools.Timer("Main"):
	main()


from common import hmds_desmume as dsm
from common import tools
from common import memory_location
from common import movie_functions as mf

nds = "D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds"
out = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"
date = [2000, 1, 1, 0, 0, 0, 0]
hmds = dsm.HMDS(nds=nds, folder=out, volume=0, movie='test.dsm', author='Exdrid', date=date, headless=True)
mem = memory_location.mem_loc(hmds_version="1.0")

def main():
	for i in range(29, 60):
		hmds.frame_loop("", 1042+i)
		hmds.frame_loop("", 1, p1=100, p2=100)
		mf.dialog(hmds, 230)
		mf.enter_name(hmds, 114, 47) # "E is for Exadrid"
		mf.dialog(hmds, 160)
		mf.enter_calendar(hmds)
		mf.dialog(hmds, 180)
		mf.enter_name(hmds, 48, 63) # "F is for Farm"
		mf.dialog(hmds, 165)
		mf.enter_name(hmds, 98, 49) # "D is for Dog"
		mf.dialog(hmds, 165)
		mf.enter_name(hmds, 84, 49) # "C is for Cat"
		mf.dialog(hmds, 2150)
		hmds.show("pic%s.jpg" % i, "%s" % i)
		hmds.reset()

with tools.Timer("Main"):
	main()

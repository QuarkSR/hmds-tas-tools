from desmume.emulator import DeSmuME, DeSmuME_Date
from desmume.controls import keymask, Keys
import time
import sys

# emu = DeSmuME()
# emu.open('D:\\Dropbox\\Speed Run\\DeSmuME 9.11\\Roms\\ROMs\\NA\\0561 - Harvest Moon DS (U)(Legacy).nds')
junk_folder = "D:\\Dropbox\\Speed Run\\HMDS - TAS\\hmds-tas-tools\\Python Desmume\\junk\\"

# emu.volume_set(0)

# date = DeSmuME_Date(946684800)
# emu.movie.record('%stest.dsm' % junk_folder, "Exadrid", rtc_date=date)

headless = True
frame = 0

class Timer(object):
    def __init__(self, name=None):
        self.name = name

    def __enter__(self):
        self.tstart = time.time()

    def __exit__(self, type, value, traceback):
        if self.name:
            print('[%s]' % self.name,)
        print('Elapsed: %s' % (time.time() - self.tstart))

def up_frame():
    global frame
    frame += 1

def add_keys(key):
    emu.input.keypad_add_key(key)

def blank_keys():
    emu.input.keypad_rm_key(Keys.KEY_RIGHT)
    emu.input.keypad_rm_key(Keys.KEY_LEFT)
    emu.input.keypad_rm_key(Keys.KEY_DOWN)
    emu.input.keypad_rm_key(Keys.KEY_UP) 
    emu.input.keypad_rm_key(Keys.KEY_SELECT) 
    emu.input.keypad_rm_key(Keys.KEY_START) 
    emu.input.keypad_rm_key(Keys.KEY_B) 
    emu.input.keypad_rm_key(Keys.KEY_A) 
    emu.input.keypad_rm_key(Keys.KEY_Y) 
    emu.input.keypad_rm_key(Keys.KEY_X) 
    emu.input.keypad_rm_key(Keys.KEY_L) 
    emu.input.keypad_rm_key(Keys.KEY_R) 

def frame_loop(buttons, frames, p1=0, p2=0, ignore = 0, run_argument = 0):
    local_frames = 0
    while local_frames < frames:

        if run_argument != 0:
            run_argument()

        if not headless:
            if window.has_quit():
                break

        local_frames += 1

        if not (p1 == 0 and p2 == 0):
            emu.input.touch_set_pos(p1, p2)
        else:
            emu.input.touch_release()

        if not headless:
            window.process_input()
        blank_keys()

        if 'R' in buttons: add_keys(Keys.KEY_RIGHT)
        if 'L' in buttons: add_keys(Keys.KEY_LEFT)
        if 'D' in buttons: add_keys(Keys.KEY_DOWN)
        if 'U' in buttons: add_keys(Keys.KEY_UP) 
        if 'T' in buttons: add_keys(Keys.KEY_SELECT) 
        if 'S' in buttons: add_keys(Keys.KEY_START) 
        if 'B' in buttons: add_keys(Keys.KEY_B) 
        if 'A' in buttons: add_keys(Keys.KEY_A) 
        if 'Y' in buttons: add_keys(Keys.KEY_Y) 
        if 'X' in buttons: add_keys(Keys.KEY_X) 
        if 'W' in buttons: add_keys(Keys.KEY_L) 
        if 'E' in buttons: add_keys(Keys.KEY_R) 

        emu.cycle()
        if not headless:
            window.draw()

        up_frame()

def dialog(frames):
    frame_loop("B", frames-1, p1=244, p2=49)
    frame_loop("", 1)

def enter_name(p1, p2):
	frame_loop("", 1, p1, p2)
	frame_loop("", 1, 20, 180)
	frame_loop("", 2)
	frame_loop("", 1, 37, 34)
	frame_loop("A", 1)

def enter_calendar():
	frame_loop("", 1, 81, 81)
	frame_loop("", 1)
	frame_loop("", 1, 78, 47)
	frame_loop("", 1)
	frame_loop("", 1, 7, 184)
	frame_loop("", 2)
	frame_loop("", 1, 31, 28)
	frame_loop("A", 1)


if not headless:
    window = emu.create_sdl_window()
    time.sleep(2)

def memory_4bytes(loc):
    return emu.memory.read(loc, loc, 4, True)


def store_data():
    global df
    df = df.append(dict(random=memory_4bytes(0x02193010)), ignore_index=True)

def intro():
    with Timer('Main'):

        frame_loop("", 440)
        frame_loop("", 1, p1=100, p2=100)
        frame_loop("", 120)
        frame_loop("", 1, p1=100, p2=100)
        dialog(230)
        enter_name(114, 47) # "E is for Exadrid"
        dialog(160)
        enter_calendar()
        dialog(180)
        enter_name(48, 63) # "F is for Farm"
        dialog(165)
        enter_name(98, 49) # "D is for Dog"
        dialog(165)
        enter_name(84, 49) # "C is for Cat"
        dialog(15990)

def collect_data():
    for i in range(0, 7):
        with Timer('Set #%s' % i):
            frame_loop("", 10000, run_argument = lambda: store_data())
            emu.savestate.save_file("%sSave N%s.save" %(junk_folder, i))
            df.to_pickle("%sDF Pickle.pkl" %(junk_folder))


def analyse_data():
    pass

def main():

    # intro()

    # emu.savestate.load_file("%sSave N9.save" %(junk_folder))

    import numpy as np 
    import pandas as pd
    global df
    # df = pd.DataFrame(columns=['random'])

    df = pd.read_pickle("%sDF Pickle.pkl" %(junk_folder))

    # collect_data()

    analyse_data()

    bins = []
    labels = []
    for i in range(-2200000000, 2200000000, 100000000):
        bins.append(i)
        labels.append("%s to %s" % (f"{i:,}", f"{i+100000000:,}"))
    bins.append(2200000000)
    print(bins)
    
    # labels = ["a", "b"

    df["binned"] = pd.cut(df["random"], bins, labels=labels, right=True)

    print(df['binned'].value_counts())
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(0)
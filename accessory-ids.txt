ID  | Accessory
----|----------------
37  | Teleport stone
38  | Pedometer
40  | Red cloak
42  | Truth bangle
43  | Love bangle
44  | Godhand
45  | Miracle glove
46  | Touch glove
47  | Necklace
48  | Harvest goddess earrings
49  | Kappa earrings
51  | Friendship pendant
52  | Harvest goddess pendant
54  | Kappa pendant
55  | Harvest goddess hat
56  | Kappa hat